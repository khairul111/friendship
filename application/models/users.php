<?php

class Users extends MY_Model
{
    function __construct()
    {
        parent::__construct();
        $this->loadTable('users');
    }

    public function isValidUser($name,$password)
    {
        $user = $this->find("username = '{$name}' AND password = md5('{$password}') AND status = 'active'");
        if($user){
            $this->_setSessionData($name);
            return true;
        }else{
            return false;
        }

    }

    private function _setSessionData($name)
    {
        $_SESSION['userName'] 	= $name;
        if(isset($_POST['rememberme'])){
            $this->setCookieDataForRememberMe();
        }else{
            delete_cookie("user");
        }
    }


     public function setFormValidationRulesForLogin()
     {
        $this->form_validation->set_rules('name','User Name','required');
        $this->form_validation->set_rules('password','Password','required');


     }

     public function setFormValidationRulesForChangePassword()
     {
       $this->form_validation->set_rules('old_password','Old Password','required');
       $this->form_validation->set_rules('new_password','New Password','required|matches[confirm_password]');
       $this->form_validation->set_rules('confirm_password','Confirm Password','required');


     }

     public function processLogin()
     {
         $name = $this->input->post('name');
         $password = $this->input->post('password');
         return $this->isValidUser($name,$password);
     }

     private function setCookieDataForRememberMe()
     {
         delete_cookie("user");
         $data = array('name' => $this->input->post('name'),
                       'password' => $this->input->post('password')
         );
         
         $cookie = array(
		                   'name'   => 'user',
		                   'value'  => serialize($data),
		                   'expire' => '86500',
		               );                       
		set_cookie($cookie);
         
     }

     public function getCookieDataForRememberMe()
     {
         $user = get_cookie('user');
         return (!empty($user)) ? unserialize($user) : false;

     }

     public function updatePassword()
     {
         if($this->isPasswordExist()){
             $newPassword = addslashes(md5($this->input->post('new_password')));
             $name = addslashes($_SESSION['userName']);
             $sql = "UPDATE users SET password = '$newPassword' WHERE username = '$name'";
             $this->query($sql);
             return true;
         }else{
             return false;
         }

     }
     
     public function saveExternal($data, &$message)
     {
         $data['status'] = 1;
         $this->loadTable('external_user');
         if($this->isExternalUserExist($data['email'])){
             $message = "You have already registered.";
             return false;
         }
         
         return $this->save($data);
     }
     
     public function isExternalUserExist($externalUserEmail)
     {
         return $this->find(array("email"=>$externalUserEmail));
     }

     private function isPasswordExist()
     {
         $oldPassword = $this->input->post('old_password');
         $name = $_SESSION['userName'];
         $user = $this->find("username = '{$name}' AND password = md5('{$oldPassword}') AND status = 'active'");
         if($user){
             return true;
         }else{
             return false;
         }

     }
     
     public function saveExternalUser(array $data)
     {
         $this->loadTable('users');
     }

}
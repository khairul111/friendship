<?php if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class Feedbacks extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->loadTable('feedback');
    }

   public function getAll($start, $total, $orderBy = "id") {

        if ($orderBy == 'id')
        {
            $sort = 'DESC';
        }else{
            $sort = 'ASC';
        }

        $results = $this->findAll(null, '*', $orderBy.' '.$sort , $start, $total);

        if ($results) {
            return $results;
        }

        return false;
    }

   public function save($data)
   {
       $feedbackId = $this->insert($data);
       if($feedbackId){
           return true;
       }
       return false;
   }
}
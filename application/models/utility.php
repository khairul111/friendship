<?php
 /**
 * Utility
 * Utility functions for dynamicaly loading javascripts and css
 * 
 * @package     Serbian Cafe
 * @category    Model
 * @author      Anis uddin Ahmad <anisniit@gmail.com>
 */
class Utility
{
    protected $JQueryUI      = null;
    protected $JQueryUITheme = 'ui-lightness';
    
    public $javascripts;
    public $stylesheets;
    
    /**
    * Set reference variables
    *
    * @author  Anis uddin Ahmad <anisniit@gmail.com>
    * @access  public
    * @param   array   $js
    * @param   array   $css 
    * @return  void
    */
    public function setVariables(array &$js, array &$css ,array &$jsVars = NULL)
    {
        $this->javascripts =& $js;	    
        $this->stylesheets =& $css;
        $this->jsVars =& $jsVars;
    }

    /**
    * Load jQueryUI DatePicker
    *
    * @author  Anis uddin Ahmad <anisniit@gmail.com>
    * @access  public
    * @return  void
    */
    public function loadDatepicker()
    {
        $this->loadUi();
        
        $this->javascripts['datepicker'] = 'ui/ui.datepicker.js';
        $this->stylesheets['datepicker'] = "ui-themes/{$this->JQueryUITheme}/ui.datepicker.css";
    }
    
    /**
    * Load jQueryUI Tabs
    *
    * @author  Anis uddin Ahmad <anisniit@gmail.com>
    * @access  public
    * @return  void
    */
    public function loadTabs()
    {
        $this->loadUi();
        
        $this->javascripts['tabs'] = 'ui/ui.tabs.js';
        $this->stylesheets['tabs'] = "ui-themes/{$this->JQueryUITheme}/ui.tabs.css";
    }

 	/**
    * Load jQueryUI Drag 
    *
    * @author  Muhit mohidul<mohidul18@gmail.com>
    * @access  public
    * @return  void
    */
	public function loadSortable()
    {
        $this->loadUi();
        
        $this->javascripts['sortable'] 		= 'jquery.ui-1.5.3/ui/packed/ui.sortable.packed.js';       
    }
 	/**
    * Load jQueryUI star Rating
    *
    * @author  Muhit mohidul<mohidul18@gmail.com>
    * @access  public
    * @return  void
    */
	public function loadStarRating()
    {
        $this->loadUi();
        
        $this->javascripts['StarRating'] 		= 'starRating/jquery.starRating.js';       
    }
    /* Load jQueryUI Dialog 
    *
    * @author  Anis uddin Ahmad <anisniit@gmail.com>
    * @access  public
    * @return  void
    */
    public function loadDialog()
    {
        $this->loadUi();
        
        // Load javascript
        $this->javascripts['draggable'] = 'ui/ui.draggable.js';
        $this->javascripts['resizable'] = 'ui/ui.resizable.js';
        $this->javascripts['dialog']    = 'ui/ui.dialog.js';
        
        // Load stylesheets
        $this->stylesheets['draggable'] = "ui-themes/{$this->JQueryUITheme}/ui.draggable.css";
        $this->stylesheets['resizable'] = "ui-themes/{$this->JQueryUITheme}/ui.resizable.css";
        $this->stylesheets['dialog']    = "ui-themes/{$this->JQueryUITheme}/ui.dialog.css";
    }
    
    /**
    * Load jQueryUI Interactions
    *
    * @author  Anis uddin Ahmad <anisniit@gmail.com>
    * @access  public
    * @param   string   $action
    * @return  void
    */
    public function loadInteraction($action)
    {
        $this->loadUi();
        
        // Load interaction specific js file
        $this->javascripts['action_' . $action] = "ui/ui.{$action}.js";
    }
    
    /**
    * Load jQueryUI Effect
    *
    * @author  Anis uddin Ahmad <anisniit@gmail.com>
    * @access  public
    * @param   string   $effect
    * @return  void
    */
    public function loadEffect($effect)
    {
        $this->loadUi();
        
        // Load effects core and specific effect file
        $this->javascripts['effect']            = 'ui/effects.core.js';
        $this->javascripts['effect_' . $effect] = "ui/effects.{$effect}.js";
    }
    
    /**
    * Load jQuery Plugins
    *
    * @author  Anis uddin Ahmad <anisniit@gmail.com>
    * @access  public
    * @param   string   $plugin
    * @return  void
    */
    public function loadPlugin($plugin)
    {
        $this->javascripts['plugin_' . $plugin] = "plugins/jquery.{$plugin}.js";
    }
    
    /**
    * Load core files for jQuery UI
    *
    * @author  Anis uddin Ahmad <anisniit@gmail.com>
    * @access  public
    * @return  void
    */
    public function loadUi()
    {
        if(! $this->JQueryUI)
        {
            $this->javascripts['ui']    = 'ui/ui.core.js';
            $this->stylesheets['core']  = "ui-themes/{$this->JQueryUITheme}/ui.core.css";
            $this->stylesheets['theme'] = "ui-themes/{$this->JQueryUITheme}/ui.theme.css";
            
            $this->JQueryUI = true;
        }
    }
    
    
    /**
    * Set jQuery UI theme name
    * Should be called (if required) before loading any UI component
    *
    * @author  Anis uddin Ahmad <anisniit@gmail.com>
    * @access  public
    * @param   string   $theme
    * @return  void
    */
    public function setTheme($theme)
    {
        $this->JQueryUITheme = $theme;	
    }	
    
    // current CI_Loader requirs it. dont know why
    // TODO : remove me
    function _assign_libraries(){}
}
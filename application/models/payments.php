<?php

class Payments extends MY_Model
{
    function __construct()
    {
        parent::__construct();
        $this->loadTable('payments');
    }

    public function getAllPayments($start = 0, $total = 10, $orderBy = "id")
    {
         $sql = "SELECT  id, name, email, address_city, address_country, transaction_id,
                         amount, status, modified

                FROM        payments
                ORDER BY    $orderBy
                LIMIT       $start, $total";

        $results = $this->query($sql);

        if ($results) {
            return $results->result();
        }

        return false;
    }

   public function save($formData)
   {
       $data['name']            = $formData['name'];
       $data['email']           = $formData['email'];
       $data['address_street']  = $formData['address'];
       $data['address_state']   = $formData['state'];
       $data['address_city']    = $formData['city'];
       $data['zip']             = $formData['zip'];
       $data['address_country'] = $formData['country'];
       $data['phone']           = $formData['phone'];
       $data['amount']          = $formData['amount'];
       
       if($formData['currency'] == 'USD' || $formData['currency']=='Euro'){
           $data['currency'] = $formData['currency'];
       }else{
           return false;
       }
       
       $data['comments'] = $formData['comments'];

       $paymentId = $this->insert($data);
       if($paymentId){
           return $paymentId;
       }
       return false;
   }

   public function updateIPN($id, $transactionId, $status)
   {
        $sql = "UPDATE payments
                SET    status           = '$status',
                       transaction_id   = '$transactionId'

                WHERE  id = $id";
        $update = $this->query($sql);
   }
}
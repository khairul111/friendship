<?php

class Common extends MY_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    public function loadLibraryForFormValidation()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<p class="ui-state-error ui-corner-all" style="margin: 5px 0pt; padding: 5px 10px; width: 335px;">','</p>');
    }

}
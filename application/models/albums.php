<?php if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class Albums extends MY_Model {
    public function __construct() {
        parent::__construct();
        $this->loadTable('albums');
    }

    public function getAll($start, $total, $orderBy = "id") {
        $sql = "SELECT  a.*, p.path

                FROM        albums a
                LEFT JOIN        photos p ON a.cover_photo_id = p.id
                ORDER BY    $orderBy";

        $results = $this->query($sql);

        if ($results) {
            return $results->result();
        }

        return false;
    }

    public function getAllActive($start, $total, $orderBy = "id") {
        $sql = "SELECT  a.*, p.path
                FROM        albums a
                LEFT JOIN        photos p ON a.cover_photo_id = p.id
                WHERE a.status = 'active'
                ORDER BY    $orderBy";

        $results = $this->query($sql);

        if ($results) {
            return $results->result();
        }

        return false;
    }


    public function getAllAlbum()
    {
        return $this->generateList("id", "title");
    }

    public function updateCover($data, $albumId) {
        return $this->save($data, $albumId);
    }
    public function getAlbum($id) {
        return $this->find(array("id"=>$id));
    }

    public function updateAlbum($data, $id) {
        return $this->save($data, $id);
    }

    public function deleteAlbum($id) {
        $sql = "DELETE FROM photos WHERE album_id='$id'";
        $this->query($sql);
        return $this->remove($id);
    }

    public function getCoverPath($id){
        $sql ="
            SELECT p.path
            FROM albums a
            JOIN photos p ON p.id = a.cover_photo_id
            WHERE a.id = $id
        ";
        $query = $this->db->query($sql);
        $result = $query->result_object();
        if($result){
            return $result[0]->path;
        }
        return false;
    }

}

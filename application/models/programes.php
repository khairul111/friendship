<?php

class Programes extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->loadTable("programe");
    }

    public function getAllProgrames($start, $total, $orderBy = "id")
    {
        if ($orderBy == 'name')
        {
            $sort = 'ASC';
        }else{
            $sort = 'DESC';
        }
        $orderBy = 'prog.'.$orderBy;

        $sql = "SELECT      prog.id, p.title, prog.name, prog.status
                FROM        programe prog
                JOIN        pages p ON prog.page_id=p.id
                ORDER BY    $orderBy
                LIMIT       $start, $total";

        $results = $this->query($sql);

        if ($results) {
            return $results->result();
        }

        return false;
    }

    public function getSelectedPrograme($programeId)
    {
        $sql = "SELECT      id, page_id, name, image_path, status
                FROM        programe
                WHERE       id = $programeId";

        $results = $this->query($sql);

        if ($results) {
            return $results->result();
        }
        return false;
    }

    function fileUpload()
    {
        $config['upload_path'] = 'assets/photos/';
        $config['allowed_types'] = 'jpg|png|gif';
        //$config['max_size']	= '10240';
        $config['remove_spaces']	= true;
        $config['overwrite'] = false;
        $config['encrypt_name'] = false;

        $this->load->library('upload', $config);      
    }

    public function saveProgrameInfo($programeData)
    {                
        $this->fileUpload();
        if ($this->upload->do_upload('image_path')) {
            $fileInfo = $this->upload->data();
            $programeData['path'] = $fileInfo['file_name'];
        }else{
            $programeData['path'] = "";
        }
        $newsInfo = array(
            'page_id'               => $programeData['page_id'],
            'name'                  => $programeData['name'],
            'image_path'            => $programeData['path'],
            'status'                => $programeData['status']
        );
        $newsId = $this->insert($newsInfo);
    }
    
    public function updateNewsInfo($programeData)
    {
        $this->fileUpload();
        if ($this->upload->do_upload('image_path')) {
            $fileInfo = $this->upload->data();
            $programeData['path'] = $fileInfo['file_name'];
            $sql = "UPDATE  programe
                    SET     image_path       = '$programeData[path]',
                            name             = '$programeData[name]',
                            page_id          = '$programeData[page_id]',
                            status           = '$programeData[status]'
                    WHERE   id               = $programeData[programeId]";

        }else{
            $sql = "UPDATE  programe
                    SET     name             = '$programeData[name]',
                            page_id          = '$programeData[page_id]',
                            status           = '$programeData[status]'
                    WHERE   id               = $programeData[programeId]";
        }
        $updatePage = $this->query($sql);
    }

    public function deletePrograme($programeId)
    {
        $return = $this->remove($programeId);

        if($return){
            return true;
        }

        return false;
    }

    public function loadFormValidationRulesForPrograme()
    {
        $this->form_validation->set_rules('name','Name','required');
       
    }

}

<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Setting Model
 *
 * Deals with the setting related functionality of the site
 *
 * @package     Friendship-bd
 * @category    Model
 * @author      Nurul Ferdous <nurul.ferdous@gmail.com>
 */
class Settings extends MY_Model
{
    /**
     * Initializes common resources
     *
     * @access   public
     * @return   void
     */
    public function __construct()
    {
        parent::MY_Model();
        $this->loadTable('settings');
    }

    /**
     * Retrieves a setting from the database
     *
     * @param  string $key
     * @access public
     * @return string
     */
    public function get($key)
    {
        if(empty($key)) {
            throw new Exception("Key must be specified.");
        }

        return $this->field(array('key' => $key), "value");
    }

    /**
     * Updates a setting in the database
     *
     * @access public
     * @return void
     */
    public function set($key, $value)
    { 
        if(empty($key)) {
            throw new Exception("key must be specified.");
        }

        $data = array("key" => $key, "value" => $value);
        $id = $this->field(array("key" => $key), "id");

        $this->save($data, $id);
    }

    /**
     * Retrieves all setting from the database
     *
     * @return string
     * @access public
     */
    public function getAll()
    {
        $allSettings = $this->generateList('key', 'value');
        return $allSettings;
    }


    public function loadFormValidationRulesForSettings()
    {
       $this->form_validation->set_rules('site_name','Site Name','required');
       $this->form_validation->set_rules('paypal_address','Paypal Address','required|valid_email');
    }


    public function updateSettings($data = array())
    { 
        $settingsData['SITE_NAME']                      = $data['site_name'];
        $settingsData['SITE_CONTACT_EMAIL_ADDRESS']     = $data['contact_email'];
        $settingsData['GENERAL_META_KEYWORD']           = $data['meta_keywords'];
        $settingsData['GENERAL_META_DESCRIPTION']       = $data['meta_description'];
        $settingsData['SITE_CONTACT_ADDRESS_EN']        = trim($data['contact_english']);
        $settingsData['SITE_CONTACT_ADDRESS_BN']        = trim($data['contact_bangla']);
        $settingsData['HOMEPAGE_MAP']                   = trim($data['homepage_map']);
        $settingsData['HOMEPAGE_MAP_LINK']              = trim($data['homepage_map_link']);
        $settingsData['FEEDBACK_LEFT']                  = trim($data['feedback_left']);
        $settingsData['FEEDBACK_RIGHT']                 = trim($data['feedback_right']);
        $settingsData['NEWS_LEFT']                      = trim($data['news_left']);
        $settingsData['NEWS_RIGHT']                     = trim($data['news_right']);
        $settingsData['SITEMAP_LEFT']                   = trim($data['sitemap_left']);
        $settingsData['SITEMAP_RIGHT']                  = trim($data['sitemap_right']);
        $settingsData['DONATE_LEFT']                    = trim($data['donate_left']);
        $settingsData['DONATE_RIGHT']                   = trim($data['donate_right']);
        $settingsData['BROCHURE']                       = trim($data['brochure']);
        $settingsData['NEWSLETER']                      = trim($data['newsleter']);
        $settingsData['VIDEO']                          = trim($data['video']);
        $settingsData['BLOG']                           = trim($data['blog']);
        $settingsData['DONATE']                         = trim($data['donate']);
        $settingsData['BANK_INFO']                      = trim($data['bank_info']);
        $settingsData['PAYPAL_ADDRESS']                 = trim($data['paypal_address']);
        $settingsData['DONATION_INFO']                 = trim($data['donation_info']);
        

        foreach($settingsData as $key=> $val)
		{
			$this->set($key,$val);
		}

        return  true;
    }
}
<?php

class Pages extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->loadTable("pages");
    }

    public function getAllPages($start, $total, $orderBy = "id")
    {
        if ($orderBy == 'title' || $orderBy == 'parent_title'){
            $order = "ASC";
        }else{
            $order = "DESC";
        }

        $sql = "SELECT      p.id, p.title, p.meta_keyword, p.meta_description, p.modified,
                            pp.title AS parent_title, pp.id AS parent_id
                FROM        pages p
                LEFT JOIN   pages pp ON p.parent = pp.id
                ORDER BY    $orderBy $order
                LIMIT       $start, $total";

        $results = $this->query($sql);

        if ($results) {
            return $results->result();
        }

        return false;
    }

    public function getSelectedFields($fields,$orderBy, $onlyParent = true)
    {
        $parent = ($onlyParent) ? "parent=0" : "1=1";
        
        $sql = "SELECT      $fields
                FROM        pages
                WHERE       $parent
                ORDER BY    $orderBy";

        $results = $this->query($sql);
        if ($results) {
            return $results->result();
        }
        return false;
    }

    public function getPagesPhotos($pageId)
    {
        $this->loadTable("pages_photos");
        return $this->generateSingleArray("page_id = $pageId", 'photo_id', null);
    }

    public function getBanner($pageId)
    {
        $this->loadTable("pages_photos");
        $return = $this->find("page_id = $pageId AND page_banner = 1", 'photo_id', null);
        return $return['photo_id'];
    }

    public function savePageInfo($pagedata)
    {
        $slug = $this->_sanitizeText($pagedata['page_title']);
        $pageInfo = array(
            'parent'            => $pagedata['parent'],
            'title'             => $pagedata['page_title'],
            'slug'              => $slug,
            'meta_keyword'      => $pagedata['meta_keyword'],
            'meta_description'  => mysql_real_escape_string(stripslashes($pagedata['meta_description'])),
            'page_color'        => $pagedata['page_color'],
            'page_column'       => $pagedata['page_column'],
            'page_order'        => $pagedata['page_order'],
            'menu_link'         => $pagedata['menu_link']
        );

        $pageId = $this->insert($pageInfo);
        if($pageId){
          $this->loadTable("contents");

          for($ctr=0; $ctr < count($pagedata['language']); $ctr++){
            if(isset($pagedata['use_raw_html'][$ctr])){
                $use_raw_html = 'yes';
                $details = mysql_real_escape_string(stripslashes($pagedata['description_raw'][$ctr]));
                $story = mysql_real_escape_string(stripslashes($pagedata['story_raw'][$ctr]));
            }else{
                $use_raw_html = 'no';
                $pageDesc = str_replace("\n", '', $pagedata['description'][$ctr]);
                $details = mysql_real_escape_string(stripslashes($pageDesc));
                $pageStory = str_replace("\n", '', $pagedata['story'][$ctr]);
                $story = mysql_real_escape_string(stripslashes($pageStory));
            }
            $contentInfo = array(
                'language_id'            => $pagedata['language'][$ctr],
                'ref_id'                 => $pageId,
                'title'                  => $pagedata['title'][$ctr],
                'nav_title'              => $pagedata['nav_title'][$ctr],
                'use_raw_html'           => $use_raw_html,
                'details'                => $details,
                'rel_story'              => $story
            );
            $this->insert($contentInfo);
          }
          $this->loadTable("pages_photos");
          if(isset($pagedata['image_check'])){
                for($ctr=0; $ctr < count($pagedata['image_check']); $ctr++){
                $pagePhotoInfo = array(
                    'page_id'            => $pageId,
                    'photo_id'           => $pagedata['image_check'][$ctr]
                );

                if($pagePhotoInfo['photo_id'] == $pagedata['innarPageBanner']){
                    $pagePhotoInfo['page_banner'] = '1';
                }else{
                    $pagePhotoInfo['page_banner'] = '0';
                }
                $this->insert($pagePhotoInfo);
            }
          }
        }

        return $pageId;
    }

    private function _sanitizeText($text)
    {
	// Get rid of any html tags
	$title = strip_tags($text);

	// Preserve escaped octets.
	$title = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '---$1---', $title);

	// Remove percent signs that are not part of an octet.
	$title = str_replace('%', '', $title);

	// Restore octets.
	$title = preg_replace('|---([a-fA-F0-9][a-fA-F0-9])---|', '%$1', $title);

	$title = strtolower($title);
	$title = preg_replace('|/+|', '-', $title);
	$title = preg_replace('/&.+?;/', '', $title); // kill entities
	$title = preg_replace('/[^%a-z0-9 _-]/', '', $title);
	$title = preg_replace('/\s+/', '-', $title);
	$title = preg_replace('|-+|', '-', $title);
	$title = trim($title, '-');

	return $title;
    }
    public function getSelectedPage($pageId)
    {
        if(ctype_digit("$pageId")) {
            
       
            $sql = "SELECT      id, parent, title, meta_keyword, meta_description, page_color, 
                                page_column, page_order, menu_link
                    FROM        pages
                    WHERE       id = $pageId";
            //echo $sql;
            $results = $this->query($sql);

            if ($results) {
                return $results->result();
            }
            return false;
        } else {
            return false;
        }
    }

    public function getPageContent($pageId, $type)
    {
        $sql = "SELECT      c.id, language_id,  nav_title, c.title as con_title, details, rel_story, l.title as lan_title, c.use_raw_html
                FROM        contents c
                JOIN        languages l on l.id=c.language_id
                WHERE       ref_id = $pageId and ref_type in ('$type')";

        //echo $sql;
        $results = $this->query($sql);

        if ($results) {
            return $results->result();
        }
        return false;
    }

    public function getNewsAndEventsPageContent($pageId, $type)
    {
        if($pageId)
        {
            $sql = "SELECT      c.id, language_id,  nav_title, c.title as con_title, details, l.title as lan_title, c.use_raw_html, c.ref_type
                    FROM        contents c
                    JOIN        languages l on l.id = c.language_id
                    WHERE       ref_id = $pageId and ref_type in ($type)";

            //echo $sql;
            $results = $this->query($sql);

            if ($results) {
                return $results->result();
            }
            return false;
        } else {
            return false;
        }
    }

    public function updatePageInfo($pagedata)
    {
        $metaDescription = mysql_real_escape_string($pagedata['meta_description']);
        $sql = "UPDATE pages
                SET    parent           = '$pagedata[parent]',
                       title            = '$pagedata[page_title]',
                       meta_keyword     = '$pagedata[meta_keyword]',
                       meta_description = '$metaDescription',
                       page_color       = '$pagedata[page_color]',
                       page_column      = '$pagedata[page_column]',
                       page_order       = '$pagedata[page_order]',
                       menu_link        = '$pagedata[menu_link]'

                WHERE  id = $pagedata[pageId]";
        $updatePage = $this->query($sql);

        if($updatePage){
          $this->loadTable("contents");

          for($ctr=0; $ctr < count($pagedata['language']); $ctr++){
              $nav_title = $pagedata['nav_title'][$ctr];
              $title = $pagedata['title'][$ctr];
              if(isset($pagedata['use_raw_html'][$ctr])){
                $use_raw_html = 'yes';
                $details = mysql_real_escape_string($pagedata['description_raw'][$ctr]);
                $story = mysql_real_escape_string($pagedata['story_raw'][$ctr]);
              }else{
                $use_raw_html = 'no';
                $pageDesc = str_replace('\n', '', $pagedata['description'][$ctr]);
                $details = mysql_real_escape_string(stripslashes($pageDesc));
                $pageStory = str_replace('\n', '', $pagedata['story'][$ctr]);
                $story = mysql_real_escape_string(stripslashes($pageStory));
              }
              $contentId = $pagedata['contentId'][$ctr];
               $sql = "UPDATE  contents
                SET         nav_title       = '$nav_title',
                            title           = '$title',
                            use_raw_html    = '$use_raw_html',
                            details         = '$details',
                            rel_story           = '$story'
                WHERE       id = $contentId";
            $this->query($sql);
          }

          $return = $this->deletePagesPhoto($pagedata['pageId']);
          if($return && isset($pagedata['image_check'])){
            for($ctr=0; $ctr < count($pagedata['image_check']); $ctr++){
                $pagePhotoInfo = array(
                    'page_id'            => $pagedata['pageId'],
                    'photo_id'           => $pagedata['image_check'][$ctr]
                );
                if($pagePhotoInfo['photo_id'] == $pagedata['innarPageBanner']){
                    $pagePhotoInfo['page_banner'] = '1';
                }else{
                    $pagePhotoInfo['page_banner'] = '0';
                }
                $this->insert($pagePhotoInfo);
            }
          }
        }
    }

    public function deletePage($pageId)
    {
        $this->deletePagesPhoto($pageId);
        $sql = "DELETE  FROM contents
                    WHERE   ref_id = $pageId
                    AND     ref_type = 'page'";
        $return = $this->query($sql);
        if($return){
            $this->loadTable("pages");
            $this->remove($pageId);
            return true;
        }
        
        return false;
    }

    public function loadFormValidationRulesForPage()
    {
        $this->form_validation->set_rules('page_title','Page Title','required');
        $this->form_validation->set_rules('title[0]','Title','required');
        $this->form_validation->set_rules('description[0]','Description','required');
        $this->form_validation->set_rules('nav_title[0]','Navigation Title','required');

    }

    function deletePagesPhoto($pageId)
    {
        $this->loadTable("pages_photos");
        $sql = "DELETE  FROM pages_photos
                WHERE   page_id = $pageId";
        $return = $this->query($sql);
        if($return){
            return true;
        }
        return false;
    }


    public function getpageNavegation($language)
    {
        $sql = "SELECT      ref_id,nav_title,p.slug
                FROM        contents c
                JOIN        languages l  ON c.language_id = l.id and l.code = '$language'
                JOIN        pages p on p.id = c.ref_id and p.parent= 0
                WHERE       ref_type = 'page' AND p.menu_link = 'yes'
                GROUP BY    ref_id
                ORDER BY    p.page_order, p.id";
        $result = $this->query($sql);
        if($result){
            return $result->result();
        }
        return false;
    }

    public function getSubNavegation($pageId, $language)
    {
        $sql = "SELECT      ref_id, nav_title, p.parent, p.slug
                FROM        contents c
                JOIN        languages l  ON c.language_id = l.id and l.code = '$language'
                JOIN        pages p on p.id = c.ref_id and p.parent=$pageId
                WHERE       ref_type = 'page' AND p.menu_link = 'yes'
                GROUP BY    ref_id";
        $result = $this->query($sql);
        if($result){
            return $result->result();
        }
        return false;
    }

    public function getPageColor($pageId = '')
    {
        if($pageId == ''){
            return '#000000';
        }
        $this->loadTable("pages");
        $color = $this->field("id = $pageId", 'page_color', 'page_color');
        if(empty($color)){
            $parentId = $this->field("id = $pageId", 'parent', 'parent');
            if(empty($parentId)){
                return '#000000';
            }
            return $this->field("id = $parentId", 'page_color', 'page_color');
        }
        return $color;
    }
    public function getPageDetails($pageId , $language)
    {
        if($pageId == ''){
            return false;
        }
        $sql = "SELECT      c.title, c.details, c.rel_story
                FROM        contents c
                JOIN        pages p on p.id=c.ref_id
                JOIN        languages l  ON c.language_id = l.id and l.code = '$language'
                WHERE       ref_id = $pageId AND ref_type = 'page'
                GROUP BY    ref_id";
        
        $result = $this->query($sql);
        if($result){
            return $result->result();
        }
        return false;
    }
    
    public function getPageTitle($pageId, $languageCode = 'en')
    {
        $this->loadTable("languages");
        $languageId = $this->field("code = '$languageCode'", "id");
        
        $sql = "SELECT      p.id, c.nav_title, p.slug, p.parent, p.page_color,p.title
                FROM        pages p
                JOIN        contents c ON c.ref_id=p.id AND c.ref_type = 'page'
                JOIN        languages l ON l.id=c.language_id AND l.id=$languageId
                WHERE       p.id = $pageId";
        $result = $this->query($sql)->row_array();
        if($result){
            return $result;
        }
        return false;
    }

    public function getProgrameImages()
    {
        $sql = "SELECT         pa.id,prog.name as programe_name, prog.image_path,pa.slug,
                               pa.page_color
                FROM           programe prog JOIN pages pa ON prog.page_id = pa.id
                WHERE          prog.status = 'active'";
        $result = $this->query($sql)->result_array();
        if($result){
            return $result;
        }

        return false;
    }

    public function getPagesPhotosPath($pageId)
    {
        $sql = "SELECT      pp.photo_id, ph.path, pp.page_banner
                FROM        pages_photos pp
                JOIN        photos ph ON ph.id=pp.photo_id
                WHERE       pp.page_id=$pageId";
        $result = $this->query($sql);
        if($result){
            return $result->result();
        }
        return false;
    }

    public function getSubPageNavegation($language)
    {
        $sql = "SELECT      p.id, p.parent, c.nav_title, p.slug
                FROM        pages p
                JOIN        contents c ON c.ref_id=p.id and c.ref_type='page'
                JOIN        languages l on l.id=c.language_id and l.code='$language' AND p.menu_link = 'yes'
                WHERE       p.parent != 0
                ORDER BY    p.page_order, p.id";
        $result = $this->query($sql)->result_array();

        if($result){
            $groupParent = array();
            foreach($result as $subNavigation){
                $groupParent[$subNavigation['parent']][] = $subNavigation;
            }
            return $groupParent;
        }
        return false;
    }

    public function getSettingsPage($pageId = '0', $lang = 'en')
    {
         $sql = "SELECT     c.title, c.details, p.slug, ph.path
                FROM        contents c
                JOIN        pages p on p.id = c.ref_id
                JOIN        languages l  ON c.language_id = l.id and l.code = '{$lang}'
                LEFT JOIN   pages_photos pp ON p.id = pp.page_id
                LEFT JOIN   photos ph ON pp.photo_id = ph.id
                WHERE       ref_id = {$pageId} AND ref_type = 'page'
                GROUP BY    ref_id";

        $results = $this->query($sql)->result_array();

        if ($results) {
            return $results;
        }
        return false;
        
    }

    public function getRelPageDesc($pageId, $language, $isMain = false)
    {
        $isParent = $this->_checkParent($pageId);

        if($isParent || $isMain){
            $sql = $this->generateQueryForRelStory('p.parent = '.$pageId, $language);
        }else{
            $parent = $this->find("id = $pageId", 'parent');
            $sisPage  = $this->find("parent = $parent[parent] and id <> $pageId", 'id');
            if(is_array($sisPage)){
                $sql = $this->generateQueryForRelStory('p.parent = '.$parent['parent'].' and p.id <> '.$pageId, $language);
            }else{
                $sql = $this->generateQueryForRelStory('p.id = '.$parent['parent'].' and p.id <> '.$pageId, $language);
            }
        }
        $result = $this->query($sql);
        if($result){
            return $result->result();
        }
        return false;
    }

    private function _checkParent($pageId)
    {
        $sql = "SELECT      id
                FROM        pages
                WHERE       parent = $pageId";
        $result = $this->query($sql)->result_array();
        if($result){
            return true;
        }
        return false;
    }

    function getRootParent($pageId)
    {
        $sql = "SELECT      id, parent
                FROM        pages
                WHERE       id = $pageId";
        $result = $this->query($sql)->result_array();

        if ($result[0]['parent']==0)
        {
            return $result[0]['id'];
        }else{
            $parent = $this->getRootParent($result[0]['parent']);
        }

        return $parent;
    }

    public function checkInner($pageId)
    {
        $id     = $pageId;
        $parent = -1;
        $title  = '';
        while($parent != 0){
            $page = $this->find('id = '.$id, 'parent,upper(title) as title');

            $title  = $page['title'];
            $id     = $page['parent'];
            $parent = $page['parent'];
        }
        if($title == 'PROGRAMME' || $title == 'HOME'){
            return false;
        }else{
            return true;
        }
    }

    public function generateQueryForRelStory($condition, $language, $currentPage = 0)
    {
        $sql = "SELECT          p.id, p.title, p.slug, ph.path, c.details
                FROM            pages p
                JOIN            contents c ON c.ref_id = p.id AND ref_type = 'page'
                JOIN            languages l ON l.id = c.language_id AND l.code = '$language'
                LEFT JOIN       pages_photos pp ON pp.page_id = p.id
                LEFT JOIN       photos ph ON ph.id = pp.photo_id
                WHERE           $condition
                GROUP BY        p.id
                ORDER BY        RAND()
                LIMIT           3";

        return $sql;
    }
    /*
     *Method to get the Additional Pages photots
     * for banner
     * @paran:  string
     * @return: array
    */
    public function getInnerPagePhoto($condition)
    {
        $sql = "SELECT      s.key, p.path
                FROM        settings s
                JOIN        photos p ON s.value=p.id
                WHERE       s.key in($condition)";
        $result = $this->query($sql);
        if($result){
            return $result->result();
        }
        return false;
    }

    public function getIdBySlug($slug)
    {
        $page = $this->find(array('slug' => $slug));

        if ($page !== false) {
            return $page['id'];
        }
    }

}
<?php if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class Medias extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->loadTable('medias');
    }

    public function getAll($start, $total, $orderBy = "id")
    {
        if ($orderBy == 'title')
        {
            $sort = 'ASC';
        }else{
            $sort = 'DESC';
        }
        
        $sql = "SELECT      m.id, m.title, m.description, m.path, m.status, m.modify_date

                FROM        medias m
                ORDER BY    $orderBy $sort
                LIMIT       $start, $total";

        $results = $this->query($sql);

        if ($results) {
            return $results->result();
        }

        return false;
    }

    public function getMedia($id)
    {
        return $this->find(array("id"=>$id));
    }

    public function updateMedia($data, $id)
    {
        return $this->save($data, $id);
    }

    public function deleteMedia($id)
    {
        return $this->remove($id);
    }

}
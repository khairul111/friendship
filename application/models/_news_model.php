<?php

class News_Model extends MY_Model {
    public function __construct() {
        parent::__construct();
        $this->loadTable("news");
    }

    public function getAllNews($start, $total, $orderBy = "id", $sort = 'ASC') {
//SELECT      id, slug, status, create_date, news_order FROM        news

        $sql = "SELECT      n.id, n.slug, n.status, n.create_date, n.event_date, n.news_order, c.ref_type, c.title
                FROM        news n
                JOIN        contents c ON n.id = c.ref_id
                JOIN        languages l ON c.language_id = l.id
                WHERE       l.code = 'en' AND c.ref_type <> 'page' AND c.title <> ''
                ORDER BY    $orderBy $sort
                LIMIT       $start, $total";
        $results = $this->query($sql);

        if ($results) {
            return $results->result();
        }

        return false;
    }

    public function getLatestNews($total = 5, $lang = 'en') {
        $sql = "SELECT      n.id, n.slug, c.title, c.details, n.create_date
                FROM        news n
                JOIN        contents c ON n.id = c.ref_id
                JOIN        languages l ON c.language_id = l.id
                WHERE       l.code = '$lang' AND c.ref_type = 'news' AND c.title <> '' AND n.status='active'
                ORDER BY    n.create_date DESC
                LIMIT       $total";

        $results = $this->query($sql);

        if($results && $lang == 'bn'){
            $sql = "SELECT      n.id, n.slug, c.title, c.details
                FROM        news n
                JOIN        contents c ON n.id = c.ref_id
                JOIN        languages l ON c.language_id = l.id
                WHERE       l.code = 'en' AND c.ref_type = 'news' AND c.title <> '' AND n.status='active'
                ORDER BY    n.create_date DESC
                LIMIT       $total";
            $results = $this->query($sql);
            return $results->result();
        }
        if ($results) {
            return $results->result();
        }

        return false;
    }

    public function getNewsPhotos($newsId)
    {
        $this->loadTable("news_photos");
        return $this->generateSingleArray("news_id = $newsId", 'photo_id', null);
    }

    public function getNewsPhotosPath($pageId)
    {
        $sql = "SELECT      ph.path
                FROM        news_photos np
                JOIN        photos ph ON ph.id = np.photo_id
                WHERE       np.news_id=$pageId";
        $result = $this->query($sql);
        if($result){
            return $result->result();
        }
        return false;
    }

    public function getNewsDetails($id, $lang = 'en') {
        $sql = "SELECT      n.id, n.slug, n.create_date, c.title, c.details
                FROM        news n
                LEFT JOIN   contents c ON n.id = c.ref_id AND (c.ref_type='news' OR c.ref_type='events')
                JOIN        languages l ON c.language_id = l.id
                WHERE       l.code = '$lang' AND n.id = $id
            ";

        $results = $this->query($sql);

        if ($results) {
            return $results->result();
        }

        return false;
    }

    public function saveNewsInfo($newsdata) {
        $date = date('Y-m-d h:i:s');
        $slug = $this->_sanitizeText($newsdata['title'][0]);
        if ($newsdata['news_order']){
            $newsOrder = $newsdata['news_order'];
        }else{
            $newsByLastOrder = $this->getAllNews(0, 1, 'news_order', 'DESC');
            $newsOrder = $newsByLastOrder[0]->news_order+1;
        }

        $newsInfo = array(
            'slug'               => $slug,
            'status'             => $newsdata['status'],
            'news_order'         => $newsOrder,
            'active_date'        => $date,
            'event_date'        =>  $newsdata['event_date']
        );

        $newsId = $this->insert($newsInfo);

        if($newsId) {
            $this->loadTable("contents");

            for($ctr=0; $ctr < count($newsdata['language']); $ctr++) {
                $contentInfo = array(
                    'language_id'            => $newsdata['language'][$ctr],
                    'ref_id'                 => $newsId,
                    'ref_type'               => $newsdata['ref_type'],
                    'title'                  => $newsdata['title'][$ctr],
                    'details'                => mysql_real_escape_string($newsdata['description'][$ctr])
                );

                $this->insert($contentInfo);
            }
            $this->loadTable("news_photos");
            if(isset($newsdata['image_check'])){
                foreach($newsdata['image_check'] as $photoId){
                    $newsPhotoInfo = array(
                        'news_id'            => $newsId,
                        'photo_id'           => $photoId
                    );
                    $this->insert($newsPhotoInfo);
                }
            }

        }

    }

    private function _sanitizeText($text)
    {
    // Get rid of any html tags
        $title = strip_tags($text);

        // Preserve escaped octets.
        $title = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '---$1---', $title);

        // Remove percent signs that are not part of an octet.
        $title = str_replace('%', '', $title);

        // Restore octets.
        $title = preg_replace('|---([a-fA-F0-9][a-fA-F0-9])---|', '%$1', $title);

        $title = strtolower($title);
        $title = preg_replace('|/+|', '-', $title);
        $title = preg_replace('/&.+?;/', '', $title); // kill entities
        $title = preg_replace('/[^%a-z0-9 _-]/', '', $title);
        $title = preg_replace('/\s+/', '-', $title);
        $title = preg_replace('|-+|', '-', $title);
        $title = trim($title, '-');

        return $title;
    }

    public function getSelectedNews($newsId)
    {
        if ($newsId && $newsId != '') {
            $sql = "SELECT      id, slug, status, news_order, event_date
                FROM        news
                WHERE       id = $newsId";

            $results = $this->query($sql);

            if ($results) {
                return $results->result();
            }

            return false;

            }
    }

    public function updateNewsInfo($newsdata) {

        if ($newsdata['news_order']){
            $newsOrder = $newsdata['news_order'];
        }else{
            $newsByLastOrder = $this->getAllNews(0, 1, 'news_order', 'DESC');
            $newsOrder = $newsByLastOrder[0]->news_order+1;
        }

        $sql = "UPDATE  news
                SET     slug            = '$newsdata[slug]',
                        status          = '$newsdata[status]',
                        news_order      = '$newsOrder',
                        event_date      = '$newsdata[event_date]'

                WHERE   id              = $newsdata[newsId]";

        //print($sql);
        $updatePage = $this->query($sql);

        if($updatePage) {
            $this->loadTable("contents");

            for($ctr=0; $ctr < count($newsdata['language']); $ctr++) {
                $title = mysql_real_escape_string($newsdata['title'][$ctr]);
                $details = mysql_real_escape_string($newsdata['description'][$ctr]);
                $contentId = $newsdata['contentId'][$ctr];

                $sql = "UPDATE  contents
                    SET     title   = '$title',
                            details = '$details',
                            ref_type = '{$newsdata['ref_type']}'
                    WHERE   id      = $contentId";
                $this->query($sql);
            }
            $return = $this->deleteNewsPhoto($newsdata['newsId']);
            if($return && isset($newsdata['image_check'])){
                for($ctr=0; $ctr < count($newsdata['image_check']); $ctr++){
                    $newsPhotoInfo = array(
                        'news_id'            => $newsdata['newsId'],
                        'photo_id'           => $newsdata['image_check'][$ctr]
                    );
                    $this->insert($newsPhotoInfo);
                }
            }
        }

    }

    public function deleteNews($newsId) {
        $return = $this->remove($newsId);

        if($return) {
            $sql = "DELETE  FROM contents
                    WHERE   ref_id = $newsId
                    AND     ref_type = 'news'";
            $this->query($sql);
            return true;
        }

        return false;
    }

    public function loadFormValidationRulesForNews() {
        $this->form_validation->set_rules('news_order','News Order','is_natural_no_zero');
        $this->form_validation->set_rules('title[0]','Title','required');
        $this->form_validation->set_rules('description[0]','Description','required');
        //$this->form_validation->set_rules('event_date','Description','exact_length[10]');
    }

    function deleteNewsPhoto($newsId)
    {
        $this->loadTable("news_photos");
        $sql = "DELETE  FROM news_photos
                WHERE   news_id = $newsId";
        $return = $this->query($sql);
        if($return){
            return true;
        }
        return false;
    }

    public function getRecentNews($total = 5, $lang = 'en')
    {
        $sql = "SELECT      n.id, n.slug, c.title as news_title, c.details, p.path, p.title
                FROM        news n
                JOIN        contents c ON n.id = c.ref_id
                JOIN        languages l ON c.language_id = l.id
                LEFT JOIN   news_photos np ON n.id = np.news_id
                LEFT JOIN   photos p ON np.photo_id = p.id
                WHERE       l.code = '{$lang}' AND c.ref_type = 'news' AND c.title <> '' AND n.status='active'
                GROUP BY    n.id
                ORDER BY    n.news_order, n.id
                LIMIT       $total";
        
        $results = $this->query($sql)->result_array();
        
        if ($results) {
            return $results;
        }

        return false;
        
    }

    public function getRecentEvents($total = 5, $lang = 'en')
    {
        $sql = "SELECT      n.id, n.slug, c.title as event_title, c.details, p.path, p.title
                FROM        news n
                JOIN        contents c ON n.id = c.ref_id
                JOIN        languages l ON c.language_id = l.id
                LEFT JOIN   news_photos np ON n.id = np.news_id
                LEFT JOIN   photos p ON np.photo_id = p.id
                WHERE       l.code = '{$lang}' AND c.ref_type = 'events' AND c.title <> '' AND n.status='active'
                GROUP BY    n.id
                ORDER BY    n.news_order, n.id
                LIMIT       $total";

        $results = $this->query($sql)->result_array();
        if ($results) {
            return $results;
        }

        return false;

    }

    

}

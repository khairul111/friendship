<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Photos extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->loadTable('photos');
    }

    public function getAllByAlbumId($id)
    {
        return $this->findAll("album_id = '$id'");
    }

    public function getAllPhotos()
    {
        $return = $this->findAll(null, "id, album_id, path");
        $groupAlbum = array();

        foreach ($return as $albumPhotos) {
            $groupAlbum[$albumPhotos['album_id']][] = $albumPhotos;
        }
        return $groupAlbum;
    }

    public function uploadTempFiles($albumId, $path = "assets/photos")
    {
        $nameArr = $_FILES['photos']['name'];
        $tempArr = $_FILES['photos']['tmp_name'];
        $errArr = $_FILES['photos']['error'];
        $imgArr = Array();
        $today = date("Y-m-d");

        foreach ($errArr AS $key => $val) {
            if ($val == 0) {
                $this->setUploadConfig($path);
                if (move_uploaded_file($tempArr[$key], $path . '/' . url_title($nameArr[$key])) && $this->savePhoto(array('album_id' => $albumId, 'path' => url_title($nameArr[$key]), 'title' => $nameArr[$key], 'description' => $nameArr[$key]))) {
                    $imgArr[] = $nameArr[$key];
                }
            }
        }
        return $imgArr;
    }

    public function setUploadConfig($path = "assets/photos", $type = '')
    {
        $config['upload_path'] = $path;

        if ($type == '') {
            $config['allowed_types'] = 'gif|jpg|png';
        } elseif ($type == 'text') {
            $config['allowed_types'] = 'word';
        }

        $config['max_size'] = "102400";
        $config['max_width'] = '';
        $config['max_height'] = '';
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);
    }

    public function updatePhotos()
    {
        $ret = false;
        for ($i = 0; $i < count($_POST['ids']); $i++) {
            $data = array(
                'title' => $_POST['titles'][$i],
                'description' => $_POST['descriptions'][$i]
            );
            $ret = $this->save($data, $_POST['ids'][$i]);
        }
        return $ret;
    }

    public function savePhoto($data)
    {
        return $this->save($data);
    }

    public function delete($id)
    {
        $photo = $this->field("id = {$id}", 'path');
        $folders = glob('assets/photos/*', GLOB_ONLYDIR);
        $folders[] = 'assets/photos/';
        
        foreach ($folders as $folder) {
            $filename = $folder . '/' . $photo;
            if (file_exists($filename)) {
                unlink($filename);
            }
        }

        $result = $this->remove($id);
        return $result;
    }

    public function getHomePageRightSidePhoto($photoId)
    {
        return $this->field("id = {$photoId}", 'path');
    }
}
-- phpMyAdmin SQL Dump
-- version 2.11.8.1deb5+lenny1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 31, 2009 at 11:44 AM
-- Server version: 5.0.51
-- PHP Version: 5.2.9-0.dotdeb.2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `staging_friendship`
--

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE IF NOT EXISTS `albums` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status` enum('active','inactive') NOT NULL default 'active',
  `cover_photo_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `contents`
--

CREATE TABLE IF NOT EXISTS `contents` (
  `id` int(11) NOT NULL auto_increment,
  `language_id` int(11) NOT NULL,
  `ref_id` int(11) NOT NULL,
  `ref_type` enum('page','news') NOT NULL default 'page',
  `nav_title` varchar(100) NOT NULL,
  `title` varchar(200) NOT NULL,
  `details` text NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_contents` (`language_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(25) NOT NULL,
  `code` varchar(4) NOT NULL,
  `status` enum('active','inactive') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `medias`
--

CREATE TABLE IF NOT EXISTS `medias` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) default NULL,
  `description` varchar(512) default NULL,
  `path` varchar(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL default 'active',
  `modify_date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status` enum('active','inactive') NOT NULL default 'active',
  `create_date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `news_photos`
--

CREATE TABLE IF NOT EXISTS `news_photos` (
  `news_id` int(11) unsigned NOT NULL,
  `photo_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL auto_increment,
  `parent` int(11) NOT NULL default '0',
  `title` varchar(200) NOT NULL,
  `meta_keyword` varchar(200) default NULL,
  `meta_description` text,
  `status` enum('active','inactive') NOT NULL default 'active',
  `page_color` varchar(25) NOT NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `pages_photos`
--

CREATE TABLE IF NOT EXISTS `pages_photos` (
  `page_id` int(11) NOT NULL,
  `photo_id` int(11) NOT NULL,
  KEY `FK_pages_photos_page` (`page_id`),
  KEY `FK_pages_photos_photos` (`photo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) default NULL,
  `email` varchar(100) default NULL,
  `address_street` varchar(100) default NULL,
  `address_state` varchar(100) default NULL,
  `address_city` varchar(100) default NULL,
  `address_country` varchar(100) default NULL,
  `transaction_id` varchar(100) default NULL,
  `amount` int(11) default NULL,
  `status` enum('pending','approved','inapproved') default 'pending',
  `modified` timestamp NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(11) NOT NULL auto_increment,
  `album_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `path` varchar(200) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_photos` (`album_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `programe`
--

CREATE TABLE IF NOT EXISTS `programe` (
  `id` tinyint(4) unsigned NOT NULL auto_increment,
  `page_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `image_path` varchar(150) NOT NULL,
  `status` enum('active','inactive') NOT NULL default 'active',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL auto_increment,
  `key` varchar(255) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(127) NOT NULL,
  `password` varchar(32) NOT NULL,
  `email` varchar(127) NOT NULL,
  `status` enum('active','inactive') NOT NULL default 'active',
  PRIMARY KEY  (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `contents`
--
ALTER TABLE `contents`
  ADD CONSTRAINT `FK_contents` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`);

--
-- Constraints for table `pages_photos`
--
ALTER TABLE `pages_photos`
  ADD CONSTRAINT `FK_pages_photos_page` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`),
  ADD CONSTRAINT `FK_pages_photos_photos` FOREIGN KEY (`photo_id`) REFERENCES `photos` (`id`);

--
-- Constraints for table `photos`
--
ALTER TABLE `photos`
  ADD CONSTRAINT `FK_photos` FOREIGN KEY (`album_id`) REFERENCES `albums` (`id`);

--
-- New Fields for table `pages`
--
ALTER TABLE `pages` ADD `slug` VARCHAR( 255 ) NOT NULL AFTER `title`

--
-- New Fields for table `pages`
--
ALTER TABLE `pages` ADD `page_column` ENUM( 'one column', 'two column left side-bar', 'two column right side-bar' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'one column' AFTER `page_color`

--
-- New Fields for table `pages`
--
ALTER TABLE `contents` ADD `use_raw_html` ENUM( 'yes', 'no' ) NOT NULL AFTER `title`


--
-- New Fields for table `pages_photos`
--
ALTER TABLE `pages_photos` ADD `page_banner` INT( 4 ) UNSIGNED NOT NULL

--
-- New table `feedback`
--
CREATE TABLE `friendshipcms`.`feedback` (
`id` INT( 11 ) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`name` VARCHAR( 100 ) NOT NULL ,
`email` VARCHAR( 100 ) NOT NULL ,
`subject` VARCHAR( 100 ) NOT NULL ,
`msg` TEXT NOT NULL
) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

--
-- New Fields for table `pages`
--
 ALTER TABLE `pages` ADD `page_order` INT( 4 ) NOT NULL AFTER `page_column`

--
-- Table structure for table `external_user`
--

CREATE TABLE IF NOT EXISTS `external_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstName` varchar(50) NOT NULL,
  `lastName` varchar(50) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

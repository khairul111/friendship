-- phpMyAdmin SQL Dump
-- version 3.1.2deb1ubuntu0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 27, 2009 at 07:13 AM
-- Server version: 5.0.75
-- PHP Version: 5.2.6-3ubuntu4.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `friendshipcms`
--

-- --------------------------------------------------------

--
-- Table structure for table `programe`
--

CREATE TABLE IF NOT EXISTS `programe` (
  `id` tinyint(4) unsigned NOT NULL auto_increment,
  `page_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `image_path` varchar(150) NOT NULL,
  `status` enum('active','inactive') NOT NULL default 'active',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `programe`
--

INSERT INTO `programe` (`id`, `page_id`, `name`, `image_path`, `status`) VALUES
(1, 0, 'dfsdfsd', 'bullet_blue.png', 'active'),
(2, 1, 'gffg', 'but_round_del_green.gif', 'active');


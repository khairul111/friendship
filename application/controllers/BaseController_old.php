<?php

/**
 * Base Controller
 *
 * Common tasks of all controllers are done here
 * Must be inherited, no direct instance allowed (abstract)
 *
 * @package     Friendship-cms
 * @category    Controller
 * @author      Nurul Ferdous
 */

abstract class BaseController extends Controller
{
    protected $data = array();
    public $_language = "en";

    public function __construct()
    {
        parent::Controller();
        //print_r($_COOKIE);die();
        if(isset($_COOKIE['site_lang'])){
            $this->_language = strtolower($_COOKIE['site_lang']);
            $this->data['language'] = $this->_language;
        }
        if($this->_language == 'bn'){
            $this->lang->load('site', 'bangla');
        }else{
            $this->lang->load('site', 'english');
        }
        
        $this->load->library('layout');
        $this->layout->setLayout('layouts/layout_main');
        $this->_loadSettings();
        $this->data['windowTitle'] = $this->data['settings']['SITE_NAME'];
        $this->breadcrumb->setHomeText($this->lang->line('MENU_HOME'));
    }

    private function _loadSettings()
    {
        $this->load->model("Settings");
        $this->load->model("Photos");
        $this->_createPageNavigation();
        $this->data['settings'] = $this->Settings->getAll();
        $this->data['html_body_id'] = 'home';
        $this->data['mapImage'] = $this->Photos->getHomePageRightSidePhoto($this->data['settings']['HOMEPAGE_MAP']);
    }

    private function _createPageNavigation()
    {
        $this->load->model("Pages");
        $this->data['contactPageId'] = $this->Pages->find("upper(title)='CONTACT US'", 'id');
        $this->data['subNavigationPage'] = $this->Pages->getSubPageNavegation($this->_language);
        $this->data['navigationPage'] = $this->Pages->getpageNavegation($this->_language);
        //die(print_r($this->data['subNavigationPage']));
    }
}
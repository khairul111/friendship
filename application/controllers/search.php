<?php

include APPPATH . "controllers/BaseController.php";

class Search extends BaseController {
    public $perPage = 10;
    public function __construct() {
        parent::__construct();
        $this->data['html_body_id'] = 'file';
        $this->data['msg']['body'] = '';
    }

    public function index() {
        $this->data['windowTitle'] = $this->data['settings']['SITE_NAME'].' - Search - Results';
        $this->data['breadCrumb'] = 'Search - Results';
        $this->data['pageTitle'] = 'Search - Results';

        $view = $this->layout->view('search', $this->data, TRUE);
        $replaces = array(
            '{FLASH_MSG_CONT}'       => $this->load->view('common/flash_msg_cont', NULL, TRUE),
            '{BANNER_DIV}'      => ''
        );
        $this->load->view('view', array('view' => $view, 'replaces' => $replaces));
    }

}
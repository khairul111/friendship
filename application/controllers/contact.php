<?php

include APPPATH . "controllers/BaseController.php";

class Contact extends BaseController {

    public function __construct()
    {
        parent::__construct();
        $this->data['html_body_id'] = 'contact';
        $this->data['pageTitle'] = $this->lang->line('MENU_CONTACT');
    }

    public function index()
    {
        $this->data['windowTitle'] = $this->lang->line('MENU_CONTACT').' - '.$this->data['settings']['SITE_NAME'];
        $this->breadcrumb->addCrumb($this->lang->line('MENU_CONTACT'));
        $this->data['breadCrumb'] = $this->breadcrumb->makeBread();

        $this->load->library('form_validation');
        $val = $this->form_validation;
        $val->set_error_delimiters('<p class="error">', '</p>');

        // Set form validation rules
        $val->set_rules('username', 'Name', 'trim|required|xss_clean|max_length[200]');
        $val->set_rules('subject', 'Subject', 'trim|required|xss_clean|max_length[200]');
        $val->set_rules('email', 'E-Mail', 'trim|required|valid_email|xss_clean|max_length[200]');
        $val->set_rules('msg', 'Message', 'trim|required');

        // Run form validation
        if($valRun = $val->run()) {
            $data = array(
                'sender'    => $val->set_value('username'),
                'subject'   => $val->set_value('subject'),
                'email'     => $val->set_value('email'),
                'body'      => $val->set_value('msg')
            );
        }
        //print_r($data);die;
        // save data if it's pass the validation
        if ($valRun) {
            sendEmail( $data['body'], $data['email'], $data['subject'] ,$this->data['settings']['SITE_CONTACT_EMAIL_ADDRESS']);
            $this->session->set_flashdata('status_message', 'Your message has been sent successfully. We will get back to you soon.');
            redirect('/');
        }

        $view = $this->layout->view('contact', $this->data, TRUE);
        $replaces = array(
            '{FLASH_MSG_CONT}' => $this->load->view('common/flash_msg_cont_front', NULL, TRUE),
            '{BANNER_DIV}'      => ''
        );
        $this->load->view('view', array('view' => $view, 'replaces' => $replaces));
    }

    public function feedback()
    {
        $this->load->library('form_validation');

        $this->data['html_body_id'] = 'feedback';
        $this->data['pageTitle']    = 'Feedback';
        $this->data['html_body_id'] = 'feedback';
        $this->load->model("Pages");
        $this->data['innerPage']         = true;
        $this->data['pagesPhotos']       = $this->Pages->getInnerPagePhoto("'FEEDBACK_LEFT', 'FEEDBACK_RIGHT'");
        foreach($this->data['pagesPhotos'] as $photo){
            if(strstr($photo->key, 'LEFT')){
                $this->data['bannerPathLeft'] = $photo->path;
            }else{
                $this->data['bannerPathRight']= $photo->path;
            }
        }

        $this->_loadFormValidationRulesForFeedback();

        if($this->form_validation->run() == false){
            $view = $this->layout->view('feedback', $this->data, TRUE);
            $replaces = array(
            '{FLASH_MSG_CONT}' => $this->load->view('common/flash_msg_cont_front', NULL, TRUE),
            '{BANNER_DIV}'      => ''
            );
            $this->load->view('view', array('view' => $view, 'replaces' => $replaces));
        }else{
            $this->load->model("Feedbacks");
            $isSaved = $this->Feedbacks->save($_POST);
            if($isSaved){
                $this->session->set_flashdata('status_message', 'Your message has been sent successfully. We will get back to you soon.');
                redirect('/');
            }else{
                $this->session->set_flashdata('status_message', 'Your message is not sent successfully. Please try again.');
                $view = $this->layout->view('feedback', $this->data, TRUE);
                $replaces = array(
                '{FLASH_MSG_CONT}' => $this->load->view('common/flash_msg_cont_front', NULL, TRUE),
                '{BANNER_DIV}'      => ''
                );
            }
       }
    }

    public function newsletter()
    {
        $this->load->model("Users");
        $userData = $_POST;
        
        $this->load->library('form_validation');
        $val = $this->form_validation;
        $val->set_error_delimiters('<p class="error">', '</p>');

        // Set form validation rules
        $val->set_rules('email', 'E-Mail', 'trim|valid_email|xss_clean|max_length[200]');
        if($this->form_validation->run() == false){
            $data['status'] = 'error';
            $data['message'] = "Email is invalid.";
        }else{            
            if($this->Users->saveExternal($userData, &$messge)){
                $mailBody = "Dear ".$data['firstName'].' '.$userData['lastName']."<br>". "You have successfully registered for friendship newsletter
                    <br> Friendship support team.";
                $isSend = sendEmail( $mailBody, $userData['email'], "Friendship newsletter subscription" ,$this->data['settings']['SITE_CONTACT_EMAIL_ADDRESS']);
                
                $data['message'] = "You have successfully registered for friendship newsletter";
            }else{
                $data['message'] = $messge;
            }
                 
        }
        
        echo json_encode($data);
        exit;
    }
    private function _loadFormValidationRulesForFeedback()
    {
        $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean|max_length[200]');
        $this->form_validation->set_rules('subject', 'Subject', 'trim|required|xss_clean|max_length[200]');
        $this->form_validation->set_rules('email', 'E-Mail', 'trim|required|valid_email|xss_clean|max_length[200]');
        $this->form_validation->set_rules('msg', 'Message', 'trim|required');
        
        $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
    }
    
    public function registerExternalUser()
    {
        
        $data = array();
        $data['firstName']= $_POST['first_name'];
        $data['lastName']= $_POST['last_name'];
        $data['email']=$_POST['email'];
        $this->load->model('users');
        $this->users->saveExternalUser($data);
    }
}
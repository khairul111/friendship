<?php
include APPPATH . "controllers/BaseController.php";

class Gallery extends BaseController {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Albums");
        $this->data['html_body_id'] = 'gallery';
        $this->data['innerPage']   = false;
        $this->load->model("Pages");
    }

    public function index()
    {
        $this->home();
    }

    public function home()
    {
        $this->data['windowTitle'] = $this->data['settings']['SITE_NAME'].' - Gallery';
        $this->data['pageTitle']   = $this->lang->line('MENU_GALLERY');
        $this->data['breadCrumb']  = '<a href="/page">Home</a> > <a href="#">Photo Gallery</a>';

        $this->data['innerPage'] = false;
        $this->data['pagesPhotos']       = $this->Pages->getInnerPagePhoto("'PHOTO_LEFT', 'PHOTO_RIGHT'");
        foreach($this->data['pagesPhotos'] as $photo){
            if(strstr($photo->key, 'LEFT')){
                $this->data['bannerPathLeft'] = $photo->path;
            }else{
                $this->data['bannerPathRight']= $photo->path;
            }
        }

        $this->data['albums'] = $this->Albums->getAllActive($start = 0, $total = 10);

        $view = $this->layout->view('gallery', $this->data, TRUE);
        $replaces = array(
                '{FLASH_MSG_CONT}' => $this->load->view('common/flash_msg_cont', NULL, TRUE),
                '{BANNER_DIV}'      => ''
        );
        $this->load->view('view', array('view' => $view, 'replaces' => $replaces));
    }

    public function album($id = NULL)
    {
        if(!ctype_digit($id))redirect('');

        $this->data['innerPage'] = false;
        $this->data['pagesPhotos']       = $this->Pages->getInnerPagePhoto("'PHOTO_LEFT', 'PHOTO_RIGHT'");
        $albumData = $this->Albums->getAlbum($id);
        $this->data['breadCrumb']  = '<a href="/gallery">Photo Gallery</a> > <a href="#">'.$albumData['title'].'</a>';

        foreach($this->data['pagesPhotos'] as $photo){
            if(strstr($photo->key, 'LEFT')){
                $this->data['bannerPathLeft'] = $photo->path;
            }else{
                $this->data['bannerPathRight']= $photo->path;
            }
        }

        $this->data['pageTitle'] = $albumData['title'];

        $this->data['windowTitle'] = $albumData['title'] . ' - ' . $this->lang->line('COMMON_TEXT_ALBUM').' - '.$this->data['settings']['SITE_NAME'];
        $this->load->model('Photos');
        $this->data['photos'] = $this->Photos->getAllByAlbumId($id);
        $view = $this->layout->view('photos', $this->data, TRUE);
        $replaces = array(
                '{FLASH_MSG_CONT}' => $this->load->view('common/flash_msg_cont', NULL, TRUE),
                '{BANNER_DIV}'      => ''
        );
        $this->load->view('view', array('view' => $view, 'replaces' => $replaces));
    }

}
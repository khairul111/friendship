<?php

include APPPATH . "controllers/BaseController.php";

class News extends BaseController {

    public $_language = 'en';

    public function __construct() {
        parent::__construct();
        $this->load->model('News_model');
        $this->load->model('Pages');

        $this->data['html_body_id'] = 'news';
        $this->data['innerPage'] = true;

        $this->data['pagesPhotos'] = $this->Pages->getInnerPagePhoto("'NEWS_LEFT', 'NEWS_RIGHT'");
        foreach ($this->data['pagesPhotos'] as $photo) {
            if (strstr($photo->key, 'LEFT')) {
                $this->data['bannerPathLeft'] = $photo->path;
            } else {
                $this->data['bannerPathRight'] = $photo->path;
            }
        }
    }

    public function index() {
        $this->home();
    }

    public function home($year = null) {

        $this->data['windowTitle'] = $this->data['settings']['SITE_NAME'] . ' - News - Home';
        $this->breadcrumb->addCrumb($this->lang->line('MENU_NEWS'));
        $this->data['breadCrumb'] = $this->breadcrumb->makeBread();
        $this->data['pageTitle'] = $this->lang->line('MENU_NEWS');

        if ($year == null) {
            $this->data['news'] = $this->News_model->getLastSixMonthNews(500);
        } else {
            $this->data['news'] = $this->News_model->getNewsByYear($year);
            $this->data['flag'] = "Archives by Year " . $year;
        }

        $this->data['distinctYear'] = $this->News_model->getDistinctYear();
        $view = $this->layout->view('news_home', $this->data, TRUE);
        $replaces = array(
            '{FLASH_MSG_CONT}' => $this->load->view('common/flash_msg_cont', NULL, TRUE),
            '{BANNER_DIV}' => ''
        );

        $this->load->view('view', array('view' => $view, 'replaces' => $replaces));
    }

    public function view($slug) {
        $splitSlug = explode("-", $slug);
        $id = end($splitSlug);

        if (!ctype_digit($id)) {
            redirect('/news/home');
        }
        // @ToDo Language mod

        $this->data['news'] = $this->News_model->getNewsDetails($id);
        $this->data['windowTitle'] = $this->data['news'][0]->title . ' - ' . $this->data['settings']['SITE_NAME'];

        $this->breadcrumb->addCrumb($this->lang->line('MENU_NEWS'), '/news');
        $this->data['breadCrumb'] = $this->breadcrumb->makeBread();
        $this->data['pageTitle'] = $this->lang->line('MENU_NEWS_DETAILS');
        $this->data['newsPhotos'] = $this->News_model->getNewsPhotosPath($id);

        $view = $this->layout->view('news', $this->data, TRUE);
        $replaces = array(
            '{FLASH_MSG_CONT}' => $this->load->view('common/flash_msg_cont', NULL, TRUE),
            '{BANNER_DIV}' => ''
        );

        $this->load->view('view', array('view' => $view, 'replaces' => $replaces));
    }

}
<?php

include APPPATH . "controllers/BaseController.php";

class SiteMap extends BaseController {

    public function __construct()
    {
        parent::__construct();
    }

    public function view()
    {
        $this->data['windowTitle'] = $this->data['settings']['SITE_NAME'].' - Site Map';
        $this->data['pageTitle']   = 'Site Map';
        $this->data['html_body_id'] = 'sitemap';
        $this->load->model("Pages");
        $this->data['parentPages']       = $this->Pages->getpageNavegation($this->_language);
        $this->data['subNavigationPage'] = $this->Pages->getSubPageNavegation($this->_language);
        
        $this->data['innerPage']         = true;
        $this->data['pagesPhotos']       = $this->Pages->getInnerPagePhoto("'SITEMAP_LEFT', 'SITEMAP_RIGHT'");
        foreach($this->data['pagesPhotos'] as $photo){
            if(strstr($photo->key, 'LEFT')){
                $this->data['bannerPathLeft'] = $photo->path;
            }else{
                $this->data['bannerPathRight']= $photo->path;
            }
        }
        
        $view = $this->layout->view('sitemap', $this->data, TRUE);
        $replaces = array(
            '{FLASH_MSG_CONT}' => $this->load->view('common/flash_msg_cont', NULL, TRUE),
            '{BANNER_DIV}'      => ''
        );
        $this->load->view('view', array('view' => $view, 'replaces' => $replaces));
    }

    }
<?php

include APPPATH . "controllers/BaseController.php";

class Donate extends BaseController {
    public $_language = 'en';
    public $Paypal;

    public $countries = array(
                  "US" => "United States",
                  "GB" => "United Kingdom",
                  "CA" => "Canada",
                  "FR" => "France",
                  "--" => "---------------",
                  "AF" => "Afghanistan",
                  "AL" => "Albania",
                  "DZ" => "Algeria",
                  "AS" => "American Samoa",
                  "AD" => "Andorra",
                  "AO" => "Angola",
                  "AI" => "Anguilla",
                  "AQ" => "Antarctica",
                  "AG" => "Antigua And Barbuda",
                  "AR" => "Argentina",
                  "AM" => "Armenia",
                  "AW" => "Aruba",
                  "AU" => "Australia",
                  "AT" => "Austria",
                  "AZ" => "Azerbaijan",
                  "BS" => "Bahamas",
                  "BH" => "Bahrain",
                  "BD" => "Bangladesh",
                  "BB" => "Barbados",
                  "BY" => "Belarus",
                  "BE" => "Belgium",
                  "BZ" => "Belize",
                  "BJ" => "Benin",
                  "BM" => "Bermuda",
                  "BT" => "Bhutan",
                  "BO" => "Bolivia",
                  "BA" => "Bosnia And Herzegowina",
                  "BW" => "Botswana",
                  "BV" => "Bouvet Island",
                  "BR" => "Brazil",
                  "IO" => "British Indian Ocean Territory",
                  "BN" => "Brunei Darussalam",
                  "BG" => "Bulgaria",
                  "BF" => "Burkina Faso",
                  "BI" => "Burundi",
                  "KH" => "Cambodia",
                  "CM" => "Cameroon",
                  "CV" => "Cape Verde",
                  "KY" => "Cayman Islands",
                  "CF" => "Central African Republic",
                  "TD" => "Chad",
                  "CL" => "Chile",
                  "CN" => "China",
                  "CX" => "Christmas Island",
                  "CC" => "Cocos (Keeling) Islands",
                  "CO" => "Colombia",
                  "KM" => "Comoros",
                  "CG" => "Congo",
                  "CD" => "Congo, The Democratic Republic Of The",
                  "CK" => "Cook Islands",
                  "CR" => "Costa Rica",
                  "CI" => "Cote D'Ivoire",
                  "HR" => "Croatia (Local Name: Hrvatska)",
                  "CU" => "Cuba",
                  "CY" => "Cyprus",
                  "CZ" => "Czech Republic",
                  "DK" => "Denmark",
                  "DJ" => "Djibouti",
                  "DM" => "Dominica",
                  "DO" => "Dominican Republic",
                  "TP" => "East Timor",
                  "EC" => "Ecuador",
                  "EG" => "Egypt",
                  "SV" => "El Salvador",
                  "GQ" => "Equatorial Guinea",
                  "ER" => "Eritrea",
                  "EE" => "Estonia",
                  "ET" => "Ethiopia",
                  "FK" => "Falkland Islands (Malvinas)",
                  "FO" => "Faroe Islands",
                  "FJ" => "Fiji",
                  "FI" => "Finland",
                  "FX" => "France, Metropolitan",
                  "GF" => "French Guiana",
                  "PF" => "French Polynesia",
                  "TF" => "French Southern Territories",
                  "GA" => "Gabon",
                  "GM" => "Gambia",
                  "GE" => "Georgia",
                  "DE" => "Germany",
                  "GH" => "Ghana",
                  "GI" => "Gibraltar",
                  "GR" => "Greece",
                  "GL" => "Greenland",
                  "GD" => "Grenada",
                  "GP" => "Guadeloupe",
                  "GU" => "Guam",
                  "GT" => "Guatemala",
                  "GN" => "Guinea",
                  "GW" => "Guinea-Bissau",
                  "GY" => "Guyana",
                  "HT" => "Haiti",
                  "HM" => "Heard And Mc Donald Islands",
                  "VA" => "Holy See (Vatican City State)",
                  "HN" => "Honduras",
                  "HK" => "Hong Kong",
                  "HU" => "Hungary",
                  "IS" => "Iceland",
                  "IN" => "India",
                  "ID" => "Indonesia",
                  "IR" => "Iran (Islamic Republic Of)",
                  "IQ" => "Iraq",
                  "IE" => "Ireland",
                  "IL" => "Israel",
                  "IT" => "Italy",
                  "JM" => "Jamaica",
                  "JP" => "Japan",
                  "JO" => "Jordan",
                  "KZ" => "Kazakhstan",
                  "KE" => "Kenya",
                  "KI" => "Kiribati",
                  "KP" => "Korea, Democratic People's Republic Of",
                  "KR" => "Korea, Republic Of",
                  "KW" => "Kuwait",
                  "KG" => "Kyrgyzstan",
                  "LA" => "Lao People's Democratic Republic",
                  "LV" => "Latvia",
                  "LB" => "Lebanon",
                  "LS" => "Lesotho",
                  "LR" => "Liberia",
                  "LY" => "Libyan Arab Jamahiriya",
                  "LI" => "Liechtenstein",
                  "LT" => "Lithuania",
                  "LU" => "Luxembourg",
                  "MO" => "Macau",
                  "MK" => "Macedonia, Former Yugoslav Republic Of",
                  "MG" => "Madagascar",
                  "MW" => "Malawi",
                  "MY" => "Malaysia",
                  "MV" => "Maldives",
                  "ML" => "Mali",
                  "MT" => "Malta",
                  "MH" => "Marshall Islands",
                  "MQ" => "Martinique",
                  "MR" => "Mauritania",
                  "MU" => "Mauritius",
                  "YT" => "Mayotte",
                  "MX" => "Mexico",
                  "FM" => "Micronesia, Federated States Of",
                  "MD" => "Moldova, Republic Of",
                  "MC" => "Monaco",
                  "MN" => "Mongolia",
                  "MS" => "Montserrat",
                  "MA" => "Morocco",
                  "MZ" => "Mozambique",
                  "MM" => "Myanmar",
                  "NA" => "Namibia",
                  "NR" => "Nauru",
                  "NP" => "Nepal",
                  "NL" => "Netherlands",
                  "AN" => "Netherlands Antilles",
                  "NC" => "New Caledonia",
                  "NZ" => "New Zealand",
                  "NI" => "Nicaragua",
                  "NE" => "Niger",
                  "NG" => "Nigeria",
                  "NU" => "Niue",
                  "NF" => "Norfolk Island",
                  "MP" => "Northern Mariana Islands",
                  "NO" => "Norway",
                  "OM" => "Oman",
                  "PK" => "Pakistan",
                  "PW" => "Palau",
                  "PA" => "Panama",
                  "PG" => "Papua New Guinea",
                  "PY" => "Paraguay",
                  "PE" => "Peru",
                  "PH" => "Philippines",
                  "PN" => "Pitcairn",
                  "PL" => "Poland",
                  "PT" => "Portugal",
                  "PR" => "Puerto Rico",
                  "QA" => "Qatar",
                  "RE" => "Reunion",
                  "RO" => "Romania",
                  "RU" => "Russian Federation",
                  "RW" => "Rwanda",
                  "KN" => "Saint Kitts And Nevis",
                  "LC" => "Saint Lucia",
                  "VC" => "Saint Vincent And The Grenadines",
                  "WS" => "Samoa",
                  "SM" => "San Marino",
                  "ST" => "Sao Tome And Principe",
                  "SA" => "Saudi Arabia",
                  "SN" => "Senegal",
                  "SC" => "Seychelles",
                  "SL" => "Sierra Leone",
                  "SG" => "Singapore",
                  "SK" => "Slovakia (Slovak Republic)",
                  "SI" => "Slovenia",
                  "SB" => "Solomon Islands",
                  "SO" => "Somalia",
                  "ZA" => "South Africa",
                  "GS" => "South Georgia, South Sandwich Islands",
                  "ES" => "Spain",
                  "LK" => "Sri Lanka",
                  "SH" => "St. Helena",
                  "PM" => "St. Pierre And Miquelon",
                  "SD" => "Sudan",
                  "SR" => "Suriname",
                  "SJ" => "Svalbard And Jan Mayen Islands",
                  "SZ" => "Swaziland",
                  "SE" => "Sweden",
                  "CH" => "Switzerland",
                  "SY" => "Syrian Arab Republic",
                  "TW" => "Taiwan",
                  "TJ" => "Tajikistan",
                  "TZ" => "Tanzania, United Republic Of",
                  "TH" => "Thailand",
                  "TG" => "Togo",
                  "TK" => "Tokelau",
                  "TO" => "Tonga",
                  "TT" => "Trinidad And Tobago",
                  "TN" => "Tunisia",
                  "TR" => "Turkey",
                  "TM" => "Turkmenistan",
                  "TC" => "Turks And Caicos Islands",
                  "TV" => "Tuvalu",
                  "UG" => "Uganda",
                  "UA" => "Ukraine",
                  "AE" => "United Arab Emirates",
                  "UM" => "United States Minor Outlying Islands",
                  "UY" => "Uruguay",
                  "UZ" => "Uzbekistan",
                  "VU" => "Vanuatu",
                  "VE" => "Venezuela",
                  "VN" => "Viet Nam",
                  "VG" => "Virgin Islands (British)",
                  "VI" => "Virgin Islands (U.S.)",
                  "WF" => "Wallis And Futuna Islands",
                  "EH" => "Western Sahara",
                  "YE" => "Yemen",
                  "YU" => "Yugoslavia",
                  "ZM" => "Zambia",
                  "ZW" => "Zimbabwe"
                );

    public function __construct() {
        parent::__construct();
        $this->load->model('Payments');
        $this->load->model('Settings');
        $this->load->model("Pages");
        $this->data['html_body_id'] = 'donate';
    }

    public function index() {
        $this->home();
    }

    public function home() {
        $this->data['windowTitle'] = $this->data['settings']['SITE_NAME'].' - Make a Donation';

        $this->data['innerPage'] = true;
        $this->data['pagesPhotos']       = $this->Pages->getInnerPagePhoto("'DONATE_LEFT', 'DONATE_RIGHT'");
        foreach($this->data['pagesPhotos'] as $photo){
            if(strstr($photo->key, 'LEFT')){
                $this->data['bannerPathLeft'] = $photo->path;
            }else{
                $this->data['bannerPathRight']= $photo->path;
            }
        }

        $this->data['pageTitle']    = $this->lang->line('MENU_DONATE');
        $this->data['donationInfo'] = $this->Settings->get('DONATION_INFO');
        $this->data['countryList']  = $this->countries;

        $this->load->model("Programes");
        $this->data['programes']    = $this->Programes->getAllProgrames(0, 100);
        $this->FormValidator();
    }

    public function success() {

        $this->data['windowTitle'] = $this->data['settings']['SITE_NAME'].' - Make a Donation - Success';
        $this->data['pageTitle'] = $this->lang->line('MENU_DONATE');

        $this->data['innerPage'] = true;
        $this->data['pagesPhotos']       = $this->Pages->getInnerPagePhoto("'DONATE_LEFT', 'DONATE_RIGHT'");
        foreach($this->data['pagesPhotos'] as $photo){
            if(strstr($photo->key, 'LEFT')){
                $this->data['bannerPathLeft'] = $photo->path;
            }else{
                $this->data['bannerPathRight']= $photo->path;
            }
        }
        
        $view = $this->layout->view('donate_success', $this->data, TRUE);
            $replaces = array(
                '{FLASH_MSG_CONT}'  => $this->load->view('common/flash_msg_cont', "Thank You for your donation", TRUE),
             );

        $this->load->view('view', array('view' => $view, 'replaces' => $replaces));
    }

    public function bankwire() {
        $this->data['windowTitle'] = $this->data['settings']['SITE_NAME'].' - Bank Information';

        $this->data['innerPage'] = true;
        $this->data['pagesPhotos']       = $this->Pages->getInnerPagePhoto("'DONATE_LEFT', 'DONATE_RIGHT'");
        foreach($this->data['pagesPhotos'] as $photo){
            if(strstr($photo->key, 'LEFT')){
                $this->data['bannerPathLeft'] = $photo->path;
            }else{
                $this->data['bannerPathRight']= $photo->path;
            }
        }
        
        $this->data['pageTitle'] = $this->lang->line('MENU_DONATE');
        $this->data['bankInfo'] = $this->Settings->get('BANK_INFO');
        
        $view = $this->layout->view('donate_bank', $this->data, TRUE);
            $replaces = array(
                '{FLASH_MSG_CONT}'  => $this->load->view('common/flash_msg_cont', NULL, TRUE),
             );

            $this->load->view('view', array('view' => $view, 'replaces' => $replaces));
    }

    function FormValidator()
    {
        $this->load->model('Common');

        $this->Common->loadLibraryForFormValidation();
        $this->loadFormValidationRules();

         if($this->form_validation->run() == false){
        $view = $this->layout->view('donate_home', $this->data, TRUE);
            $replaces = array(
                '{FLASH_MSG_CONT}'  => $this->load->view('common/flash_msg_cont', NULL, TRUE),
             );

            $this->load->view('view', array('view' => $view, 'replaces' => $replaces));
        }else{
                 if($_POST['paymentMethod']=='wire'){
                     redirect('/donate/bankwire/');die();
                 }
            $paymentId = $this->Payments->save($_POST);
            if($paymentId){
                $this->load->library('Paypal');
                $this->Paypal = new Paypal();
                // Specify your paypal email
                $this->Paypal->addField('business', $this->Settings->get('PAYPAL_ADDRESS'));
                // Specify the currency
                $this->Paypal->addField('currency_code', $_POST['currency']);
                // Specify the url where paypal will send the user on success/failure
                $this->Paypal->addField('return', base_url().'donate/success');
                $this->Paypal->addField('cancel_return', base_url().'donate');
                // Specify the url where paypal will send the IPN
                $this->Paypal->addField('notify_url', base_url().'donate/ipn');

                // Specify the product information
                $this->Paypal->addField('item_name', 'General Donation');
                $this->Paypal->addField('address1', $_POST['address']);
                $this->Paypal->addField('city', $_POST['city']);
                $this->Paypal->addField('state', $_POST['state']);
                $this->Paypal->addField('zip', $_POST['zip']);
                $this->Paypal->addField('country', $_POST['country']);
                $this->Paypal->addField('email', $_POST['email']);
                $this->Paypal->addField('night_phone_c', $_POST['phone']);
                $this->Paypal->addField('amount', $_POST['amount']);
                $this->Paypal->addField('item_number', $paymentId);

                if ($this->Settings->get('PAYPAL_DEMO')=='yes'){
                    // Enable test mode if needed
                    // Override on Aug 22nd as per request
                    // $this->Paypal->enableTestMode();
                }

                $this->Paypal->submitPayment();
            }
        }
    }

    function ipn(){
        $this->load->library('Paypal');
        // Create an instance of the paypal library
        $this->Paypal = new Paypal();

                if ($this->Settings->get('PAYPAL_DEMO')=='yes'){
                    // Enable test mode if needed
                    // Override on Aug 22nd as per request
                    // $this->Paypal->enableTestMode();
                }
		$this->Paypal->logResults(false);
        // Check validity and write down it
        if ($this->Paypal->validateIpn())
        {
            if ($this->Paypal->ipnData['payment_status'] == 'Completed')
            {
                 $this->Payments->updateIPN($this->Paypal->ipnData['item_number'],$this->Paypal->ipnData['txn_id'],'approved');
            }

        }
    }
    
    public function loadFormValidationRules()
    {
        $this->form_validation->set_rules('name','Name','required');
        $this->form_validation->set_rules('email','Email','required|valid_email');
        $this->form_validation->set_rules('address','Address','required');
        $this->form_validation->set_rules('state','State','min_length[2]');
        $this->form_validation->set_rules('city','City','required|min_length[2]');
        $this->form_validation->set_rules('zip','Zip','required');
        $this->form_validation->set_rules('amount','Amount','required|numeric');
        $this->form_validation->set_rules('country','Country','required');
        //$this->form_validation->set_rules('programe','Programe','required');
        $this->form_validation->set_rules('comment','Comments','max_length[500]');
        $this->form_validation->set_rules('paymentMethod','Payment Method','required');
    }
}

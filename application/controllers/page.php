<?php

include APPPATH . "controllers/BaseController.php";

class Page extends BaseController {

    public function __construct() {
        parent::__construct();
    }

    public function index($slug = '') {
        $this->_prepareHomePageData();
        $this->data['generalPage'] = true;
 
        if($slug == ''){
            $this->data['showSlider'] = true;
            $this->data['html_body_id'] = 'home';
            $this->load->model("Pages");
            $pageId = $this->Pages->find("upper(title)='HOME'", 'id');
            $this->_loadPageDetails($pageId['id']);
            $this->_loadPageView();
        }else{
            $splitSlug      = explode("-", $slug);
            $pageId         = end($splitSlug);

            if (!ctype_digit($pageId)) {
                $pageId = $this->Pages->getIdBySlug($slug);
            }

            if (!ctype_digit($pageId)) {
                redirect(base_url()); return;
            }
            
            $this->data['innerPage']      = $this->Pages->checkInner($pageId);
            $parentId                     = $this->Pages->find("id = $pageId", 'parent');
            if($parentId['parent'] == 0){
                $this->showParentPage($pageId);
            }else{
                $this->showSubPage($parentId['parent'], $pageId);
            }
        }
    }

    public function showParentPage($pageId)
    {
        $this->load->model("Pages");

        $this->_createSubNavigation($pageId);
        $this->_loadPageDetails($pageId);
        
        $this->data['RootParent'] = $pageId;
        $this->data['relPageDesc']  = $this->Pages->getRelPageDesc($pageId, $this->_language, true);
        $this->data['breadCrumb']   = $this->_generateBreadCrumb($pageId);
        
        $pageTitle       = $this->Pages->getPageTitle($pageId, $this->_language);
        $this->pageTitle = $pageTitle['title'];
        
        if(strtoupper($this->pageTitle) == 'HOME'){
            $this->data['showSlider'] = true;
            $this->data['html_body_id'] = 'home';
            $this->_loadPageView();
        }else{
            $this->data['html_body_id'] = 'page-' . $pageId;
            //$this->data['html_body_id'] = 'page';
            //echo $this->data['html_body_id']; die();
            $this->_loadPagePhotos($pageId);
            $this->_loadSubPageView();
        }

    }

    public function showSubPage($parentPageId, $currentPageId)
    {
        $this->data['RootParent'] = $this->Pages->getRootParent($currentPageId);
        
        $this->_createSubNavigation($parentPageId);
        $this->_loadPageDetails($currentPageId);
        $this->data['breadCrumb'] = $this->_generateBreadCrumb($currentPageId);
        //die($this->data['breadCrumb']);
        $pageTitle = $this->Pages->getPageTitle($currentPageId, $this->_language);

        $this->data['pageColor']    = $this->Pages->getPageColor($currentPageId);
        $this->data['relPageDesc']  = $this->Pages->getRelPageDesc($currentPageId, $this->_language);

        $this->pageTitle = $pageTitle['nav_title'];

        $this->data['html_body_id'] = 'page-' . $currentPageId;
//        $this->data['html_body_id'] = 'page';

        $this->_loadPagePhotos($currentPageId);
        $this->_loadSubPageView();
    }

    private function _generateBreadCrumb($pageId)
    {
       $this->load->model("Pages");
       $pageInfo    = $this->Pages->getPageTitle($pageId);
       $parent      = $pageInfo['parent'];
       $pageTitle   = $pageInfo['title'];
       $_breadCrumb = '';
       
       if($parent != 0){
           while ($parent != 0){
               $subPageInfo = $this->Pages->getPageTitle($parent);
               $_breadCrumb = "<a href = /page/$subPageInfo[slug]-$subPageInfo[id]>$subPageInfo[nav_title]</a> > ".$_breadCrumb;
               $parent = $subPageInfo['parent'];
           }
       }
       
       $_breadCrumb .= "<a href = /page/$pageInfo[slug]-$pageInfo[id]>$pageInfo[nav_title]</a>";
//       $_breadCrumb .= "<a href = /page/$pageInfo[slug]>$pageInfo[nav_title]</a>";
       //echo $_breadCrumb; die();
       //return substr($_breadCrumb, 0, -2);
       return $_breadCrumb;
       
    }
    private function _createSubNavigation($pageId)
    {
        $this->load->model("Pages");
        $this->data['subNavigation'] = $this->Pages->getSubNavegation($pageId);
    }

    private function _loadPageDetails($pageId)
    {
        $this->load->model("Pages");
        $this->data['pageContents'] = $this->Pages->getPageDetails($pageId, $this->_language);
        $this->data['pageColor'] = $this->Pages->getPageColor($pageId);
    }

    private function _loadPageView()
    {
        $this->data['windowTitle'] = $this->data['settings']['SITE_NAME'].' - Home';
        $this->load->model("Pages");
        $this->data['programe_images'] = $this->Pages->getProgrameImages();
        
        $view = $this->layout->view('home', $this->data, TRUE);
        $replaces = array(
            '{FLASH_MSG_CONT}'  => $this->load->view('common/flash_msg_cont_front', NULL, TRUE),
            '{BANNER_DIV}'      => $this->load->view('banner', NULL, TRUE)
        );

        $this->load->view('view', array('view' => $view, 'replaces' => $replaces));
    }

    private function _loadSubPageView()
    {
        $this->data['windowTitle'] = $this->data['settings']['SITE_NAME'].' - '.$this->pageTitle;
        
        $this->data['pageTitle'] = $this->pageTitle;    

        $view = $this->layout->view('inner', $this->data, TRUE);
        $replaces = array(
            '{FLASH_MSG_CONT}'  => $this->load->view('common/flash_msg_cont_front', NULL, TRUE),
            '{BANNER_DIV}'      => ''
        );

        $this->load->view('view', array('view' => $view, 'replaces' => $replaces));
    }

    private function _loadPagePhotos($pageId)
    {
        $this->load->model("Pages");
        $this->data['pagesPhotos'] = $this->Pages->getPagesPhotosPath($pageId);
 
        foreach($this->data['pagesPhotos'] as $pagesPhoto){
            if($pagesPhoto->page_banner == 1){
                $this->data['bannerPathLeft'] = $pagesPhoto->path;
            }else{
                $this->data['bannerPathRight'] = $pagesPhoto->path;
            }
        }
    }


    private function _prepareHomePageData()
    {
        $this->load->model('News_model');
        $this->load->model('Medias');
        $this->load->model('Pages');
        
        
        $this->data['news']         = $this->News_model->getRecentNews(5);
        $this->data['events']       = $this->News_model->getRecentEvents(5);
        $this->data['brochure']     = $this->Medias->getMedia($this->data['settings']['BROCHURE']);
        $this->data['newsleter']    = $this->Pages->getSettingsPage($this->data['settings']['NEWSLETER']);
        $this->data['video']        = $this->Pages->getSettingsPage($this->data['settings']['VIDEO']);
        $this->data['blog']         = $this->Pages->getSettingsPage($this->data['settings']['BLOG']);
        $this->data['donate']       = $this->Pages->getSettingsPage($this->data['settings']['DONATE']);
        $this->data['firstNews']    = $this->data['news'][0];
        
    }
}
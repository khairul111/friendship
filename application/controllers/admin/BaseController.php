<?php

/**
 * Base Controller
 *
 * Common tasks of all controllers are done here
 * Must be inherited, no direct instance allowed (abstract)
 *
 * @package     Friendship-cms
 * @category    Controller
 * @author      Nurul Ferdous
 */

abstract class BaseController extends Controller
{
    protected $data = array();

    public function __construct()
    {
        parent::Controller();

        $this->_loadSettings();
        $this->_setLayout();

        if(!isset($_SESSION['userName'])){
            redirect('admin/login');
        }
    }

    private function _setLayout()
    {
        $this->load->library('layout');

        $this->data['html_body_id'] = '';
        $this->data['{FLASH_MSG_CONT}'] = '';

        $this->layout->setLayout('layouts/layout_admin', $this->data);
    }

    private function _loadSettings()
    {
        $this->load->model("Settings");
        $this->data['settings'] = $this->Settings->getAll();
        $this->data['html_body_id'] = 'dashboard';
    }
}
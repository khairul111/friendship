<?php

include APPPATH . "controllers/admin/BaseController.php";

class Page extends BaseController
{
    protected $_perPage = 10;

    public function __construct()
    {
        parent::__construct();
    }

    public function index($sortBy='title', $page = 0)
    {
        $this->load->model("Pages");
        $totalPage = $this->Pages->findCount();
        if($totalPage > $this->_perPage){
            $this->data['pagination'] = getPagination("admin/page/index/".$sortBy,$totalPage,5,$this->_perPage);
            $from = $page;
            $this->data['pages'] = $this->Pages->getAllPages($from, $this->_perPage, $sortBy);
        }else{
            $this->data['pages'] = $this->Pages->getAllPages(0, 10, $sortBy);
        }

        $view = $this->layout->view('admin/pages', $this->data, TRUE);
              $replaces = array(
                '{FLASH_MSG_CONT}'       => $this->load->view('common/flash_msg_cont', NULL, TRUE),
             );

         $this->load->view('view', array('view' => $view, 'replaces' => $replaces));

    }

    public function addNewPage()
    {
        $this->load->model("Languages");
        $this->load->model("Photos");
        $this->load->model("Pages");
        $this->load->model("Albums");

        $this->data['languages'] = $this->Languages->getAll();
        $this->data['pages'] = $this->Pages->getSelectedFields("id,title","id",false);
        $this->data['albums'] = $this->Albums->getAllAlbum();
        $this->data['photoGroup'] = $this->Photos->getAllPhotos();
        $this->data['pages_photo'] = "";
        $this->data['action'] = "";
        $this->data['pageData'] = $_POST;
        $this->newsFormValidator();
    }

    public function editPageData($pageId)
    {
        $this->load->model("Pages");
        $this->load->model("Photos");
        $this->load->model("Albums");

        $this->data['pageData']     = $this->Pages->getSelectedPage($pageId);
        $this->data['contentData']  = $this->Pages->getPageContent($pageId, 'page');
        $this->data['pages']        = $this->Pages->getSelectedFields("id,title","id",false);
        $this->data['albums']       = $this->Albums->getAllAlbum();
        $this->data['photosId']     = $this->Pages->getPagesPhotos($pageId);
        $this->data['banner']       = $this->Pages->getBanner($pageId);

        $this->data['photoGroup']   = $this->Photos->getAllPhotos();

        $this->data['action'] = "edit";
        $this->newsFormValidator();

    }

    function newsFormValidator()
    {
        $this->load->model('Common');

        $this->Common->loadLibraryForFormValidation();
        $this->Pages->loadFormValidationRulesForPage();

         if($this->form_validation->run() == false){
            $view = $this->layout->view("/admin/page_edit", $this->data, TRUE);
            $replaces = array(
                '{FLASH_MSG_CONT}'  => $this->load->view('common/flash_msg_cont', NULL, TRUE),
             );

            $this->load->view('view', array('view' => $view, 'replaces' => $replaces));
        }else{
            $this->getPageData();
        }
    }

    public function getPageData()
    {
        $this->load->model("Pages");
        if($_POST['button'] == "save"){
            $this->Pages->savePageInfo($_POST);
            $this->session->set_flashdata('status_message', 'Page added successfully');
            redirect('/admin/page');

        }
        else{ 
            $this->Pages->updatePageInfo($_POST);
            $this->session->set_flashdata('status_message', 'Page updated successfully');
            redirect('/admin/page');
        }
    }

    public function deletePage($pageId)
    {
        $this->load->model("Pages");
        $return = $this->Pages->deletePage($pageId);

        if($return){
            $this->session->set_flashdata('status_message', 'Page deleted successfully');
        }
        else{
            $this->session->set_flashdata('status_message', 'Page not deleted');
        }

        redirect('/admin/page');
    }


}
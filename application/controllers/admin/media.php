<?php

include APPPATH . "controllers/admin/BaseController.php";

class Media extends BaseController {
    public $perPage = 10;
    public function __construct() {
        parent::__construct();
        $this->load->model("Medias");
        $this->data['html_body_id'] = 'media';
        $this->data['msg']['body'] = '';
    }

    public function index() {
        $this->home();
    }

    public function home($sortBy = 'id', $page = 0) {
        $total = $this->Medias->findCount();
        if($this->perPage < $total) {
            $this->data['pagination'] = getPagination("admin/media/home/".$sortBy,$total,5,$this->perPage);
            $from = $page;
            $this->data['medias'] = $this->Medias->getAll($from, $this->perPage, $sortBy);
        }else {
            $this->data['medias'] = $this->Medias->getAll(0,10, $sortBy);
        }
        
        $view = $this->layout->view('admin/media/home', $this->data, TRUE);
        $replaces = array(
            '{FLASH_MSG_CONT}'       => $this->load->view('common/flash_msg_cont', NULL, TRUE),
        );
        $this->load->view('view', array('view' => $view, 'replaces' => $replaces));
    }

    public function add() {
        $this->edit();
    }

    public function edit($id = NULL) {
        $this->load->library('form_validation');
        $config['upload_path'] = './assets/media/';
        $config['allowed_types'] = 'pdf|rtf|doc|txt';
        $config['max_size']	= '10240';
        $config['remove_spaces']	= true;
        $config['overwrite'] = false;
        $config['remove_spaces'] = true;
        $config['encrypt_name'] = false;

        $this->load->library('upload', $config);
        $val = $this->form_validation;
        $val->set_error_delimiters('<p class="ui-state-error ui-corner-all" style="margin:5px 0;padding:5px 10px;width:335px">', '</p>');
        // Set form validation rules
        $val->set_rules('title', 'Title', 'trim|required|xss_clean|max_length[200]');
        $val->set_rules('description', 'Description', 'trim|required');

        // Run form validation
        if($valRun = $val->run()) {
            $updateMediaData = array(
                'title'         => $val->set_value('title'),
                'description'   => $val->set_value('description'),
            );

            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('media')) {
                $this->session->set_flashdata('status_message', strip_tags($this->upload->display_errors()));
                redirect('/admin/media/home');
            }else {
                $fileInfo = $this->upload->data();
                $updateMediaData['path'] = url_title($fileInfo['file_name']);
            }
        }
        // save data if it's pass the validation
        if ($valRun AND $this->Medias->updateMedia($updateMediaData, $id)) {
            $this->session->set_flashdata('status_message', 'Information updated successfully');
            redirect('admin/media/home');
        }else {
            $this->data['media'] = $this->Medias->getMedia($id);
        }

        $view = $this->layout->view('admin/media/edit', $this->data, TRUE);
        $replaces = array(
            '{FLASH_MSG_CONT}'       => $this->load->view('common/flash_msg_cont', NULL, TRUE),
        );
        $this->load->view('view', array('view' => $view, 'replaces' => $replaces));

    }

    public function delete($id = NULL) {
        if(!ctype_digit($id)) redirect('/');

        if($this->Medias->deleteMedia($id)) {
            $this->session->set_flashdata('status_message', 'Information deleted successfully');
        }
        redirect('admin/media/home');
    }
}
<?php
include_once('BaseController.php');

/**
 * Admin Controller
 * Controls basic actions of admin module
 *
 * @subpackage  Admin
 * @category    Controller
 */
class Dashboard extends BaseController {

    public $perPage = 10;

    /**
     * Initializes common resources
     *
     * @access   public
     * @return   void
     */
    function __construct() {
        parent::__construct();

        $this->siteTitle = $this->Setting->get('site_title');
        $authEmail = $this->Setting->get('auth_email');

        $this->load->helper('html');

        // redirect for unauthorized person

        //$this->form_validation->set_error_delimiters('<p class="error">', '</p>');

        //$this->data['windowTitle'] = $this->siteTitle;

        $this->load->model ( 'Utility' );
        $this->layout->setLayout ( 'layouts/layout_admin' );

    }

    /**
     * redirects to default action
     *
     * @access   public
     * @return   void
     */
    public function index() {
        $this->home();
    }

    public function home() {
        $this->_loadAndDisplay('admin/dashboard');
    }

    protected function _loadAndDisplay($viewName) {
    // Loading common jQuery plugins
        $this->output->enable_profiler(false);

        $this->data['activeTab']  = 'home';
        $output = $this->layout->view($viewName, $this->data, true);

        $replaces = array(
            '{FLASH_MSG_CONT}' => $this->load->view('common/flash_msg_cont', NULL, TRUE),
        );

        $this->load->view('view', array('view' => $output, 'replaces' => $replaces));

    }

}

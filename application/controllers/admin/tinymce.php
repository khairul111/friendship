<?php
/**
 * This controller will provide informations for tinymcs popup dialogs
 *
 * @packege     Friendship CMS
 * @author		Anis uddin Ahmad
 * @copyright 	2009
 */

class Tinymce extends Controller
{
	private $data = array();

	function __construct()
	{
		parent::Controller();
	}

	function pages()
	{
		$this->load->model("Pages");
		$pages = $this->Pages->findAll();

        $this->data['pages'] = $pages;
		$this->load->view('admin/tinymce/pages', $this->data);
	}

	
}

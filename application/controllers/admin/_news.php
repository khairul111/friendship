<?php

include APPPATH . "controllers/admin/BaseController.php";

class News extends BaseController
{
    protected $_perNews = 10;

    public function __construct()
    {
        parent::__construct();
    }

    public function index($sortBy = 'create_date', $news = 0)
    {
        if ($sortBy!=null) setcookie("newsSortBy", $sortBy, time()+3600);        
        if ($sortBy=='slug') $sortKey = 'ASC'; else  $sortKey = 'DESC';

        $this->load->model("News_Model");

        $totalNews = $this->News_Model->findCount();
        if($totalNews > $this->_perNews){
            $this->data['pagination'] = getPagination("admin/news/index/".$sortBy, $totalNews, 5,$this->_perNews);
            $from = $news;
            $this->data['news'] = $this->News_Model->getAllNews($from, $this->_perNews, $sortBy, $sortKey);
        }else{
            $this->data['news'] = $this->News_Model->getAllNews(0, 10, $sortBy, $sortKey);
        }

        $view = $this->layout->view('/admin/news_views', $this->data, TRUE);
              $replaces = array(
                '{FLASH_MSG_CONT}'       => $this->load->view('common/flash_msg_cont', NULL, TRUE),
             );


         $this->load->view('view', array('view' => $view, 'replaces' => $replaces));
    }
    
    public function addNews()
    {
        $this->load->model("Languages");
        $this->load->model("News_Model");
        $this->load->model("Photos");
        $this->load->model("Albums");

        $this->data['headLine'] = "Add News";
        $this->data['languages'] = $this->Languages->getAll();
        $this->data['albums'] = $this->Albums->getAllAlbum();
        $this->data['photosGroup'] = $this->Photos->getAllPhotos();
        $this->data['action'] = "";
        $this->data['news_photo'] = "";
        $this->data['newsData'] = $_POST;
        $this->newsFormValidator();
        
    }

    public function editNews($newsId)
    {
        $this->load->model("News_Model");
        $this->load->model("Pages");
        $this->load->model("Photos");
        $this->load->model("Albums");

        $this->data['newsData'] = $this->News_Model->getSelectedNews($newsId);
        $this->data['contentData'] = $this->Pages->getNewsAndEventsPageContent($newsId, "'news', 'events'");
        $this->data['albums'] = $this->Albums->getAllAlbum();
        $this->data['news_photo'] = $this->News_Model->getNewsPhotos($newsId);
        $this->data['photosGroup'] = $this->Photos->getAllPhotos();
        $this->data['headLine'] = "Edit News";
        $this->data['action'] = "edit";
        $this->data['newsId'] = $newsId;
        $this->newsFormValidator();
    }

    function newsFormValidator()
    {
        $this->load->model('Common');

        $this->Common->loadLibraryForFormValidation();
        $this->News_Model->loadFormValidationRulesForNews();
        if($this->form_validation->run() == false){
            $view = $this->layout->view("/admin/news_form", $this->data, TRUE);
            $replaces = array(
                '{FLASH_MSG_CONT}'       => $this->load->view('common/flash_msg_cont', NULL, TRUE),
             );
            $this->load->view('view', array('view' => $view, 'replaces' => $replaces));
        }else{
            $this->getnewsdata();
        }

    }
    
	function check_negative($number)
	{
		if ($number <= 0)
		{
			$this->form_validation->set_message('check_negative', 'The %s field can not have zero or negative value');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

    public function getnewsdata()
    {
        $this->load->model("News_Model");
        if($_POST['button'] == "save"){ 
            $this->News_Model->saveNewsInfo($_POST);
            $this->session->set_flashdata('status_message', 'News added successfully');
            redirect('/admin/news');

        }
        else{
            $this->News_Model->updateNewsInfo($_POST);
            $this->session->set_flashdata('status_message', 'News updated successfully');
            redirect('/admin/news');
        }
    }
    
    public function deleteNews($newsId)
    {
        $this->load->model("News_Model");
        $return = $this->News_Model->deleteNews($newsId);
        if($return){
            $this->session->set_flashdata('status_message', 'News deleted successfully');
        }
        else{
            $this->session->set_flashdata('status_message', 'News not deleted');
        }
        redirect('/admin/news');
    }

}

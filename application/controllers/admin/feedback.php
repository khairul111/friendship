<?php

include APPPATH . "controllers/admin/BaseController.php";

class Feedback extends BaseController
{
    protected $_perPage = 10;

    public function __construct()
    {
        parent::__construct();
    }

    public function index($orderBy='id', $from = 0)
    {
        $this->load->model("Feedbacks");

        $total = $this->Feedbacks->findCount();
        if($total > $this->_perPage){
            $this->data['pagination'] = getPagination("admin/feedback/index/".$orderBy ,$total,5,$this->_perPage);

            $this->data['rows'] = $this->Feedbacks->getAll($from, $this->_perPage, $orderBy);
        }else{
            $this->data['rows'] = $this->Feedbacks->getAll(0, 10, $orderBy);
        }

        $view = $this->layout->view('/admin/feedback_views', $this->data, TRUE);
              $replaces = array(
                '{FLASH_MSG_CONT}'       => $this->load->view('common/flash_msg_cont', NULL, TRUE),
             );

         $this->load->view('view', array('view' => $view, 'replaces' => $replaces));

    }
 
    public function delete($id)
    {
        $this->load->model("Feedbacks");
        $return = $this->Feedbacks->remove($id);
        if($return){
            $this->session->set_flashdata('status_message', 'Feedback deleted successfully');
        }
        else{
            $this->session->set_flashdata('status_message', 'Feedback not deleted');
        }
        redirect('/admin/feedback');
    }

    public function showDetails($id)
    {
        $this->load->model("Feedbacks");
        $this->data['details'] = $this->Feedbacks->find('id = '.$id);

        $view = $this->layout->view('/admin/feedback_details', $this->data, TRUE);
              $replaces = array(
                '{FLASH_MSG_CONT}'       => $this->load->view('common/flash_msg_cont', NULL, TRUE),
             );

        $this->load->view('view', array('view' => $view, 'replaces' => $replaces));
    }

}

<?php

include APPPATH . "controllers/admin/BaseController.php";
/**
 * Login Controller
 *
 * all settings functionality here
 *
 * @package     Friendship-cms
 * @category    Controller
 * @author      Raju Mazumder
 * @copyright   2008-2009 right brain solution ltd
 * @link        http://www.rightbrainsolution.com
 */

 class Setting extends BaseController{

     protected $data = array();

     function __construct()
     {
         parent::__construct();

         $this->load->model('Settings');
         $this->load->model('Common');

         $this->data['formTitle'] = 'Friendship CMS Settings';
     }

     public function index()
     {
         $this->Common->loadLibraryForFormValidation();
         $this->Settings->loadFormValidationRulesForSettings();

          if($this->form_validation->run() == false){
              $this->getPages();
              $this->data['settings'] = $this->Settings->getAll();
              $view = $this->layout->view('admin/settings', $this->data, TRUE);
              $replaces = array(
                '{FLASH_MSG_CONT}'       => $this->load->view('common/flash_msg_cont', NULL, TRUE),
             );

             $this->load->view('view', array('view' => $view, 'replaces' => $replaces));
          }else{
                $this->Settings->updateSettings($_POST);
                $this->session->set_flashdata('status_message', 'Settings updated successfully');
                redirect('admin/setting');
          }

     }

     public function getPages()
     {
         $this->load->model("Pages");
         $this->load->model("Medias");
         $this->load->model('Photos');
         
         $this->data['pages'] = $this->Pages->findAll();
         $this->data['medias'] = $this->Medias->findAll();
         $this->data['photos'] = $this->Photos->findAll(null, '*', 'title');
     }


 }

<?php

include APPPATH . "controllers/admin/BaseController.php";

class Album extends BaseController {
    protected $_albumId;

    public function __construct() {
        parent::__construct();
        $this->load->model("Albums");
        $this->data['html_body_id'] = 'album';
    }

    public function index() {
        $this->home();
    }

    public function home() {
        $this->data['albums'] = $this->Albums->getAll($start = 0, $total = 10);

        $view = $this->layout->view('admin/album/home', $this->data, TRUE);
        $replaces = array(
            '{FLASH_MSG_CONT}' => $this->load->view('common/flash_msg_cont', NULL, TRUE),
        );
        $this->load->view('view', array('view' => $view, 'replaces' => $replaces));
    }

    public function add() {
        $this->edit();
    }

    public function edit($id = NULL) {
        $this->load->library('form_validation');
        $this->load->model('Photos');
        $this->data['photos'] = $this->Photos->getAllByAlbumId($id);
        $val = $this->form_validation;
        $val->set_error_delimiters('<p class="ui-state-error ui-corner-all" style="margin:5px 0;padding:5px 10px;width:335px">', '</p>');
        // Set form validation rules
        $val->set_rules('title', 'Title', 'trim|required|xss_clean|prep_for_form|max_length[200]');
        $val->set_rules('description', 'Description', 'trim|xss_clean|prep_for_form|required');

        // Run form validation
        if($valRun = $val->run()) {
            $updateAlbumData = array(
                'title'                 => $val->set_value('title'),
                'description'           => $val->set_value('description'),
                'status'                => $_POST['status']
            );
        }
        // save data if it's pass the validation
        if ($valRun) {
            $id = $this->Albums->updateAlbum($updateAlbumData, $id);
            if($this->Photos->uploadTempFiles($id)){
                $this->form_validation->set_message('photos[]', $this->lang->line('auth_username_check'));
            }
            if($id){
                $this->session->set_flashdata('status_message', 'Album has been updated successfully');
            }else{
                $this->session->set_flashdata('status_message', 'Album has been added successfully');
            }
            redirect('admin/album/update/'.$id);
        }else {
            $this->data['album'] = $this->Albums->getAlbum($id);
        }

        $view = $this->layout->view('admin/album/edit', $this->data, TRUE);
        $replaces = array(
            '{FLASH_MSG_CONT}'       => $this->load->view('common/flash_msg_cont', NULL, TRUE),
        );
        $this->load->view('view', array('view' => $view, 'replaces' => $replaces));

    }

    public function update($id) {
        $this->load->library('form_validation');
        $this->load->model('Photos');
        $this->data['photos'] = $this->Photos->getAllByAlbumId($id);
        if($id){
            $this->data['cover'] = $this->Albums->getCoverPath($id);
        }
        if(count($_POST) > 0) {
            if($this->Photos->updatePhotos()) {
                $updateCoverData = array('cover_photo_id'=>$_POST['cover']);
                //print_r($updateCoverData);die;
                $this->Albums->updateCover($updateCoverData, $id);
                $this->session->set_flashdata('status_message', 'Information updated successfully');
            }else {
                $this->session->set_flashdata('status_message', 'Oops, some error has been occured. Please try again.');
            }
            redirect('admin/album/home/');
        }
        $view = $this->layout->view('admin/album/update', $this->data, TRUE);
        $replaces = array(
            '{FLASH_MSG_CONT}'       => $this->load->view('common/flash_msg_cont', NULL, TRUE),
        );
        $this->load->view('view', array('view' => $view, 'replaces' => $replaces));
    }

    public function delete($id = NULL) {
        if(!ctype_digit($id)) redirect('/');

        if($this->Albums->deleteAlbum($id)) {
            $this->session->set_flashdata('status_message', 'Album deleted successfully');
        }
        redirect('admin/album/home');
    }

    public function delete_photo($id = NULL) {
        if(!ctype_digit($id)) redirect('/');
        $this->load->model('Photos');
        if($this->Photos->delete($id)) {
            $this->session->set_flashdata('status_message', 'Photo deleted successfully');
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

}
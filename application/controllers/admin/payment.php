<?php
include APPPATH . "controllers/admin/BaseController.php";

class Payment extends BaseController
{
    public $perPage = 10;
    function __construct()
    {
        parent::__construct();

        $this->data['pageTitle'] = 'Manage Payments Data';
        $this->data['formTitle'] = 'Manage Payments Data';

    }

    public function index($orderBy='id', $page = 0)
    {
        if($orderBy=='id' || $orderBy=='modified' || $orderBy=='amount'){
            $sort = 'DESC';
        }else{
            $sort = 'ASC';
        }

        $this->load->model('Payments');

        $totalPayment = $this->Payments->findCount();
        if($this->perPage < $totalPayment){
            $this->data['pagination'] = getPagination("admin/payment/index/".$orderBy, $totalPayment,5,$this->perPage);
			$from = $page;
            $this->data['payments'] = $this->Payments->getAllPayments($from, $this->perPage, $orderBy." ".$sort);
        }else{
             $this->data['payments'] = $this->Payments->getAllPayments(0,10, $orderBy." ".$sort);
        }
       

        $view = $this->layout->view('admin/payments', $this->data, TRUE);
        $replaces = array(
                '{FLASH_MSG_CONT}'       => $this->load->view('common/flash_msg_cont', NULL, TRUE),
        );

        $this->load->view('view', array('view' => $view, 'replaces' => $replaces));
    }

    public function Delete($paymentId = 0)
    {
        $this->load->model('Payments');
        $paymentId = $_POST['paymentId'];
        if(ctype_digit("$paymentId")){
            if($this->Payments->remove($paymentId)){
                echo 'SUCCESS';
            }else{
                echo 'ERROR';
            }
        }

    }

    
}
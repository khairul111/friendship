<?php

include APPPATH . "controllers/admin/BaseController.php";

class Programe extends BaseController
{
    protected $_perProg = 10;

    public function __construct()
    {
        parent::__construct();
    }

    public function index($sortBy='id', $news = 0)
    {
        $this->load->model("Programes");

        $totalNews = $this->Programes->findCount();
        if($totalNews > $this->_perProg){
            $this->data['pagination'] = getPagination("admin/programe/index/".$sortBy,$totalNews,5,$this->_perProg);
            $from = $news;
            $this->data['programes'] = $this->Programes->getAllProgrames($from, $this->_perProg, $sortBy);
        }else{
            $this->data['programes'] = $this->Programes->getAllProgrames(0, 10, $sortBy);
        }

        $view = $this->layout->view('/admin/programes', $this->data, TRUE);
              $replaces = array(
                '{FLASH_MSG_CONT}'       => $this->load->view('common/flash_msg_cont', NULL, TRUE),
             );
         $this->load->view('view', array('view' => $view, 'replaces' => $replaces));
    }

    public function addPrograme()
    {
        $this->load->model("Pages");
        $this->load->model("Programes");

        $this->data['pages'] = $this->Pages->getSelectedFields("id,title","id", false);
        $this->data['action'] = "";
        $this->programeFormValidator();
    }

    public function editPrograme($programeId)
    {
        $this->load->model("Pages");
        $this->load->model("Programes");
        $this->data['pages'] = $this->Pages->getSelectedFields("id,title","id", false);
        $this->data['programe'] = $this->Programes->getSelectedPrograme($programeId);
        $this->data['action'] = "edit";
        $this->programeFormValidator();
    }

    function programeFormValidator()
    {
        $this->load->model('Common');
        $this->Common->loadLibraryForFormValidation();
        $this->Programes->loadFormValidationRulesForPrograme();

        if($this->form_validation->run() == false){
            $view = $this->layout->view("/admin/programe_form", $this->data, TRUE);
            $replaces = array(
                '{FLASH_MSG_CONT}'       => $this->load->view('common/flash_msg_cont', NULL, TRUE),
             );
            $this->load->view('view', array('view' => $view, 'replaces' => $replaces));
        }else{
            $this->getProgrameData();
        }
    }

    public function getProgrameData()
    {
        $this->load->model("Programes");

        if($_POST['button'] == "save"){
            $this->Programes->saveProgrameInfo($_POST);
            $this->session->set_flashdata('status_message', 'Programe added successfully');
            redirect('/admin/programe');
        }
        else{
            $this->Programes->updateNewsInfo($_POST);
            $this->session->set_flashdata('status_message', 'Programe updated successfully');
            redirect('/admin/programe');
        }
    }

    public function deletePrograme($programeId)
    {
        $this->load->model("Programes");

        $return = $this->Programes->deletePrograme($programeId);

        if($return){
            $this->session->set_flashdata('status_message', 'Programe deleted successfully');
        }
        else{
            $this->session->set_flashdata('status_message', 'Programe not deleted');
        }

        redirect('/admin/programe');
    }

}

<?php

include APPPATH . "controllers/BaseController.php";

class File extends BaseController {
    public $perPage = 10;
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Medias");
        $this->data['html_body_id'] = 'file';
        $this->data['msg']['body'] = '';
    }

    public function index()
    {
        $this->data['windowTitle'] = $this->lang->line('MENU_FILE_CABINET').' - '.$this->data['settings']['SITE_NAME'];
        $this->home();
    }

    public function home($page = 0)
    {
        $this->data['breadCrumb'] = '<a href="/">Home</a>';
        $this->data['pageTitle'] = $this->lang->line('MENU_FILE_CABINET');

        $this->data['pageTitle'] = $this->lang->line('MENU_FILE_CABINET');
        $this->breadcrumb->addCrumb($this->lang->line('MENU_FILE_CABINET'));
        $this->data['breadCrumb'] = $this->breadcrumb->makeBread();


        $total = $this->Medias->findCount();
        if($this->perPage < $total) {
            $this->data['pagination'] = getPagination("media/home",$total,3,$this->perPage);
            $from = $page;
            $this->data['medias'] = $this->Medias->getAll($from, $this->perPage);
        }else {
            $this->data['medias'] = $this->Medias->getAll(0,10,"id DESC");
        }

        $view = $this->layout->view('media', $this->data, TRUE);
        $replaces = array(
            '{FLASH_MSG_CONT}'       => $this->load->view('common/flash_msg_cont', NULL, TRUE),
            '{BANNER_DIV}'      => ''
        );
        $this->load->view('view', array('view' => $view, 'replaces' => $replaces));
    }

    public function change_language()
    {
        $urlString = explode("_w_",$this->uri->uri_string());
        setLangCookie($this->uri->rsegment(3));
        $this->_language = $language['code'];
        redirect($urlString[1]);
    }

}
<?php

function getPagination($uri, $totalRows, $currPageSig, $perPage = NULL) {
    $ci = &get_instance();
    $ci->load->library('pagination');

    $config['base_url']    = site_url($uri);
    $config['total_rows']  = $totalRows;
    $config['per_page']    = ($perPage) ? $perPage : $this->config->item('numOfItemsPerPage');
    $config['uri_segment'] = $currPageSig;
    $config['num_links']   = 5;

    $config['first_link']      = 'First';
    $config['first_tag_open']  = '<span>';
    $config['first_tag_close'] = '</span>';

    $config['last_link']      = 'Last';
    $config['last_tag_open']  = '<span>';
    $config['last_tag_close'] = '</span>';

    $config['next_link']      = 'Next >>';
    $config['next_tag_open']  = '<span class="next">';
    $config['next_tag_close'] = '</span>';

    $config['prev_link']      = '<< Previous';
    $config['prev_tag_open']  = '<span class="previous-off">';
    $config['prev_tag_close'] = '</span>';

    $config['cur_tag_open']  = '<span class="active">';
    $config['cur_tag_close'] = '</span>';

    $config['num_tag_open']  = '<span class="others">';
    $config['num_tag_close'] = '</span>';

    $ci->pagination->initialize($config);

    return $ci->pagination->create_links();
}

function sendEmail($msg = '',$from = 'webmaster@friendship-bd.org', $sub = 'Email - www.friendship-bd.org' ,$to ='webmaster@friendship-bd.org' ) {
    $ci = &get_instance();
    $ci->load->library('email');

    $ci->email->from($from);
    $ci->email->to($to);
    $ci->email->subject($sub);
    $ci->email->message($msg);

    return $ci->email->send();
}

function limitCharacter($string, $limit, $suffix = ' . . .') {
    $string = strip_tags($string);

    if(strlen($string) < $limit) {
        return $string;
    }

    for($i = $limit; $i >= 0; $i--) {
        $c = $string[$i];
        if($c == ' ' OR $c == "\n") {
            return substr($string, 0, $i) . $suffix;
        }
    }

    return  substr($string, 0, $limit) . $suffix;

}

function stripallslashes($string) {
    while(strchr($string,'\\')) {
        $string = stripslashes($string);
    }
    return $string;
}

function setLangCookie($value) {
    $cookie = array(
        'name'   => 'site_lang',
        'value'  => $value,
        'expire' => '86500',
    );
    set_cookie($cookie);
}
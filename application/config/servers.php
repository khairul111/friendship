<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Local servers
|--------------------------------------------------------------------------
|
| List of servers used in local development
|
*/
$config['server_local'] = array('local.friendship-bd.org', 'localhost', 'local.friendship.tld');

/*
|--------------------------------------------------------------------------
| Staging servers
|--------------------------------------------------------------------------
|
| List of servers used in staging server
|
*/
$config['server_staging'] = array('friendship.rbs-staging.com', 'www.friendship.rbs-staging.com');

/*
|--------------------------------------------------------------------------
| Live servers
|--------------------------------------------------------------------------
|
| List of servers used in live server
|
*/
$config['server_live'] = array('friendship-bd.org', 'www.friendship-bd.org');

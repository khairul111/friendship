<ol id="download">
    <?php if (count($medias) > 0): foreach ($medias as $media): ?>
    <li>
        <h3>
            <a href="/assets/media/<?php echo $media->path; ?>">
            <?php echo $media->title ?>
            <img alt="" src="/assets/images/icons/disk.png"/>
            </a>
        </h3>
        <p>
            <?php echo (!empty($media->description)) ? $media->description : "-" ?>
        </p>
        <small class="tips"><?php  echo $this->lang->line('COMMON_TEXT_UPLOADED_ON'); ?> <?php echo date('M j, y', strtotime($media->modify_date)); ?></small>

    </li>
    <?php endforeach; endif; ?>
</ol>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/galleryview/jquery.galleryview-2.0-pack.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/galleryview/jquery.timers-1.1.2.js"></script>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/galleryview.css" type="text/css" />
<div class="gal-wrap">
    <div id="photos" class="galleryview">
        <ul id="gallery">
            <?php if (count($photos) > 0):foreach ($photos as $key => $photo): ?>
                    <li>
                        <span class="panel-overlay">
                            <span class="panel-overlay2"><h3><?php echo $photo['title']; ?></h3><h4><?php echo $photo['description']; ?></h4></span>
                        </span>
                        <img class="img" alt="" src="<?php echo base_url(); ?>assets/photos/650x310/<?php echo $photo['path']; ?>" />
                    </li>
                <?php endforeach;
            endif; ?>
        </ul>
    </div>
</div>
<style type="text/css">
.panel{background-color: #1e1e23}
div.panel img{
    left: 0px !important;
    padding-top: 0px;
    padding-right: 50px !important;
    padding-bottom: 0px;
    padding-left: 52px !important;
    width: 570px !important;
}
div.panel span{
    /*left:500px !important;*/
}
.overlay-background{
    width: 608px !important;
    left:0px !important;
}

.overlay-background {
    height: 0 !important;
}
.panel-overlay2{
    float: right;
    width: 255px;
    font-size: 12px;
}
.panel-overlay2 h3{
    text-align: left;
    font-size: 14px;
    padding-top: 15px;
}

.panel-overlay2 h4{
    text-align: left;
    font-size: 12px;
    padding-top: 15px;
}
div#content {padding-top: 0;}
</style>
<script type="text/javascript">
    $('#photos').galleryView({
        panel_width: 907,
        overlay_position: 'top',
        panel_height: 300,
        frame_width: 100,
        frame_height: 70,
        nav_theme: 'light',
        frame_gap: 15,
        pause_on_hover: true,
        easing: 'easeInOutQuad'
    });
</script>

<?php foreach($news as $n): ?>
<div class="news">
    <h3><a href="/news/view/<?php echo $n->slug.'-'.$n->id; ?>"><?php echo limitCharacter(stripallslashes($n->title), 75); ?></a></h3>
    <label class="small">Published Date: <?php echo date("F j, Y", strtotime($n->create_date)); ?></label><br>
    <p><?php echo limitCharacter(stripallslashes($n->details), 230); ?><span class="readmore"><a href="/news/view/<?php echo $n->slug.'-'.$n->id; ?>"> read more</a></span></p>
</div>
<?php endforeach; ?>

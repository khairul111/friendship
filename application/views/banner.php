<div id="banner">

    <div id="main-photo">
        
        <?php $i=1; if(isset($programe_images) && is_array($programe_images) && count($programe_images) > 0): foreach($programe_images as $programe_image):?>
            <div id="bannerImage_<?php echo $i++;?>">
                <?php if(empty($programe_image['image_path']))$programe_image['image_path'] = 'banner.jpg';?>
                <a href="/page/<?php echo $programe_image['slug'].'-'.$programe_image['id'];?>"><img src="<?php echo base_url();?>assets/photos/580x290/<?php echo $programe_image['image_path']?>" alt="<?php echo $programe_image['programe_name']?>" /></a>
                <span><?php echo $programe_image['programe_name']?></span>
            </div>
        <?php 
            endforeach;
            else:
            $programe_images = 0;
        ?>
            <div id="bannerImage_1">
                <img src="<?php echo base_url();?>assets/photos/580x290/banner.jpg" alt="banner.jpg" />
                <span>No Programme Available</span>
            </div>
        <?php endif;?>
    </div>
    
    <div class="photo-sidebar-right">
        <?php $i=1;if(isset($programe_images) && is_array($programe_images) && count($programe_images) > 0):foreach($programe_images as $programe_image):?>
            <?php if(empty($programe_image['image_path']))$programe_image['image_path'] = 'banner.jpg';?>
            <img id="thumbImage_<?php echo $i++?>" class="banner-thumb red" style="border:3px solid <?php echo $programe_image['page_color'] ?>" src="<?php echo base_url();?>assets/photos/115x85/<?php echo $programe_image['image_path']?>" alt="<?php echo $programe_image['image_path']?>" />
        <?php endforeach;else:?>
        <img id="thumbImage_1" class="banner-thumb red"  src="<?php echo base_url();?>assets/photos/115x85/banner.jpg" alt="banner.jpg"/>
        <?php endif;?>

    </div>
</div>

<script type="text/javascript">
	var totalImage   	= "<?php echo count($programe_images);?>";
	var currentImageId = 1;
	$(document).ready(function() {

        for(i=2; i <= totalImage; i++){
           $("div#main-photo #bannerImage_"+i).css("display", "none");
        }

	$('.photo-sidebar-right img').click( function ()
        {
            result = ($(this).attr('id')).split("_");
            setBannerImage(result[1]);
            return false;
        });
	});
	function setBannerImage(imageId)
	{
		currentImageId = imageId
		for(i=1; i <= totalImage; i++){
           $("div#main-photo #bannerImage_"+i).css("display", "none");
         }

		$("div#main-photo #bannerImage_"+imageId).fadeIn('slow');
	}
</script>
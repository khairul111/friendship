<div class="grid_15">
    <h1 class="page-title content_edit">Pages</h1>
    <a class="button right" href="/admin/page/addnewpage"><span>Add New Page</span></a>
</div>

<div class="grid_15">
    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Pages">

        <thead>
            <tr>
                <th width="250" scope="col"><a href="/admin/page/index/title">Title</a></th>
                <th width="250" scope="col"><a href="/admin/page/index/parent_title">Parent</a></th>
                <th width="100" scope="col"><a href="/admin/page/index/modified">Last Modified</a></th>
                <th width="90" scope="col">&nbsp;</th>
            </tr>
        </thead>

        <tbody>
        <?php if (count($pages) > 0): foreach ($pages as $page): ?>
            <tr>
                <td><?php echo $page->title ?></td>
                <td><?php echo (!empty($page->parent_title)) ? $page->parent_title : "-" ?></td>
                <td><?php echo date("F j, Y", strtotime($page->modified)) ?></td>
                <td width="90">
                    <a href="/admin/page/editPageData/<?php echo $page->id; ?>" class="edit_icon" title="Edit"></a>&nbsp;&nbsp;
                    <a href="/admin/page/deletePage/<?php echo $page->id; ?>" onclick="javascript:return confirm('Are you sure to delete this page?');" class="delete" title="Delete"></a>
                </td>
            </tr>
        <?php endforeach; endif; ?>
        </tbody>

    </table>
    <div class="pagination">
        <?php echo (isset($pagination) ? $pagination : '')?>
    </div>
</div>
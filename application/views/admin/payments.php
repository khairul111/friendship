<div class="grid_9">
    <h1 class="content_edit">Payments</h1>
</div>
<div class="grid_15">
    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Pages">
       <thead>
            <tr>
                <th width="250" scope="col"><a href="/admin/payment/index/name">Name</a></th>
                <th width="250" scope="col"><a href="/admin/payment/index/email">Email</a></th>
                <th width="100" scope="col"><a href="/admin/payment/index/address_city">City</a></th>
                <th width="90" scope="col"><a href="/admin/payment/index/address_country">Country</a></th>
                <th width="90" scope="col">Transaction ID</th>
                <th width="90" scope="col"><a href="/admin/payment/index/amount">Amount</a></th>
                <th width="90" scope="col">Status</th>
                <th width="90" scope="col"><a href="/admin/payment/index/modified">Date</a></th>
                <th width="90" scope="col">&nbsp;</th>
                

            </tr>
        </thead>

        <tbody>
        <?php if (count($payments) > 0): foreach ($payments as $payment): ?>
            <tr id="payment_<?php echo $payment->id?>">
                <td><?php echo (!empty($payment->name)) ? $payment->name : "-" ?></td>
                <td><?php echo (!empty($payment->email)) ? $payment->email : "-" ?></td>
                <td><?php echo (!empty($payment->address_city)) ? $payment->address_city : "-" ?></td>
                <td><?php echo (!empty($payment->address_country)) ? $payment->address_country : "-" ?></td>
                <td><?php echo (!empty($payment->transaction_id)) ? $payment->transaction_id : "-" ?></td>
                <td><?php echo (!empty($payment->amount)) ? $payment->amount : "0" ?></td>
                <td><?php echo (!empty($payment->status)) ? $payment->status : "-" ?></td>
                <td><?php echo date("F j, Y", strtotime($payment->modified)) ?></td>
                <td width="90"><a href="#" onclick="deleteCheck('<?php echo $payment->id?>')" class="delete" title="Delete"></a></td>
            </tr>
        <?php endforeach; endif; ?>
        </tbody>

    </table>
    <div class="pagination">
    <?php echo isset($pagination) ? $pagination : '';?>
    </div>
</div>

<script type="text/javascript">

    var paymentId = 0;
    function deleteCheck(id)
    {
        paymentId = id;

        showConfirmation("Payment Data Delete","This operation is NOT undoable.So are you sure you want to delete?");
    }

    function bindPageEvents()
    {

        $('#confirm-dialog').dialog('option', 'buttons', {
        "Cancel": function() { $(this).dialog("close"); return false; },
        "OK"    : function() { $(this).dialog("close");

            $.ajax({
                type: "POST",
                url: "/admin/payment/delete",
                data: {"paymentId" : paymentId},
                success: function(data){
                    if(data == 'SUCCESS'){

                        $('tr#payment_'+paymentId).remove();

                        $('#dialog').dialog('option', 'title', 'Deleted');
                        $('#dialog').html("Payment Data Deleted Successfully").dialog('open');
                     }
                    else{
                        $('#dialog').dialog('option', 'title', 'Not Deleted');
                        $('#dialog').html("Error!! Please try later").dialog('open');
                    }

                }


            });
        }
        });
    }
</script>
<div class="grid_15">
    <h1 class="page-title content_edit">News</h1>
    <a class="button right" href="/admin/news/addNews"><span>Add News</span></a>
</div>

<div class="grid_15">
    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Pages">
        <thead>
            <tr>
                <th width="250" scope="col"><a href="/admin/news/index/slug/">Title</a></th>
                <th width="100" scope="col">Type</th>
                <th width="100" scope="col">Status</th>
                <th width="100" scope="col">Order</th>
                <th width="100" scope="col"><a href="/admin/news/index/create_date/">Create Date</a></th>
                <th width="100" scope="col">Event Date</th>
                <th width="90" scope="col">&nbsp;</th>
            </tr>
        </thead>

        <tbody>
        <?php if (count($news) > 0): foreach ($news as $news): ?>
            <tr>
                <td><?php echo $news->title ?></td>
                <td><?php echo $news->ref_type ?></td>
                <td><?php echo $news->status ?></td>
                <td><?php echo ($news->news_order == 0) ? 'Not Set' : $news->news_order; ?></td>
                <td><?php echo date("F j, Y", strtotime($news->create_date)) ?></td>
                <td><?php echo(empty($news->event_date) || substr($news->event_date, 0, 4) == '0000')? 'Not Set':date("F j, Y", strtotime($news->event_date))?></td>
                <td width="90">
                    <a href="/admin/news/editNews/<?php echo $news->id; ?>" class="edit_icon" title="Edit"></a> &nbsp; &nbsp;
                    <a href="/admin/news/deleteNews/<?php echo $news->id; ?>" onclick="javascript:return confirm('Are you sure to delete this news?');" class="delete" title="Delete"></a>
                </td>
            </tr>
        <?php endforeach; endif; ?>
        </tbody>

    </table>
    <div class="pagination">
        <?php echo (isset($pagination) ? $pagination : '')?>
    </div>
</div>
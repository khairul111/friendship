<div class="grid_9">
    <h1 class="content_edit">Add/Edit Programme</h1>
</div>

<div class="grid_15">

    <form method="post" name="frmPrograme" enctype="multipart/form-data" action="/admin/programe/addprograme">

        <input type="hidden" name="programeId" value="<?php if($action=="edit") echo $programe[0]->id; ?>">

        <label>Programme Name</label>
        <input type="text" class="smallInput medium" name="name" value="<?php echo($action=="edit") ? $programe[0]->name : set_value('page_title'); ?>"/>
        <?php echo form_error('name'); ?>

        <label>Page</label>
        <select class="smallInput" name="page_id">
            <?php foreach ($pages as $page): ?>
            <option value="<?php echo $page->id ?>" <?php if($action == 'edit')if($page->id == $programe[0]->page_id) echo "selected"?> ><?php echo $page->title ?></option>
            <?php endforeach; ?>
        </select>

        <label>Status</label>
        <select class="smallInput" name="status">
            <option value="active"   <?php if($action == 'edit')if('active' == $programe[0]->status) echo "selected"?>>Active</option>
            <option value="inactive" <?php if($action == 'edit')if('inactive' == $programe[0]->status) echo "selected"?>>Inactive</option>
        </select>

        <label>Select Image</label>
        <input type="file" name="image_path" value="<?php echo($action=="edit") ? $programe[0]->image_path : set_value('page_title'); ?>">

        <br class="clear" />
        
        <input type="hidden" name="button" value="<?php echo($action == "edit") ? "update" : "save"?>">
        <?php if($action == "edit"): ?>
        <a class="button_ok" onclick="javascript:document.frmPrograme.submit();"><span>Update Information</span></a>
        <?php else:?>
        <a class="button_ok" onclick="javascript:document.frmPrograme.submit();"><span>Save Information</span></a>
        <?php endif;?>
        <a class="button_notok" href="/admin/programe"><span>Cancel</span></a>
    </form>
</div>

<script type="text/javascript">
    
    $('.editFile').click(function(){
        $(this).preventDefault();
        $('#media_uploader').toggle();
    });
</script>


<div class="grid_15">
    <h1 class="page-title content_edit">Photo Albums</h1>
    <a class="button right" href="/admin/album/add"><span>Add new album</span></a>
</div>

<div class="grid_15">
    <div class="clear pad5"></div>

    <?php if (count($albums) > 0): foreach ($albums as $album): ?>
    <div class="album">
        <a href="/admin/album/delete/<?php echo $album->id; ?>" onclick="javascript:return confirm('Are you sure?');" class="delete del_album"></a>
        <a href="/admin/album/edit/<?php echo $album->id; ?>">
        <span class="thumb">
        <?php if($album->cover_photo_id == NULL): ?>
            <img src="/assets/photos/150x130/album.jpg" >
        <?php else: ?>
            <img src="/assets/photos/150x130/<?php echo $album->path; ?>" >
        <?php endif; ?>
        </span>
        </a>
        <p class="album-cat-title"><?php echo $album->title ?></p>
    </div>
    <?php endforeach; endif; ?>

</div>

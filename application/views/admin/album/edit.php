<div class="grid_9">
    <h1 class="content_edit">Photo album <?php echo (($album['title'])?' - '.$album['title']:''); ?></h1>
</div>

<div class="grid_15">
    <form method="post" name="frmMedia" enctype="multipart/form-data" action="">

        <div class="clear"></div>
        <?php if(!empty($formCommonError)): ?>
        <div class="form-common-error ui-state-error ui-corner-all">
            <div><?php echo (!empty($formCommonError)) ? $formCommonError : ''; ?></div>
        </div>
        <?php endif; ?>
        <div class="grid_7">
            <label>Title</label>
            <input type="text" class="smallInput medium" name="title" value="<?php if(isset($_POST['title'])) {echo $_POST['title'];}else {echo form_prep($album['title']);} ?>" />
            <?php echo form_error('title'); ?>

            <label>Description</label>
            <textarea cols="30" rows="7" class="smallInput medium" id="wysiwyg" name="description"><?php if(isset($_POST['description'])) {echo $_POST['description'];}else {echo form_prep($album['description']);} ?></textarea>
            <?php echo form_error('description'); ?>

            <label>Show In Gallery</label>
            <div>
                <select name="status" class="smallInput">
                    <option value="active">Active</option>
                    <option value="inactive"  <?php echo($album['status']=='inactive')? 'selected':''?>>Inactive</option>
                </select>
            </div>
            <!-- Edit link -->
        </div>

        <div class="grid_8">
            <label>Add new photos</label>
            <input type="file" class="multi smallInput" name="photos[]" accept="gif|jpg|png" maxlength="100" />
        </div>
        <div class="clear"></div><br />
        <div class="grid_16">
            <?php if($this->uri->segment(3) == 'edit'):?>
            <h3><a class="" href="/admin/album/update/<?php echo $album['id']; ?>"><span>Edit details</span></a></h3>
            <?php endif; ?>

            <?php if($photos):foreach($photos as $photo): ?>
            <div class="float">
                <a class="delete del_photo" onclick="javascript:return confirm('Are you sure?');" href="/admin/album/delete_photo/<?php echo $photo['id']; ?>"></a>
                    <img class="picture-thumb" src="/assets/photos/120x120/<?php echo $photo['path']; ?>"/>
            </div>
                <?php endforeach; endif; ?>
        </div>
        <div style="clear:left; margin-left:10px;">
            <a class="button" onclick="javascript:document.frmMedia.submit();" ><span>Update Album</span></a>
            <a class="button" href="/admin/album"><span>Cancel</span></a>
        </div>
    </form>
</div>

<script type="text/javascript" src="<?php echo site_url('assets/js/plugins/jquery.MultiFile.pack.js');?>"></script>

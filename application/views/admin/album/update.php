<div class="grid_9">
    <h1 class="content_edit">Edit photo(s)</h1>
</div>

<div class="grid_15">
    <form method="post" action="" name="frmUpdate">
        <table class="update">
            <?php if($photos):foreach($photos as $photo): ?>
            <tr>
                <td width="10px">                    
                    <label>Title</label>
                    <input type="text" name="titles[]" class="smallInput medium" value="<?php echo form_prep($photo['title']); ?>" />
                    <label>Description</label>
                    <textarea name="descriptions[]" class="smallInput medium"><?php echo form_prep($photo['description']); ?></textarea>
                    <input type="hidden" name="ids[]" value="<?php echo $photo['id']; ?>"/>
                </td>
                <td>
                    <img class="picture-thumb margin-left" src="/assets/photos/120x120/<?php echo $photo['path']; ?>" />
                    <span class="margin-left block">
                        <input type="radio" class="cover" name="cover" value="<?php echo $photo['id']; ?>" <?php echo(($cover == $photo['path'])?'checked="checked"':''); ?>/>
                        <span class="cover-label">Make it album cover</span>
                    </span>
                </td>
            </tr>
                <?php endforeach; endif ?>
            <tr>
                <td colspan="2">
                    <a class="button" onclick="javascript:document.frmUpdate.submit();" ><span>Update Photos</span></a>
                    <a class="button" href="/admin/album"><span>Cancel</span></a>
                </td>
            </tr>
        </table>

    </form>
</div>
<?php if(!$cover): ?>
<script type="text/javascript">
    $(document).ready(function(){        
        $('tr:first span input').attr('checked', 'checked');
    });
</script>
<?php endif; ?>
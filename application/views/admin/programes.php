<div class="grid_15">
    <h1 class="page-title content_edit">Programme</h1>
    <a class="button right" href="/admin/programe/addprograme"><span>Add New Programme</span></a>
</div>

<div class="grid_15">
    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Pages">

        <thead>
            <tr>
                <th width="250" scope="col"><a href="/admin/programe/index/name">Programme Name</a></th>
                <th width="100" scope="col">Page Title</th>
                <th width="100" scope="col">Status</th>
                <th width="90" scope="col">&nbsp;</th>
            </tr>
        </thead>

        <tbody>
        <?php if (count($programes) > 0): foreach ($programes as $programe): ?>
            <tr>
                <td><?php echo $programe->name ?></td>
                <td><?php echo $programe->title ?></td>
                <td><?php echo $programe->status ?></td>
                <td width="90">
                    <a href="/admin/programe/editprograme/<?php echo $programe->id; ?>" class="edit_icon" title="Edit"></a>&nbsp;&nbsp;
                    <a href="/admin/programe/deleteprograme/<?php echo $programe->id; ?>" onclick="javascript:return confirm('Are you sure to delete this program?');" class="delete" title="Delete"></a>
                </td>
            </tr>
        <?php endforeach; endif; ?>
        </tbody>

    </table>
    <div class="pagination">
        <?php echo (isset($pagination) ? $pagination : '')?>
    </div>
</div>
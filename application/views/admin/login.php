
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo $pageTitle?></title>
    <link href="<?php echo base_url()?>assets/css/960.css" rel="stylesheet" type="text/css" media="all" />
    <link href="<?php echo base_url()?>assets/css/reset.css" rel="stylesheet" type="text/css" media="all" />
    <link href="<?php echo base_url()?>assets/css/text.css" rel="stylesheet" type="text/css" media="all" />
    <link href="<?php echo base_url()?>assets/css/login.css" rel="stylesheet" type="text/css" media="all" />
</head>

<body>
<div class="container_16">
  <div class="grid_6 prefix_5 suffix_5">
   	  <h1><?php echo $pageTitle?></h1>
    	<div id="login">
          <?php if(isset($message)){echo $message;}?>
    	  <form id="form1" name="login" method="post" action="<?php echo ($action == 'login')? base_url().'admin/login/index' : base_url().'admin/login/changepassword'?>">
            <?php if($action == 'login'):?>
    	    <p>
    	      <label><strong>Username</strong>
                    <input type="text" name="name" class="inputText" id="name" 
                    value="<?php echo isset($user['name']) ? $user['name'] : set_value('name')?>" />
                    <?php echo form_error('name'); ?>
    	      </label>
            </p>

            <p>
    	      <label><strong>Password</strong>
                    <input type="password" name="password" class="inputText" id="password"
                    value="<?php echo isset($user['password']) ? $user['password'] : ''?>"/>
                    <?php echo form_error('password'); ?>
  	        </label>
    	    </p>

              
             <label>
                 <?php if(isset($user['password']))$default = 'checked = "checked"';?>
                 <input type="checkbox" name="rememberme" id="rememberme" <?php echo isset($default) ? $default : ''?> />Remember me
             </label>
             <?php else:?>
                <p>
                    <label><strong>Old Password</strong>
                        <input type="password" name="old_password" class="inputText" id="old_password" />
                        <?php echo form_error('old_password'); ?>
                    </label>
                </p>

                <p>
                    <label><strong>New Password</strong>
                        <input type="password" name="new_password" class="inputText" id="new_password" />
                        <?php echo form_error('new_password'); ?>
                    </label>
                </p>

                <p>
                    <label><strong>Confirm Password</strong>
                        <input type="password" name="confirm_password" class="inputText" id="confirm_password" />
                        <?php echo form_error('confirm_password'); ?>
                    </label>
                </p>

              <?php endif;?>
             <input type="submit" name="submit" value="<?php echo ($action == 'login') ? 'Login' : 'Change Password';?>" class="black-button" />
    	  </form>
		  <br clear="all" />
    	</div>
        
  </div>
</div>
<br clear="all" />
</body>
</html>


<div class="grid_15">
    <h1 class="page-title content_edit">File cabinet</h1>
    <a class="button right" href="/admin/media/add"><span>Upload new file</span></a>
</div>

<div class="grid_15">
    
    <div class="clear pad5"></div>
    <?php if($msg['body'] !== ''): ?>
    <p id="<?php echo $msg['class']; ?>" class="info">
        <span class="info_inner">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</span>
    </p>
    <?php endif; ?>
    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Pages">

        <thead>
            <tr>
                <th width="250" scope="col"><a href="/admin/media/home/title">Title</a></th>
                <th width="250" scope="col">Description</th>
                <th width="100" scope="col">File</th>
                <th width="100" scope="col"><a href="/admin/media/home/modify_date">Last modified</a></th>
                <th width="90" scope="col">&nbsp;</th>
            </tr>
        </thead>

        <tbody>
            <?php if (count($medias) > 0): foreach ($medias as $media): ?>
            <tr>
                <td><?php echo $media->title ?></td>
                <td><?php echo (!empty($media->description)) ? $media->description : "-" ?></td>
                <td><a href="/assets/media/<?php echo $media->path; ?>">Download</a></td>
                <td><?php echo date('F j, Y', strtotime($media->modify_date)); ?></td>
                <td width="90">
                    <a href="/admin/media/edit/<?php echo $media->id; ?>" class="edit_icon" title="Edit"></a>&nbsp&nbsp;
                    <a href="/admin/media/delete/<?php echo $media->id; ?>" onclick="javascript:return confirm('Are you sure to delete this file?');" class="delete" title="Delete"></a>
                </td>
            </tr>
                <?php endforeach; endif; ?>
        </tbody>

    </table>
    <div class="pagination"><?php echo (isset($pagination)?$pagination:''); ?></div>
</div>

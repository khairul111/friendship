<div class="grid_9">
    <h1 class="content_edit">Edit file</h1>
</div>

<div class="grid_15">
    <form method="post" name="frmMedia" enctype="multipart/form-data" action="">
        <?php if(!empty($formCommonError)): ?>
        <div class="form-common-error ui-state-error ui-corner-all">
            <div><?php echo (!empty($formCommonError)) ? $formCommonError : ''; ?></div>
        </div>
        <?php endif; ?>
        <?php if(isset($media['path']) || !empty($media['path'])): ?>
        <label>Filename</label>
            <?php echo $media['path']; ?> <a href="javascript:void();" class="editFile">[ Edit ]</a>
        <?php endif; ?>
        <div id="mediaDiv" class="<?php if(isset($media['path'])) {echo 'ui-helper-hidden';}else {echo '';}?>">
            <label>Select a file</label>
            <input type="file" class="smallInput medium" name="media" />
        </div>
        <label>Title</label>
        <input type="text" class="smallInput medium" name="title" value="<?php if(isset($_POST['title'])) {echo $_POST['title'];}else {echo $media['title'];} ?>" />
        <?php echo form_error('title'); ?>

        <label>Description</label>
        <textarea cols="30" rows="7" class="smallInput medium" id="wysiwyg" name="description"><?php if(isset($_POST['description'])) {echo $_POST['description'];}else {echo $media['description'];} ?></textarea>
        <?php echo form_error('description'); ?>

        <a class="button_ok" onclick="javascript:document.frmMedia.submit();" ><span>Update file</span></a>
        <a class="button_notok" href="/admin/media"><span>Cancel</span></a>
    </form>
</div>
<script type="text/javascript">
    $('.editFile').click(function(){
        $('#mediaDiv').toggle();
        return false;
    });
</script>

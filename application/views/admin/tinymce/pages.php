<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Pages</title>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/js/tiny_mce/tiny_mce_popup.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/js/tiny_mce/plugins/pagelink/js/dialog.js"></script>
</head>
<body>
<script type="text/javascript">
var pathPrefix = '<?php echo site_url('page') ?>';
</script>

<form onsubmit="PagelinkDialog.insert();return false;" action="#">
	<h3>Insert Friendship page link.</h3>
    <table>
        <tr>
            <td><label>Text</label></td>
            <td><input id="ontext" name="ontext" type="text" class="smallInput medium" /><br /></td>
        </tr>
        <tr>
            <td><label><label>Page name</label></label></td>
            <td>
                <select id="page_name" name="page_name" class="smallInput medium">
                    <?php foreach($pages as $page): ?>
                    <option value="<?php echo $page['slug'] ?>-<?php echo $page['id'] ?>"><?php echo $page['title'] ?></option>
                    <?php endforeach; ?>
                </select>
            </td>
        </tr>
    </table>
	
	<div class="mceActionPanel">
		<div style="float: left">
			<input type="button" id="insert" name="insert" value="{#insert}" onclick="PagelinkDialog.insert();" />
		</div>
		
		<div style="float: right">
			<input type="button" id="cancel" name="cancel" value="{#cancel}" onclick="tinyMCEPopup.close();" />
		</div>
	</div>
</form>

</body>
</html>

<link type="text/css" rel="stylesheet" href="/assets/css/smoothness/ui.tabs.css" />
<script type="text/javascript" src="/assets/js/ui.tabs.js"></script>

<script type="text/javascript" src="<?php echo base_url() ?>assets/js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php"></script>
<script type="text/javascript">
    var BASE_URL = "<?php echo base_url()?>";
    tinyMCE.init({
    mode : "specific_textareas",
    editor_selector : /(mceEditor|mceRichText)/,
    theme : "advanced",
    skin : "o2k7",

    plugins : "safari,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,imagemanager,filemanager,pagelink",
    // Theme options
    theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
    theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
    theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen,|,pagelink",
    theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom",
    theme_advanced_resizing : true,

    file_browser_callback : "tinyBrowser",
    // Example content CSS (should be your site CSS)
    content_css : "<?php echo site_url('assets/css/frontend/style.css');?>",
    
    extended_valid_elements : "a[name|href|target|title|onclick],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]"
    });
</script>

<div class="grid_9">
    <h1 class="content_edit">Add/Edit Page</h1>
</div>

<form method="POST" name="frmPage" action="/admin/page/addNewPage">
<div class="grid_15">

    <div id="ui-tabs">

        <ul>
            <?php if($action != 'edit'): foreach ($languages as $key => $value):  ?>
            <li><a href="#tab-<?php echo $key ?>"><?php echo $value ?></a></li>
            <?php endforeach; ?>
            <?php else: foreach ($contentData as $content): ?>
            <li><a href="#tab-<?php echo $content->language_id ?>"><?php echo $content->lan_title ?></a></li>
            <?php endforeach; ?>
            <?php endif;?>
        </ul>

        <?php if($action != 'edit'): foreach ($languages as $key => $value):  ?>
        <div id="tab-<?php echo $key ?>">
            <input type="hidden" name="language[]" value="<?php echo $key ?>">

            <label>Page Title <span>(shown in page body)</span></label>
            <input type="text" class="smallInput large" name="title[]" value="<?php echo $pageData['title'][$key-1];?>" />
            <?php echo form_error('title[]'); ?>

            <label>Navigation Title <span>(shown in menu)</span></label>
            <input type="text" class="smallInput large" name="nav_title[]" value="<?php echo $pageData['nav_title'][$key-1];?>"/>
            <?php echo form_error('nav_title[0]'); ?>

            <label>Contents</label>
            <div id="<?php echo 'general-'.$content->language_id;?>" style="display:<?php echo ($content->use_raw_html == 'yes')? 'none' : 'block' ?>">
                <textarea id="<?php echo 'text-'.$content->language_id;?>" class="large mceEditor" rows="7" cols="30" name="description[]"><?php echo $pageData['description'][$key-1] ?></textarea>
            </div>
            <div id="<?php echo 'html-'.$content->language_id;?>" style="display:<?php echo ($content->use_raw_html == 'yes')? 'block' : 'none' ?>">
                <textarea id="<?php echo 'text_raw-'.$content->language_id;?>" class="smallInput large" rows="7" cols="30" name="description_raw[]"><?php echo $pageData['description_raw'][$key-1] ?></textarea>
            </div>
            <?php echo form_error('description[0]'); ?>
            <input type="checkbox" id="<?php echo 'use_raw_html-'.$content->language_id;?>" name="use_raw_html[]" value="no" onclick="toogleEditorMode('<?php echo $content->language_id;?>')"/><span>HTML Option</span>

            <label>Related Story</label>
            <div id="<?php echo 'general-story-'.$content->language_id;?>" style="display:<?php echo ($content->use_raw_html == 'yes')? 'none' : 'block' ?>">
                <textarea id="<?php echo 'text-story-'.$content->language_id;?>" class="large mceEditor" rows="7" cols="30" name="story[]"><?php echo $pageData['story'][$key-1] ?></textarea>
            </div>
            <div id="<?php echo 'html-story-'.$content->language_id;?>" style="display:<?php echo ($content->use_raw_html == 'yes')? 'block' : 'none' ?>">
                <textarea id="<?php echo 'text_raw-story-'.$content->language_id;?>" class="smallInput large" rows="7" cols="30" name="story_raw[]"><?php echo $pageData['story_raw'][$key-1] ?></textarea>
            </div>
            <?php echo form_error('story[0]'); ?>

        </div>
        <?php endforeach; ?>

        <?php else: foreach ($contentData as $content): ?>
        <div id="tab-<?php echo $content->language_id ?>">
            <input type="hidden" name="language[]" value="<?php echo $content->language_id ?>">
            <input type="hidden" name="contentId[]" value="<?php echo $content->id ?>">

            <label>Page Title <span>(shown in page body)</span></label>
            <input type="text" class="smallInput large" name="title[]" value="<?php echo $content->con_title ?>"/>
            <?php echo form_error('title[0]'); ?>
            
            <label>Navigation Title <span>(shown in menu)</span></label>
            <input type="text" class="smallInput large" name="nav_title[]" value="<?php echo $content->nav_title; ?>"/>
            <?php echo form_error('nav_title[0]'); ?>

            <label>Contents</label>
            <div id="<?php echo 'general-'.$content->language_id;?>" style="display:<?php echo ($content->use_raw_html == 'yes')? 'none' : 'block' ?>">
                <textarea id="<?php echo 'text-'.$content->language_id;?>" class="large mceEditor" rows="7" cols="30" name="description[]"><?php echo $content->details ?></textarea>
            </div>
            <div id="<?php echo 'html-'.$content->language_id;?>" style="display:<?php echo ($content->use_raw_html == 'yes')? 'block' : 'none' ?>">
                <textarea id="<?php echo 'text_raw-'.$content->language_id;?>" class="smallInput large" rows="7" cols="30" name="description_raw[]"><?php echo $content->details ?></textarea>
            </div>
            <?php echo form_error('description[0]'); ?>
            <input type="checkbox" id="<?php echo 'use_raw_html-'.$content->language_id;?>" name="use_raw_html[]" value="no" onclick="toogleEditorMode('<?php echo $content->language_id;?>')" <?php echo($content->use_raw_html == 'yes')? 'checked':''?>/><span>HTML Option</span>

            <label>Related Story</label>
            <div id="<?php echo 'general-story-'.$content->language_id;?>" style="display:<?php echo ($content->use_raw_html == 'yes')? 'none' : 'block' ?>">
                <textarea id="<?php echo 'text-story'.$content->language_id;?>" class="large mceEditor" rows="7" cols="30" name="story[]"><?php echo $content->rel_story ?></textarea>
            </div>
            <div id="<?php echo 'html-story-'.$content->language_id;?>" style="display:<?php echo ($content->use_raw_html == 'yes')? 'block' : 'none' ?>">
                <textarea id="<?php echo 'text_raw-story-'.$content->language_id;?>" class="smallInput large" rows="7" cols="30" name="story_raw[]"><?php echo $content->rel_story ?></textarea>
            </div>
            <?php echo form_error('story[0]'); ?>

        </div>
        <?php endforeach; ?>
        <?php endif;?>
        
    </div>

    <br /><br />
    <div class="ui-widget-header ui-corner-top pad10">Additional Settings</div>
    <div class="ui-widget-content ui-corner-bottom" style="padding: 5px 20px 20px;">
        
        <label>Page Title <span>(only used in admin panel)</span></label>
        <input type="text" class="smallInput large" name="page_title" value="<?php echo($action=="edit") ? $pageData[0]->title : set_value('page_title'); ?>"/>
        <?php echo form_error('page_title'); ?>

        <label>Parent</label>
        <select class="smallInput" name="parent">
            <option value="0">Select</option>
            <?php foreach ($pages as $page): ?>
            <option value="<?php echo $page->id ?>" <?php if($action == 'edit')if($page->id == $pageData[0]->parent) echo "selected"?> ><?php echo $page->title ?></option>
            <?php endforeach; ?>
        </select>


        <label>Meta Keyword</label>
        <input type="text" class="smallInput large" name="meta_keyword" value="<?php if($action=="edit") echo $pageData[0]->meta_keyword; ?>"/>

        <label>Meta Description</label>
        <textarea id="wysiwyg" class="smallInput large mceEditor" rows="7" cols="30" name="meta_description"><?php if($action=="edit") echo $pageData[0]->meta_description; ?></textarea>

        <label>Page Column Option</label>
        <select class="smallInput" name="page_column">
            <option value="0">Select</option>
            <option value="one column" <?php if($action == 'edit')if('one column' == $pageData[0]->page_column) echo "selected"?> >One Column</option>
            <option value="two column left side-bar" <?php if($action == 'edit')if('two column left side-bar' == $pageData[0]->page_column) echo "selected"?> >Two column with left side bar</option>
            <option value="two column right side-bar" <?php if($action == 'edit')if('two column right side-bar' == $pageData[0]->page_column) echo "selected"?> >Two column with right side bar</option>
        </select>

        <label>Page Color</label>
        <input type="text" class="smallInput small" name="page_color" value="<?php if($action=="edit") echo $pageData[0]->page_color; ?>"/>
        Example: <strong>#f7d11d</strong>
        
        <label>Page Order</label>
        <input type="text" class="smallInput small" name="page_order" value="<?php if($action=="edit") echo $pageData[0]->page_order; ?>"/>
        
        <label>Visible At Navigation</label>
        <select class="smallInput" name="menu_link">
            <option value="yes" <?php if($action == 'edit')if('yes' == $pageData[0]->menu_link) echo "selected"?> >Yes</option>
            <option value="no" <?php if($action == 'edit')if('no' == $pageData[0]->menu_link) echo "selected"?> >No</option>
        </select>

        <input type="hidden" name="pageId" value="<?php if($action=="edit") echo $pageData[0]->id; ?>">

    </div>

    <br /><br />

    <div class="ui-widget-header ui-corner-top pad10">Related Picture</div>
    <div class="ui-widget-content ui-corner-bottom" style="padding: 5px 20px 20px;">
        <?php if(is_array($albums) && count($albums) > 0):foreach ($albums as $albumId => $title): ?>        
        <label>Album: <a id="<?php echo $albumId;?>" href="#" class="action"><?php echo $title;?></a></label>
        <div id="slidepanel<?php echo $albumId;?>" class="slide-panel">
                <?php if(isset($photoGroup[$albumId])):foreach ($photoGroup[$albumId] as $photo):?>
                <div class="float" id ="img-list">
                    <input type="checkbox" name="image_check[]" id="<?php echo $photo['id']; ?>"  value="<?php echo $photo['id']; ?>" <?php if(is_array($photosId) && count($photosId) > 0)if(in_array($photo['id'], $photosId)) echo "checked";?> />
                    <img class="picture-thumb" src="/assets/photos/30x30/<?php echo $photo['path']; ?>" height="40" width="40" onclick="javascript:check(<?php echo $photo['id'];?>);"><br>
                    <input class="page-banner-select" type="radio" name="innarPageBanner" value="<?php echo $photo['id']?>" <?php echo($photo['id'] == $banner)? 'checked':''?>>Page Banner
                </div>
                <?php endforeach; endif;?>
            </div>
            <br class="clear"/>
        <?php endforeach; else: ?>
        <div class="grid_15">
            There is no image in the gallery. Please <a href="<?php echo site_url("admin/album") ?>">create an album and upload some photos</a> in it. Afterwards, you can link them here.
        </div>
        <?php endif;?>
    </div>
    <br class="clear"/>
    
    <div style="clear:left">
        <input type="hidden" name="button" value="<?php echo($action == "edit") ? "update" : "save"?>">
        <?php if($action == "edit"): ?>
        <a class="button" onclick="javascript:document.frmPage.submit();"><span>Update Informations</span></a>
        <?php else:?>
        <a class="button" onclick="javascript:document.frmPage.submit();"><span>Save Informations</span></a>
        <?php endif;?>
        <a class="button" href="/admin/page"><span>Cancel</span></a>
    </div>
</div>


</form>

<script type="text/javascript">
function toogleEditorMode(sEditorID) {
    try {
        if($('#use_raw_html-'+sEditorID).attr('checked')){
           $('#use_raw_html-'+sEditorID).val('yes');
           $('#html-'+sEditorID).show();
           $('#general-'+sEditorID).hide();
           
           $('#html-story-'+sEditorID).show();
           $('#general-story-'+sEditorID).hide();
       }else {
           $('#use_raw_html-'+sEditorID).val('no');
           $('#general-'+sEditorID).show();
           $('#html-'+sEditorID).hide();

           $('#general-story-'+sEditorID).show();
           $('#html-story-'+sEditorID).hide();
       }
    } catch(e) {
        alert(sEditorID);
    }
}
       $("div#ui-tabs").tabs();
       var panelId = '';
       $(".action").click(function(){
       panelId = $(this).attr('id');
       $("#slidepanel" + panelId).slideToggle("slow");
		$(this).toggleClass("active"); return false;
	});


    function check(id) {
        document.getElementById(id).checked = "checked";
    }
</script>

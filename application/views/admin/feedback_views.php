<div class="grid_15">
    <h1 class="page-title content_edit">Feedback</h1>
</div>

<div class="grid_15">
    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Pages">
        <thead>
            <tr>
                <th width="250" scope="col"><a href="/admin/feedback/index/name">Sent By</a></th>
                <th width="100" scope="col"><a href="/admin/feedback/index/email">E-mail</a></th>
                <th width="100" scope="col"><a href="/admin/feedback/index/subject">Subject</a></th>
                <th width="90" scope="col">&nbsp;</th>
            </tr>
        </thead>

        <tbody>
        <?php if (is_array($rows)): foreach ($rows as $feedback): ?>
            <tr>
                <td><?php echo $feedback['name'] ?></td>
                <td><?php echo $feedback['email'] ?></td>
                <td><a href="/admin/feedback/showDetails/<?php echo $feedback['id']; ?>"><?php echo $feedback['subject'] ?></a></td>
                <td width="90">
                    <a href="/admin/feedback/delete/<?php echo $feedback['id']; ?>" onclick="javascript:return confirm('Are you sure to delete this feedback?');" class="delete" title="Delete"></a>
                </td>
            </tr>
        <?php endforeach; endif; ?>
        </tbody>

    </table>
    <div class="pagination">
        <?php echo (isset($pagination) ? $pagination : '')?>
    </div>
</div>
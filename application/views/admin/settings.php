<div class="grid_9">
    <h1 class="content_edit"><?php echo isset($formTitle) ? $formTitle : 'Friendship CMS'?></h1>
</div>
<div class="grid_15">
    <div id="settings">
        <?php if(isset($message)){echo $message;}?>
        <form id="formSettings" name="formSettings" method="post" action="<?php echo base_url()?>admin/setting">

            <p>
                <label><strong>Site Name</strong>
                    <input type="text" name="site_name" class="smallInput medium" id="site_name"
                           value="<?php echo isset($settings['SITE_NAME']) ? $settings['SITE_NAME'] : set_value('site_name')?>" />
                           <?php echo form_error('site_name'); ?>
                </label>
            </p>

            <p>
                <label><strong>Site Contact Email</strong>
                    <input type="text" name="contact_email" class="smallInput medium" id="contact_email"
                           value="<?php echo isset($settings['SITE_CONTACT_EMAIL_ADDRESS']) ? $settings['SITE_CONTACT_EMAIL_ADDRESS'] : ''?>"/>
                           <?php echo form_error('contact_email'); ?>
                </label>
            </p>

            <p>
                <label><strong>General meta keywords</strong>
                    <input type="text" name="meta_keywords" class="smallInput medium" id="site_copyright"
                           value="<?php echo isset($settings['GENERAL_META_KEYWORD']) ? $settings['GENERAL_META_KEYWORD'] : ''?>"/>
                           <?php echo form_error('meta_keywords'); ?>
                </label>
            </p>

            <p style="margin-bottom:0">
                <label><strong>General meta descriptioon</strong>
                    <input type="text" name="meta_description" class="smallInput medium" id="meta_description"
                           value="<?php echo isset($settings['GENERAL_META_DESCRIPTION']) ? $settings['GENERAL_META_DESCRIPTION'] : ''?>"/>
                           <?php echo form_error('meta_description'); ?>
                </label>
            </p>

            <p style="margin-bottom:0">
                <label><strong>Home Page Right Side Image</strong><br />
                   <select class="smallInput medium" name="homepage_map">
                        <?php foreach ($photos as $photo): ?>
                            <option value="<?php echo $photo['id'] ?>" <?php if($photo['id'] == $settings['HOMEPAGE_MAP']) echo 'selected = "selected"'?> ><?php echo $photo['title'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </label>
            </p>
            <p style="margin-bottom:0">
                <label><strong>Home Page Right Side Image Link</strong><br />
                   <select class="smallInput medium" name="homepage_map_link">
                        <?php foreach ($pages as $page): ?>
                            <option value="<?php echo $page['id'] ?>" <?php if($page['id'] == $settings['HOMEPAGE_MAP_LINK']) echo "selected"?> ><?php echo $page['title'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </label>
            </p>

            <p style="margin-bottom:0">
                <label><strong>Feedback Page Left Side Image</strong><br />
                   <select class="smallInput medium" name="feedback_left">
                        <?php foreach ($photos as $photo): ?>
                            <option value="<?php echo $photo['id'] ?>" <?php if($photo['id'] == $settings['FEEDBACK_LEFT']) echo 'selected = "selected"'?> ><?php echo $photo['title'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </label>
            </p>

            <p style="margin-bottom:0">
                <label><strong>Feedback Page Right Side Image</strong><br />
                   <select class="smallInput medium" name="feedback_right">
                        <?php foreach ($photos as $photo): ?>
                            <option value="<?php echo $photo['id'] ?>" <?php if($photo['id'] == $settings['FEEDBACK_RIGHT']) echo 'selected = "selected"'?> ><?php echo $photo['title'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </label>
            </p>

            <p style="margin-bottom:0">
                <label><strong>News Page Left Side Image</strong><br />
                   <select class="smallInput medium" name="news_left">
                        <?php foreach ($photos as $photo): ?>
                            <option value="<?php echo $photo['id'] ?>" <?php if($photo['id'] == $settings['NEWS_LEFT']) echo 'selected = "selected"'?> ><?php echo $photo['title'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </label>
            </p>

            <p style="margin-bottom:0">
                <label><strong>News Page Right Side Image</strong><br />
                   <select class="smallInput medium" name="news_right">
                        <?php foreach ($photos as $photo): ?>
                            <option value="<?php echo $photo['id'] ?>" <?php if($photo['id'] == $settings['NEWS_RIGHT']) echo 'selected = "selected"'?> ><?php echo $photo['title'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </label>
            </p>

            <p style="margin-bottom:0">
                <label><strong>Sitemap Page Left Side Image</strong><br />
                   <select class="smallInput medium" name="sitemap_left">
                        <?php foreach ($photos as $photo): ?>
                            <option value="<?php echo $photo['id'] ?>" <?php if($photo['id'] == $settings['SITEMAP_LEFT']) echo 'selected = "selected"'?> ><?php echo $photo['title'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </label>
            </p>

            <p style="margin-bottom:0">
                <label><strong>Sitemap Page Right Side Image</strong><br />
                   <select class="smallInput medium" name="sitemap_right">
                        <?php foreach ($photos as $photo): ?>
                            <option value="<?php echo $photo['id'] ?>" <?php if($photo['id'] == $settings['SITEMAP_RIGHT']) echo 'selected = "selected"'?> ><?php echo $photo['title'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </label>
            </p>

            <p style="margin-bottom:0">
                <label><strong>Donate Page Left Side Image</strong><br />
                   <select class="smallInput medium" name="donate_left">
                        <?php foreach ($photos as $photo): ?>
                            <option value="<?php echo $photo['id'] ?>" <?php if($photo['id'] == $settings['DONATE_LEFT']) echo 'selected = "selected"'?> ><?php echo $photo['title'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </label>
            </p>

            <p style="margin-bottom:0">
                <label><strong>Donate Page Right Side Image</strong><br />
                   <select class="smallInput medium" name="donate_right">
                        <?php foreach ($photos as $photo): ?>
                            <option value="<?php echo $photo['id'] ?>" <?php if($photo['id'] == $settings['DONATE_RIGHT']) echo 'selected = "selected"'?> ><?php echo $photo['title'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </label>
            </p>

            <p style="margin-bottom:0">
                <label><strong>Brochure</strong><br />
                   <select class="smallInput medium" name="brochure">
                        <?php foreach ($medias as $media): ?>
                            <option value="<?php echo $media['id'] ?>" <?php if($media['id'] == $settings['BROCHURE']) echo "selected"?> ><?php echo $media['title'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </label>
            </p>
            
            <p style="margin-bottom:0">
                <label><strong>News Leter</strong><br />
                   <select class="smallInput medium" name="newsleter">
                        <?php foreach ($pages as $page): ?>
                            <option value="<?php echo $page['id'] ?>" <?php if($page['id'] == $settings['NEWSLETER']) echo "selected"?> ><?php echo $page['title'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </label>
            </p>

            <p style="margin-bottom:0">
                <label><strong>Video</strong><br />
                    <select class="smallInput medium" name="video">
                        <?php foreach ($pages as $page): ?>
                            <option value="<?php echo $page['id'] ?>" <?php if($page['id'] == $settings['VIDEO']) echo "selected"?> ><?php echo $page['title'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </label>
            </p>

            <p style="margin-bottom:0">
                <label><strong>Blog</strong><br />
                    <select class="smallInput medium" name="blog">
                        <?php foreach ($pages as $page): ?>
                            <option value="<?php echo $page['id'] ?>" <?php if($page['id'] == $settings['BLOG']) echo "selected"?> ><?php echo $page['title'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </label>
            </p>

            <p style="margin-bottom:0">
                <label><strong>Donate</strong><br />
                    <select class="smallInput medium" name="donate">
                        <?php foreach ($pages as $page): ?>
                            <option value="<?php echo $page['id'] ?>" <?php if($page['id'] == $settings['DONATE']) echo "selected"?> ><?php echo $page['title'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </label>
            </p>

            <p style="margin-bottom:0">
                <label><strong>Bangla contact address</strong>
                    <textarea cols="30" rows="5" name="contact_bangla" class="smallInput medium" id="contact_bangla"><?php echo isset($settings['SITE_CONTACT_ADDRESS_BN']) ? $settings['SITE_CONTACT_ADDRESS_BN'] : ''?></textarea>
                   <?php echo form_error('contact_bangla'); ?>
                </label>
            </p>

            <p style="margin-bottom:0">
                <label><strong>English contact address</strong>
                    <textarea cols="30" rows="5" name="contact_english" class="smallInput medium" id="contact_english"><?php echo isset($settings['SITE_CONTACT_ADDRESS_EN']) ? $settings['SITE_CONTACT_ADDRESS_EN'] : ''?></textarea>
                   <?php echo form_error('contact_english'); ?>
                </label>
            </p>

              <p style="margin-bottom:0">
                <label><strong>Donation Information</strong>
                    <textarea cols="30" rows="5" name="donation_info" class="smallInput medium" id="donation_info"><?php echo isset($settings['DONATION_INFO']) ? $settings['DONATION_INFO'] : ''?></textarea>
                   <?php echo form_error('donation_info'); ?>
                </label>
            </p>

              <p style="margin-bottom:0">
                <label><strong>Bank Information</strong>
                    <textarea cols="30" rows="5" name="bank_info" class="smallInput medium" id="bank_info"><?php echo isset($settings['BANK_INFO']) ? $settings['BANK_INFO'] : ''?></textarea>
                   <?php echo form_error('bank_info'); ?>
                </label>
            </p>

            <p>
                <label><strong>Paypal Address</strong>
                    <input type="text" name="paypal_address" class="smallInput medium" id="paypal_address"
                           value="<?php echo isset($settings['PAYPAL_ADDRESS']) ? $settings['PAYPAL_ADDRESS'] : set_value('paypal_address')?>" />
                           <?php echo form_error('paypal_address'); ?>
                </label>
            </p>

            <p style="margin-bottom:0">
                <label><strong>Paypal Server</strong><br />
                    <select class="smallInput small" name="paypal_demo">
                        <option value="no" <?php if($settings['PAYPAL_DEMO'] == "no") echo "selected" ?>>Live</option>
                        <option value="yes" <?php if($settings['PAYPAL_DEMO'] == "yes") echo "selected" ?>>Sandbox</option>
                    </select>
                </label>
            </p>

            <a onclick="javascript:document.formSettings.submit();" class="button"><span>Save</span></a>
        </form>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url() ?>assets/js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
    tinyMCE.init({
    mode : "textareas",
    theme : "advanced",
    skin : "o2k7",

    plugins : "advlink,insertdatetime,preview,media,searchreplace,paste,imagemanager,filemanager",
    // Theme options
    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontselect,fontsizeselect,bullist,numlist,|,undo,redo,|,link,unlink,anchor,image,|,insertdate,inserttime,|,forecolor,backcolor",

    theme_advanced_buttons2 : "",
    theme_advanced_buttons3 : "",
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom",
    theme_advanced_resizing : true,
    extended_valid_elements : "a[name|href|target|title|onclick],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]"
    });
</script>
<div class="grid_15">
    <h1 class="page-title content_edit">Feedback</h1>
</div>

<div class="grid_15">
    <div class="grid_5">
        <label>Name:</label>
    </div>
    <div class="grid_9">
        <label><?php echo $details['name']?></label>
    </div>

    <div class="grid_5">
        <label>E-mail:</label>
    </div>
    <div class="grid_9">
        <label><?php echo $details['email']?></label>
    </div>

    <div class="grid_5">
        <label>Subject:</label>
    </div>
    <div class="grid_9">
        <label><?php echo $details['subject']?></label>
    </div>

    <div class="grid_5">
        <label>Feedback Massege:</label>
    </div>
    <div class="grid_9">
        <p><?php echo $details['msg']?></p>
    </div>
</div>
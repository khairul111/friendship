<div id="site-map" class="site-map" style="width: 100%">
        <ul>
            <?php foreach($parentPages as $parent ): ?>
            <li>
                <a href="/page/<?php echo $parent->slug."-".$parent->ref_id;?>"><?php echo $parent->nav_title;?></a>
                <?php if(isset($subNavigationPage[$parent->ref_id])):?>
                <ul>
                     <?php foreach($subNavigationPage[$parent->ref_id] as $secondLevel):?>
                     <li>
                           <a href="/page/<?php echo $secondLevel['slug'].'-'.$secondLevel['id'];?>"><span>&raquo;</span><?php echo stripallslashes($secondLevel['nav_title']) ?></a>
                           <?php if(isset($subNavigationPage[$secondLevel['id']])): ?>
                           <ul>
                               <?php foreach($subNavigationPage[$secondLevel['id']] as $thirdLevel):?>
                               <li>
                                   <a href="/page/<?php echo $thirdLevel['slug'].'-'.$thirdLevel['id'];?>">
                                       <span>&raquo;</span><?php echo stripallslashes($thirdLevel['nav_title']) ?>
                                   </a>
                               </li>
                               <?php endforeach;?>
                           </ul>
                           <?php endif;?>
                     </li>
                     <?php endforeach;?>
                </ul>
                <?php endif;?>
            </li>
            <?php endforeach; ?>
            <li><a href="/gallery">Photo Gallery</a></li>
        </ul>

    </div>
<div style=" width:470px; padding:10px 0;">
    <?php echo $donationInfo; ?>
</div>

<h3 style="color: #000000;">2. Online Transfar</h3>
<p class="spacious">An asterix (<span class="require-field">*</span>) indicates a required field.</p>
<div style="width:470px; padding:10px 0;">
    <form action="" method="POST">
        <?php if(!empty($formCommonError)): ?>
        <div class="form-common-error ui-state-error ui-corner-all">
            <div><?php echo (!empty($formCommonError)) ? $formCommonError : ''; ?></div>
        </div>
        <?php endif; ?>
        <label><span class="require-field">*</span><?php echo $this->lang->line('COMMON_TEXT_NAME'); ?></label>
        <input type="text" name="name" class="smallInput medium" value="<?php echo set_value('name'); ?>"/>
        <?php echo form_error('name'); ?>

        <label><span class="require-field">*</span><?php echo $this->lang->line('COMMON_TEXT_ADDRESS'); ?></label>
        <input type="text" name="address" class="smallInput medium" value="<?php echo set_value('address'); ?>"/>
        <?php echo form_error('address'); ?>

        <label><span class="require-field">*</span><?php echo $this->lang->line('COMMON_TEXT_CITY'); ?></label>
        <input type="text" name="city" class="smallInput small" value="<?php echo set_value('city'); ?>"/>
        <?php echo form_error('city'); ?>

        <label><span class="require-field">*</span><?php echo $this->lang->line('COMMON_TEXT_STATE'); ?></label>
        <input type="text" name="state" class="smallInput small" value="<?php echo set_value('state'); ?>"/>
        <?php echo form_error('state'); ?>

        <label><span class="require-field">*</span><?php echo $this->lang->line('COMMON_TEXT_ZIP'); ?></label>
        <input type="text" name="zip" class="smallInput small" value="<?php echo set_value('zip'); ?>"/>
        <?php echo form_error('zip'); ?>

        <label><span class="require-field">*</span><?php echo $this->lang->line('COMMON_TEXT_COUNTRY'); ?></label>
        <select class="smallInput medium" name="country">
            <option></option>
        <?php foreach ($countryList as $country): ?>
                   <option value="<?php echo $country ?>" <?php if($country == set_value('country')) echo "selected"?> ><?php echo $country ?></option>
        <?php endforeach; ?>
        </select>
        <?php echo form_error('country'); ?>

        <label><?php echo $this->lang->line('COMMON_TEXT_PHONE'); ?></label>
        <input type="text" name="phone" class="smallInput medium" value="<?php echo set_value('phone'); ?>"/>
        <?php echo form_error('phone'); ?>

        <label><span class="require-field">*</span><?php echo $this->lang->line('COMMON_TEXT_EMAIL'); ?></label>
        <input type="text" name="email" class="smallInput medium" value="<?php echo set_value('email'); ?>"/>
        <?php echo form_error('email'); ?>

        <label>Programme Area</label>
        <label>[For specific programme donations, please select from the following]</label>
        <select class="smallInput medium" name="programe">
            <option></option>
        <?php foreach ($programes as $programe): ?>
           <option value="<?php echo $programe->name ?>" <?php if($programe->name == set_value('programe')) echo "selected"?> ><?php echo $programe->name ?></option>
        <?php endforeach; ?>
        </select>
        <?php echo form_error('programe'); ?>

        <label><span class="require-field">*</span><?php echo $this->lang->line('COMMON_TEXT_AMOUNT'); ?></label>
        <input type="text" name="amount" class="smallInput small" style="float:left;" value="<?php echo set_value('amount'); ?>"/>

        <span class="cover-label"><input class="single-choice" type="radio" value="USD" name="currency" checked="true">USD</span>
        <span class="cover-label"><input class="single-choice" type="radio" value="EUR" name="currency">Euro</span>
        <span class="clear"></span>
        <p>(Minimum $10 or equivalent)</p>
        <?php echo form_error('amount'); ?>

        <label><?php echo $this->lang->line('COMMON_TEXT_COMMENTS'); ?></label>
        <textarea class="smallInput medium" rows="4" cols="10" name="comment"></textarea>
        <?php echo form_error('comment'); ?>
        <p>Please keep it within 500 characters.<br/><br/></p>

        <label><?php echo $this->lang->line('COMMON_TEXT_PAYMENT_METHOD'); ?></label>
        
        <input id="paymentMethod" type="hidden" name="paymentMethod">
<!--        <input type="submit" value="" id="button-bankwire">-->
        <input type="submit" value="" id="button-paypal">
    </form>
</div>

<style type="text/css">
    /*******************************************************************************
      FORMS
    *******************************************************************************/
    .ui-state-error{color:red; font-size:10px;}
    #button-bankwire{height:100px;width:100px;background: url('/assets/images/bankwire.jpeg') no-repeat; margin-bottom:10px; cursor: pointer;}
    #button-paypal{height:100px;width:100px;background: url('/assets/images/paypal.jpeg') no-repeat; margin-bottom:10px; margin-left:0px; cursor: pointer;}
    form label { display:block !important; line-height:normal !important; margin: 5px 0px;  font-size:11px; color: #000000;}
    textarea { display:block; }
    .smallInput { padding:3px 3px; border:1px solid #999; font-size:11px !important; font-family:"Trebuchet MS", Arial, Helvetica, sans-serif !important; color: #333 !important;}
    .largeInput { padding:6px 5px; border:1px solid #999; font-size:15px !important; font-family:"Trebuchet MS", Arial, Helvetica, sans-serif !important; color: #333 !important; }
    form .small { width:150px; }
    form .medium { width:350px; }
    form select.medium{width:357px;}
    form .large { width:800px; }
    form .wide { width:890px; }
    form input.single-choice{ 
        margin:5px 3px 0 15px;
        float:left;
    }
    p.spacious { margin: 10px 0; }
    form p { margin: 2px 0 0 0; }
    h2 { font-size: 20px; }
    .button {
        margin: 0px;
        padding: 0px !important;
        border: 0px;
        background: transparent url('/assets/images/but_right_blue.gif') no-repeat scroll top right;
        color: #1b486a;
        display: block;
        float: left;
        height: 29px;
        margin-right: 6px;
        margin-top:10px;
        padding-right: 12px !important;
        text-decoration: none;
        overflow: hidden;
        font-size: 12px;
        outline: none !important;
        cursor: pointer;
        font-weight: bold;
    }
    .button span {
        background: url('/assets/images/but_left_blue.gif') no-repeat left top;
        display: block;
        line-height: 29px;
        padding: 0px 0px 0px 12px;
        outline: none !important;
        float:left;
    }
    .button:hover {
        background-position: right bottom;
        text-decoration:none !important
    }
    .button:hover span {
        background-position: left bottom;
        color: #1b486a;
    }
    .require-field{
        color:#ff0000;
        padding-right:2px;
        font-family:"Verdana","Arial",sans-serif;
        font-weight:normal;
    }
    #donate-form-heading{
        font-weight:bold;
        font-size:13px;
        padding-left:9px;
    }
    .cover-label{
        display:block;
        float:left;
        line-height:25px;
    }
</style>
<script type="text/javascript">
$("#button-bankwire").click(function(){
    $("#paymentMethod").val('wire');
});
$("#button-paypal").click(function(){
    $("#paymentMethod").val('paypal');
});
</script>

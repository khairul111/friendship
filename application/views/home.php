<script type="text/javascript" src="<?php echo site_url('assets/js/jquery.innerfade.js');?>"></script>
<script type="text/javascript" src="<?php echo site_url('assets/js/config.js');?>"></script>

<div class="mid-block">
    <!--news-block--> 
    <div class="news-block">
        <!--newsHolder-->
        <div class="newsHolder">
            <p><?php echo substr($pageContents[0]->details, 0, 720); ?></p>
        </div><!--/newsHolder-->
            
        <!--round-block-->  
        <div class="round-block">
            <div class="round-t"></div>
            <div class="newsSlider">
                <div class="min-prev">
                    <a title="Previous" href="#"><img alt="Previous" src="<?php echo base_url()?>assets/images/btn-prev.jpg" /></a></div>
                <div class="news-con">
                    <div class="wrap">                        
                        <ul>
                            <?php if(is_array($news) && count($news) > 0):foreach($news as $newsInfo):?>
                            <li>
                                <div class="left-block">
                                    <h3><b>News and Event</b></h3>
                                    <a href="<?php echo base_url()?>news/view/<?php echo $newsInfo['slug'].'-'.$newsInfo['id']; ?>">
                                        <img alt="News" src="<?php echo base_url()?>assets/photos/148x106/<?php echo $newsInfo['path']?>">
                                    </a>
                                </div>
                                <div class="right-block">
                                    <h3><?php echo $newsInfo['news_title']?></h3>
                                    <p>
                                        <?php echo limitCharacter($newsInfo['details'], 100); ?><br/>
                                        <a href="<?php echo base_url()?>news/view/<?php echo $newsInfo['slug'].'-'.$newsInfo['id']; ?>">read more</a>
                                    </p>
                                </div>
                            </li>
                            <?php endforeach;endif;?>
                        </ul>
                    </div>
                </div>
                <div class="min-next">
                    <a title="Next" href="#"><img  alt="Next" src="<?php echo base_url()?>assets/images/btn-next.jpg" /></a>
                </div>
            </div>
            <div class="round-b"></div>
        </div><!--/round-block-->   
            
    </div><!--/news-block-->
</div><!--/mid-block-->
<!--feature-block-->   
<div class="feature-block">
    <ul>
        <li><a href="/page/video-<?php echo $settings['VIDEO']?>">
                <img src="<?php echo base_url()?>assets/photos/150x130/<?php echo $video[0]['path'] ?>" alt="Video" />
            </a>
        </li>
        <li>
            <a href="/gallery"><img src="<?php echo base_url()?>assets/images/gallery-thumb.jpg" alt="" /></a>
        </li>
        <li><a href="<?php echo base_url()?>blog">
                <img src="<?php echo base_url()?>assets/photos/150x130/<?php echo $blog[0]['path'] ?>" alt="Blog" />
            </a>
        </li>
    </ul>
           
    <div class="newsletter">
        <div class="donateBtn">
            <a href="/donate"><img src="/assets/images/btn-doneate.jpg" alt="Donate" /></a>
        </div>
        <div class="signup-form">
            <h3>Newsletter Sign up</h3>
            <form action="" method="">
                <input type="text" class="txt-field" id="newsletter-firstname" onFocus="if(this.value=='First Name') this.value='';" onBlur="if(this.value=='') this.value='First Name'" value="First Name" />
                <input type="text" class="txt-field" id="newsletter-lastname" onFocus="if(this.value=='Last Name') this.value='';" onBlur="if(this.value=='') this.value='Last Name'" value="Last Name" />
                <input type="text" class="txt-field" id="newsletter-email" onFocus="if(this.value=='Email') this.value='';" onBlur="if(this.value=='') this.value='Email'" value="Email" />
                <a class="newsletter-signup" href="#"><input type="image" src="assets/images/btn-signup.jpg" /></a>
            </form>
        </div>
    </div>
</div><!--/feature-block--> 
    
<!--program-block-->     
<div class="program-block">
    <?php if($events): foreach($events as $id=>$event):?>
    <div class="blockRptr <?php echo($id==4)? 'noBorder':''?>" style="height: 220px;">
        <img src="/assets/images/<?php echo $event['path']?>" alt="eventimage" />
        <h3><?php echo $event['event_title']?></h3>
        <div>
            <a href="<?php echo base_url()?>news/view/<?php echo $event['slug'].'-'.$event['id']; ?>">
            <?php echo substr($event['details'], 0, 100)?>
            </a>
        </div>
    </div>
    <?php endforeach; endif;?>
</div><!--/program-block-->

<!--news-block-->    
   
<script type="text/javascript">
        $(function () {
            $('.news-con').anythingSlider({
                easing: "linear",        // Anything other than "linear" or "swing" requires the easing plugin
                autoPlay: false,                 // This turns off the entire FUNCTIONALY, not just if it starts running or not.
                delay: 3000,                    // How long between slide transitions in AutoPlay mode
                startStopped: false,            // If autoPlay is on, this can force it to start stopped
                animationTime: 600,             // How long the slide transition takes
                hashTags: true,                 // Should links change the hashtag in the URL?
                buildNavigation: false,          // If true, builds and list of anchor links to link to each slide
                pauseOnHover: true,             // If true, and autoPlay is enabled, the show will pause on hover
                nextButtonClass: '.min-next',             // next button class
                prevButtonClass: '.min-prev'             // previous button class
                });
                
//                var proH=$(".program-block").height();
//		$('.blockRptr').css('height',proH +"px");
        });
</script>

<div id="gallery-cat">
<?php if (count($albums) > 0): foreach ($albums as $album): ?>
<div class="album">
    <a href="/gallery/album/<?php echo $album->id; ?>">
        <span class="thumb">
            <?php if($album->cover_photo_id == NULL): ?>
            <img src="/assets/photos/150x130/album.jpg" >
            <?php else: ?>
            <img src="/assets/photos/150x130/<?php echo $album->path; ?>" >
            <?php endif; ?>
        </span>
    </a>
    <p class="center bold"><?php echo $album->title ?></p>
</div>
<?php endforeach; endif; ?>
</div>
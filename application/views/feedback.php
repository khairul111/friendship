<div style="width:900px;float:left; padding-bottom: 10px;">
<form method="post" action="/contact/feedback" name="frmContact">
    <?php if(!empty($formCommonError)): ?>
    <div class="form-common-error ui-state-error ui-corner-all">
        <div><?php echo (!empty($formCommonError)) ? $formCommonError : ''; ?></div>
    </div>
    <?php endif; ?>
    <label><?php echo $this->lang->line('COMMON_TEXT_NAME'); ?></label>
    <input type="text" name="name" class="smallInput medium" value="<?php echo set_value('username'); ?>"/>
    <?php echo form_error('name'); ?>

    <label><?php echo $this->lang->line('COMMON_TEXT_EMAIL'); ?></label>
    <input type="text" name="email" class="smallInput medium" value="<?php echo set_value('email'); ?>"/>
    <?php echo form_error('email'); ?>

    <label><?php echo $this->lang->line('COMMON_TEXT_SUBJECT'); ?></label>
    <input type="text" name="subject" class="smallInput medium" value="<?php echo set_value('subject'); ?>"/>
    <?php echo form_error('subject'); ?>

    <label><?php echo $this->lang->line('COMMON_TEXT_MESSAGE'); ?></label>
    <textarea name="msg" cols="30" rows="8" class="smallInput medium"><?php echo set_value('msg'); ?></textarea>
    <?php echo form_error('msg'); ?>

    <a class="button" onclick="javascript:document.frmContact.submit();" ><span><?php echo $this->lang->line('COMMON_TEXT_SEND'); ?></span></a>
    <a class="button" href="/"><span><?php echo $this->lang->line('COMMON_TEXT_CANCEL'); ?></span></a>
</form>
</div>

<div style="float:left; width:465px !important;">
    <?php if($language == 'en'): ?>
    <?php echo $settings['SITE_CONTACT_ADDRESS_EN']; ?>
    <?php else: ?>
    <?php echo $settings['SITE_CONTACT_ADDRESS_BN']; ?>
    <?php endif; ?>
</div>

<style type="text/css">
    /*******************************************************************************
      FORMS
    *******************************************************************************/
    form label { display:block !important; line-height:normal !important; margin: 5px 0px;  font-size:12px;	font-weight:bold; color: #000000;}
    textarea { display:block; }
    .smallInput { padding:3px 3px; border:1px solid #999; font-size:12px !important; font-family:"Trebuchet MS", Arial, Helvetica, sans-serif !important; color: #333 !important; }
    .largeInput { padding:6px 5px; border:1px solid #999; font-size:15px !important; font-family:"Trebuchet MS", Arial, Helvetica, sans-serif !important; color: #333 !important; }
    form .small { width:150px; }
    form .medium { width:350px; }
    form .large { width:800px; }
    form .wide { width:890px; }
    .button {
        margin: 0px;
        padding: 0px !important;
        border: 0px;
        background: transparent url('/assets/images/but_right_blue.gif') no-repeat scroll top right;
        color: #1b486a;
        display: block;
        float: left;
        height: 29px;
        margin-right: 6px;
        margin-top:10px;
        padding-right: 12px !important;
        text-decoration: none;
        overflow: hidden;
        font-size: 12px;
        outline: none !important;
        cursor: pointer;
        font-weight: bold;
    }
    .button span {
        background: url('/assets/images/but_left_blue.gif') no-repeat left top;
        display: block;
        line-height: 29px;
        padding: 0px 0px 0px 12px;
        outline: none !important;
        float:left;
    }
    .button:hover {
        background-position: right bottom;
        text-decoration:none !important
    }
    .button:hover span {
        background-position: left bottom;
        color: #1b486a;
    }
</style>

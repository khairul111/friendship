<div class="right"><a href="/news/home">Back to news home</a></div>
<div class="clear"></div>
<h3><?php echo stripallslashes($news[0]->title); ?></h3>
<span class="small">Published Date: <?php echo date("F j, Y", strtotime($news[0]->create_date)); ?></span>
<p><?php echo stripallslashes($news[0]->details); ?></p>
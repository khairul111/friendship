<?php if($flashdata = $this->session->flashdata('status_message')): ?>
<!-- FLASH MSG -->
<div id="flash-content" class="grid_16">
    <div id="flash_msg_cont" class="ui-state-highlight ui-corner-all">
        <p><?php echo $flashdata; ?></p>
    </div>
</div>
<!-- END FLASH MSG -->
<?php endif; ?>
<script type="text/javascript" src="<?php echo site_url('assets/js/jquery.innerfade.js');?>"></script>
<script type="text/javascript" src="<?php echo site_url('assets/js/config.js');?>"></script>

<div class="home">

    <div class="index-left">
        <?php echo $pageContents[0]->details; ?>
    </div>

    <div class="index-right">

        <div class="round-block">

            <div class="round-t"><div><div></div></div></div>

            <div class="block-conten">

                <div class="min-prev"><a href="#" title="Previous"><img src="<?php echo base_url()?>assets/images/min-prev.png" alt="Previous" width="24" height="110" /></a></div>

                <div class="news-con">

                    <div class="wrapper">
                        
                    <ul>
                        <?php if(is_array($news) && count($news) > 0):foreach($news as $newsInfo):?>
                        <li>
                            <a href="<?php echo base_url()?>news/view/<?php echo $newsInfo['slug'].'-'.$newsInfo['id']; ?>">
                                <img src="<?php echo base_url()?>assets/photos/148x106/<?php echo $newsInfo['path']?>" alt="News" />
                            </a>
                            <a href="<?php echo base_url()?>news/view/<?php echo $newsInfo['slug'].'-'.$newsInfo['id']; ?>">
                                <span class="news-title"><h4><?php echo $newsInfo['news_title']?></h4></span>
                            </a>
                            <?php echo limitCharacter($newsInfo['details'], 100); ?>
                            <span class="readmore"><a href="<?php echo base_url()?>news/view/<?php echo $newsInfo['slug'].'-'.$newsInfo['id']; ?>">read more</a></span>
                        </li>
                        <?php endforeach;endif;?>
                    </ul>
                    </div>
                </div>

                <div class="min-next"><a href="#" title="Next"><img src="<?php echo base_url()?>assets/images/min-next.png" alt="Next" width="24" height="110" /></a></div>
                <span class="clear"></span>
            </div>
            
            <div class="round-b"><div><div></div></div></div>

        </div>

        <span class="clear"></span>
        
        <div class="doc-block">

            <div class="doc-item">
                <img src="<?php echo base_url()?>assets/photos/79x61/<?php echo $newsleter[0]['path'] ?>" alt="Newsletter" />
                <h2><a href="/page/newsleter-<?php echo $settings['NEWSLETER']?>">Newsletter</a></h2>
                <p><?php echo limitCharacter($newsleter[0]['details'], 50, ""); ?></p>
            </div>

            <div class="doc-item">
                <img src="<?php echo base_url()?>assets/photos/79x61/<?php echo $video[0]['path'] ?>" alt="Video" />
                <h2><a href="/page/video-<?php echo $settings['VIDEO']?>">Video</a></h2>
                <p><?php echo limitCharacter($video[0]['details'], 50, ""); ?></p>
            </div>

            <div class="doc-item">
                <img src="<?php echo base_url()?>assets/photos/79x61/<?php echo $blog[0]['path'] ?>" alt="Blog" />
                <h2><a href="<?php echo base_url()?>blog" target="blank">Blog</a></h2>
                <p><?php echo limitCharacter($blog[0]['details'], 50, ""); ?></p>
            </div>

            <div class="doc-item">
                <img src="<?php echo base_url()?>assets/photos/79x61/<?php echo $donate[0]['path'] ?>" alt="Donate" />
                <h2><a href="/donate">Donate</a></h2>
                <p><?php echo limitCharacter($donate[0]['details'], 50, ""); ?></p>
            </div>
            
        </div>
        
        <div class="features-block">
            <div class="round-block-gra">
                <div class="round-gra-t">
                    <div>
                        <div><h2>Upcoming Events</h2></div>
                    </div>
                </div>

                <?php if(is_array($events) && count($events)):?>
                <div class="block-conten-gra">
                <ul class="list-item">
                <?php foreach($events as $event):?>
                    <li><span>&raquo;</span>
                    <a href="<?php echo base_url()?>news/view/<?php echo $event['slug'].'-'.$event['id']; ?>">
                        <?php echo limitCharacter($event['event_title'], 60, ""); ?>
                     </a>
                    </li>
                <?php endforeach;?>
                </ul>
                </div>
                <?php else:?>
                  <div class="block-conten-gra">Currently No events Available</div>
                <?php endif;?>
                <div class="round-gra-b fix">
                    <div class="round-left"></div>
                    <div class="round-right"></div>
                </div>

            </div>
            
            <div class="round-block-gra">
                <div class="round-gra-t">
                    <div>
                        <div><h2>Brochure</h2></div>
                    </div>
                </div>
             
                <div class="block-conten-gra">
                   
                    <div class="publication">
                        <table>
                            <tr>
                                <td><img src="<?php echo base_url()?>assets/photos/66x92/bc.jpg" alt="Brochure" /></td>
                                <td valign="middle"><h2><a href="<?php echo base_url()?>assets/media/<?php echo $brochure['path']?>"><?php echo $brochure['title']?></a></h2></td>
                            </tr>
                        </table>
                        <span class="clear"></span>
                    </div>
                </div>
                
                <div class="round-gra-b">
                    <div class="round-left"></div>
                    <div class="round-right"></div>
                </div>

            </div>

        </div>
        <span class="clear"></span>
    </div>
    <span class="clear"></span>
</div>
<script type="text/javascript">


        $(function () {

            $('.news-con').anythingSlider({
                easing: "linear",        // Anything other than "linear" or "swing" requires the easing plugin
                autoPlay: false,                 // This turns off the entire FUNCTIONALY, not just if it starts running or not.
                delay: 3000,                    // How long between slide transitions in AutoPlay mode
                startStopped: false,            // If autoPlay is on, this can force it to start stopped
                animationTime: 600,             // How long the slide transition takes
                hashTags: true,                 // Should links change the hashtag in the URL?
                buildNavigation: false,          // If true, builds and list of anchor links to link to each slide
                pauseOnHover: true,             // If true, and autoPlay is enabled, the show will pause on hover
                nextButtonClass: '.min-next',             // next button class
                prevButtonClass: '.min-prev'             // previous button class
                });



        });
</script>

<div class="inner-content">
    <?php echo stripcslashes($pageContents[0]->details); ?>
</div>
<div id="inner-sidebar">
    <?php if(is_array($subNavigationPage[$RootParent])): ?>
    <div id="inner-sidenav">
        <ul>
            <?php foreach($subNavigationPage[$RootParent] as $subParent ): ?>
            <li>
                <a href="<?php echo(strtoupper($subParent['nav_title']) == 'DONATE')? '/donate/':'/page/'.$subParent['slug']."-".$subParent['id'];?>"><?php echo $subParent['nav_title'];?></a>
                <?php if(isset($subNavigationPage[$subParent['id']]) && stripos($breadCrumb, $subParent['nav_title']) == true):?>
                <ul>
                     <?php foreach($subNavigationPage[$subParent['id']] as $secondLevelSubNav):?>
                     <li>
                           <a href="<?php echo(strtoupper($secondLevelSubNav['nav_title']) == 'DONATE')? '/donate/':'/page/'.$secondLevelSubNav['slug'].'-'.$secondLevelSubNav['id'];?>"><span>&raquo;</span><?php echo stripallslashes($secondLevelSubNav['nav_title']) ?></a>
                     </li>
                     <?php endforeach;?>
                </ul>
                <?php endif;?>
            </li>
            <?php endforeach;?>
        </ul>
    </div>
    <?php endif;?>
<?php if ($pageContents[0]->rel_story): ?>
    <div class="round-block-gray">
        <div class="round-gray-t">
        <div>
        <div></div>
        </div>
        </div>
        <div class="block-gray-conten">
            <h2 class="sideheader">Related Stories</h2>
            <?php echo $pageContents[0]->rel_story; ?>
            <span class="clear"></span>
        </div>
        <div class="round-gray-b">
        <div>
        <div></div>
        </div>
        </div>
    </div>
    <?php endif; ?>
</div>
<span class="clear"></span>



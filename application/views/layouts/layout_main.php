<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><?php echo $windowTitle; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link type="image/gif" href="/assets/images/favicon.gif" rel="shortcut icon"/>
        <meta name="author" content="Right Brain Solution Ltd." />
        <meta name="description" content="<?php echo isset($settings['GENERAL_META_DESCRIPTION']) ? $settings['GENERAL_META_DESCRIPTION'] : '' ?>" />
        <meta name="keywords" content="<?php echo isset($settings['GENERAL_META_KEYWORD']) ? $settings['GENERAL_META_KEYWORD'] : '' ?>" />
        <link rel="stylesheet" href="<?php echo site_url('assets/css/frontend/jqueryslidemenu.css'); ?>" type="text/css" />
        <?php if($html_body_id != 'home'):?>
        <link rel="stylesheet" href="<?php echo site_url('assets/css/frontend/style.css'); ?>?2" type="text/css" />
        <?php endif;?>
        <link rel="stylesheet" href="<?php echo site_url('assets/css/frontend/newhome.css'); ?>?2" type="text/css" />
        <link rel="stylesheet" href="<?php echo site_url('assets/css/frontend/dropdown.css'); ?>" type="text/css" />

        <script type="text/javascript" src="<?php echo site_url('assets/js/jquery-1.3.2.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo site_url('assets/js/plugins/jquery.tools.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo site_url('assets/js/plugins/jquery.easing.1.2.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo site_url('assets/js/jquery.anythingslider.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo site_url('assets/js/jquery.cookie.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo site_url('assets/js/common.js'); ?>"></script>

    <!--[if IE 7]><style type="text/css">html .jqueryslidemenu{margin-bottom:1px;}</style><![endif]-->
    <!--[if IE 6]><style type="text/css">html .inner-banner{height : -1%} </style><![endif]-->

        <script type="text/javascript" src="<?php echo site_url('assets/js/jqueryslidemenu.js'); ?>"></script>

        <script type="text/javascript">

            $(document).ready(function(){
                SearchBg();
                $("#textfield").blur(function(){
                    changeSearchBg();
                });
                $('.banner-photo-left').parent().css({'height' : '175px'});
            });
            function SearchBg(){
                $("#textfield").css("background-image", "url(/assets/images/search_watermark.gif)");
            }
            function changeSearchBg()
            {
                $("#textfield").css("background-image", "no-repeat");
                //$("#textfield").css("background-image", "url(/assets/images/search_watermark.gif)");
            }

        </script>

    </head>
    <body>
        <div class="wrapper">
            <div class="header">
                <a href="/" title="Friendship" class="logo"><img src="<?php echo base_url() ?>assets/images/logo.jpg" alt="Friendship" /></a>
                <ul>
                    <li><a title="Bangla" href="/file/change_language/bn"><img src="<?php echo base_url() ?>assets/images/bangla.jpg" alt="" /></a></li><li>|</li>
                    <li><a title="English" href="/file/change_language/en"><img src="<?php echo base_url() ?>assets/images/english.jpg" alt="" /></a></li>
                    <li class="search">
                         
                        <form id="form1" action="http://www.google.com/cse">
                            <input type="hidden" name="cx" value="006456919801058306882:5yza0f-hw7g" />
                            <input type="hidden" name="ie" value="UTF-8" />
                            <input type="hidden" value="www.friendship-bd.org" name="site"/>
                            <input name="q" type="text" onFocus="if(this.value=='Site Search') this.value='';" onBlur="if(this.value=='') this.value='Site Search'" value="Site Search" />
                            <input type="submit" name="sa" id="button" value="" />                     
                        </form>
                    </li>
                </ul>
                <ul class="textResize">
                    <li><a name="fz_small" href="">a</a></li>
                    <li><a name="fz_midium" href="" class="medium">a</a></li>
                    <li><a name="fz_large" href="" class="large">a</a></li>
                </ul>
            </div><!--/header-->
            <!--navi-->     
            <div class="navi jqueryslidemenu" id="myslidemenu">
                <ul>
                <?php foreach ($navigationPage as $ctr=>$navigationTitle): $navClass = (($html_body_id == 'home') || ($html_body_id == 'page-' . $navigationTitle->ref_id)) ? 'active' : ''; ?>
                        <li class="<?php echo $navClass ?> dir">
                            <a href="<?php if (!isset($subNavigationPage[$navigationTitle->ref_id])): ?>/page/<?php echo $navigationTitle->slug; ?><?php else: ?>#<?php endif; ?>"><?php echo $navigationTitle->nav_title; ?></a>
                            <?php if (isset($subNavigationPage[$navigationTitle->ref_id])): ?>
                                <ul class="sub-nav">
                                    <?php foreach ($subNavigationPage[$navigationTitle->ref_id] as $subNav): ?>
                                        <li><a href="<?php echo(strtoupper($subNav['nav_title']) == 'DONATE') ? '/donate/' : '/page/' . $subNav['slug']; ?>"><?php echo stripallslashes($subNav['nav_title']) ?></a></li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                        </li>
                        <?php if($ctr <= 4):?>
                        <li>|</li>
                        <?php endif;?>
                    <?php endforeach; ?>                                                 
                </ul>
            </div><!--/navi-->  
            
            <!--#main-slide-->
            <?php if (isset($showSlider) && $showSlider === true): ?>
            <div id="main-slide">
                <!--slide-con1--> 
                <div class="slide-con1">
                    <div class="wrap">
                        <ul id="slide">
                            <?php if (is_array($programe_images) && count($programe_images) > 0):foreach ($programe_images as $programe_image): ?>
                                    <li style="position: absolute; display: none;">
                                        <img alt="" src="<?php echo base_url() ?>assets/photos/<?php echo $programe_image['image_path'] ?>" />
                                    </li>
                                <?php endforeach;
                            endif; ?>
                        </ul>
                    </div>
                </div><!--/slide-con1--> 
                <!--#slide_buttons--> 
                <ul id="slide_buttons" style="display:none; height:0px;">
                    <?php if (is_array($programe_images) && count($programe_images) > 0):$count = 0;
                        foreach ($programe_images as $programe_image): ?>
                            <li class="slide_<?php echo $count ?>"></li>
                            <?php
                            $count++;
                        endforeach;
                    endif;
                    ?>
                </ul><!--/#slide_buttons--> 
                <!--slide-nav-->                       
                <div class="slide-nav">
          <ul>                               
              <li id="prev_button"><a href="#s">&nbsp;</a></li>
              <li id="next_button"><a href="#s">&nbsp;</a></li>
          </ul>
      </div><!--/slide-nav--> 
                <!--#programe-desc-->     
                <ul id="programe-desc" class="slide-text">
                    <?php if (is_array($programe_images) && count($programe_images) > 0):$count = 0;
                        foreach ($programe_images as $programe_image): ?>
                            <li>
                                <h2 class="slide-headline"><?php //echo $programe_image['programe_name'];    ?></h2>
                                <p class="slide-tagline"></p>
                            </li>
                            <?php
                            $count++;
                        endforeach;
                    endif;
                    ?>
                </ul><!--/#programe-desc-->  
                    
            </div><!--/#main-slide-->
            <?php else: ?>
                <div class="<?php echo($innerPage) ? 'inner-banner' : 'inner-banner' ?>" style="background-color:<?php echo ((!empty($pageColor)) ? $pageColor : '#B89865'); ?>">
                    <?php if ($innerPage): ?>
                        <div class="banner-photo-left">
                            <img alt="" height="251px" width="920px" src="/assets/photos/640x175/<?php echo $bannerPathLeft; ?>"/>
                        </div>
                    <?php else: ?>
                        <div class="inner-banner-left">
                            <div class="berdcum">
                                <?php echo $breadCrumb; ?>
                            </div>
                            <h1 class="page-header"><?php echo $pageTitle ?></h1>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
            <?php if (isset($showSlider) && $showSlider === true): ?>
            <?php echo $content_for_layout; ?>  
            <?php else:?>
            <div id="container">
                <div id="content">
                    <?php echo $content_for_layout; ?>
                </div>
            </div>
            <?php endif;?>
            <!--footer-->
            <div class="news-block share">
                <h3>stay connected to friendship</h3>
                <ul>
                    <li><a href="https://twitter.com/friendshipbd"><img src="/assets/images/icon-tw.png" alt="" /></a></li>
                    <li><a href="http://www.facebook.com/pages/Friendship-Bangladesh/207962325911240?ref=hl"><img src="/assets/images/icon-fb.png" alt="" /></a></li>
                    <li><a href="http://www.youtube.com/friendshipNGO"><img src="/assets/images/icon-you-tube.png" alt="" /></a></li>
                    <li><a href="http://friendship-bd.org"><img src="/assets/images/icon-rss.png" alt="" /></a></li>
                </ul>
            </div><!--/news-block--> 
            <div class="footer">    
                <p class="copyright"> Copyright &copy; <?php echo date("Y") ?> Friendship</p>
                <ul>
                    <li><a href="/sitemap/view">Sitemap</a></li><li>|</li>
                    <li><a href="/contact/feedback">Feedback</a></li><li>|</li>
                    <li><a href="/page/contact-us<?php //echo(empty($contactPageId))? '0':$contactPageId['id']?>"><?php echo $this->lang->line('MENU_CONTACT'); ?></a></li>
                </ul>
            </div><!--/footer--> 
        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-20692269-1']);
            _gaq.push(['_trackPageview']);

            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();

        </script>
        </div>
    </body>
</html>

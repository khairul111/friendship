<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><?php echo $windowTitle; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link type="image/gif" href="/assets/images/favicon.gif" rel="shortcut icon"/>
    <meta name="author" content="Right Brain Solution Ltd." />
    <meta name="description" content="<?php echo isset($settings['GENERAL_META_DESCRIPTION']) ? $settings['GENERAL_META_DESCRIPTION'] : ''?>" />
    <meta name="keywords" content="<?php echo isset($settings['GENERAL_META_KEYWORD']) ? $settings['GENERAL_META_KEYWORD'] : ''?>" />
    <link rel="stylesheet" href="<?php echo site_url('assets/css/frontend/jqueryslidemenu.css');?>" type="text/css" />

    <link rel="stylesheet" href="<?php echo site_url('assets/css/frontend/style.css');?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo site_url('assets/css/frontend/dropdown.css');?>" type="text/css" />

    <script type="text/javascript" src="<?php echo site_url('assets/js/jquery-1.3.2.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo site_url('assets/js/plugins/jquery.tools.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo site_url('assets/js/plugins/jquery.easing.1.2.js');?>"></script>
    <script type="text/javascript" src="<?php echo site_url('assets/js/jquery.anythingslider.js');?>"></script>

    <!--[if IE 7]><style type="text/css">html .jqueryslidemenu{margin-bottom:1px;}</style><![endif]-->
    <!--[if IE 6]><style type="text/css">html .inner-banner{height : -1%} </style><![endif]-->

    <script type="text/javascript" src="<?php echo site_url('assets/js/jqueryslidemenu.js');?>"></script>

    <script type="text/javascript">

        $(document).ready(function(){
           SearchBg();
           $("#textfield").blur(function(){
               changeSearchBg();
           });
           $('.banner-photo-left').parent().css({'height' : '175px'});
        });
        function SearchBg(){
            $("#textfield").css("background-image", "url(/assets/images/search_watermark.gif)");
        }
        function changeSearchBg()
        {
            $("#textfield").css("background-image", "no-repeat");
            //$("#textfield").css("background-image", "url(/assets/images/search_watermark.gif)");
        }

    </script>

</head>
<body>
    <div id="wrapper">

        <div id="header">

            <div id="branding">

                <div class="header-left">
                    <div class="logo"> <a href="/" title="Friendship"><img src="<?php echo base_url()?>assets/images/logo.jpg" alt="Friendship" /></a></div>
                </div>

                <div class="header-right">
                    <form id="form1" action="http://www.google.com/cse">
                        <input type="hidden" name="cx" value="006456919801058306882:5yza0f-hw7g" />
                        <input type="hidden" name="ie" value="UTF-8" />
                        <ul class="search-box">
                            <li>
                                <input type="hidden" value="www.friendship-bd.org" name="site"/>
                                <input name="q" type="text" class="search-textbox" id="textfield" />
                            </li>
                            <li>
                                <input type="submit" name="sa" id="button" value="Google Search" class="search-btn" />
                            </li>
                        </ul>
                    </form>
                    <script type="text/javascript" src="http://www.google.com/cse/brand?form=form1&amp;lang=en"></script>

                    <span class="clear"></span>
                    <div class="right-nav">
                        <a href="/donate/"><?php echo $this->lang->line('MENU_DONATE'); ?></a> |
                        <a href="/page/contact-us<?php //echo(empty($contactPageId))? '0':$contactPageId['id']?>"><?php echo $this->lang->line('MENU_CONTACT'); ?></a> |
                        <a class="my_language bangla-lng" title="Bangla" href="/file/change_language/bn">বাংলা</a> |
                        <a class="my_language" title="English" href="/file/change_language/en">English</a>
                    </div>
                </div>
                <span class="clear"></span>
            </div>

            <div id="myslidemenu" class="jqueryslidemenu">
                <ul>
                    <?php foreach($navigationPage as $navigationTitle): $navClass = (($html_body_id == 'home') || ($html_body_id == 'page-'.$navigationTitle->ref_id)) ? 'active' : ''; ?>
                    <li class="<?php  echo $navClass?> dir">
                        <a href="<?php if(!isset($subNavigationPage[$navigationTitle->ref_id])): ?>/page/<?php echo $navigationTitle->slug; ?><?php else: ?>#<?php endif; ?>">
                        <?php echo $navigationTitle->nav_title;?>
                        </a>
                        <?php if(isset($subNavigationPage[$navigationTitle->ref_id])): ?>
                        <ul class="sub-nav">
                            <?php foreach($subNavigationPage[$navigationTitle->ref_id] as $subNav):?>
                            <li><a href="<?php echo(strtoupper($subNav['nav_title']) == 'DONATE')? '/donate/':'/page/'.$subNav['slug'];?>"><?php echo stripallslashes($subNav['nav_title']) ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                        <?php endif; ?>
                    </li>
                    <?php endforeach;?>
                    <li><a href="/gallery">Photo Gallery</a></li>
            </ul>
        </div>
    </div>
        <div id="container">
            <?php if(isset($showSlider) && $showSlider === true):?>
            <div id="main-slide">

                <div class="slide-con1">
                    <div class="wrapper">
                    <ul id="slide">
                        <?php if(is_array($programe_images) && count($programe_images) > 0):foreach($programe_images as $programe_image):?>
                            <li style="position: absolute; display: none;">
                                <img alt="" src="<?php echo base_url()?>assets/photos/650x310/<?php echo $programe_image['image_path']?>" />
                            </li>
                        <?php endforeach;endif;?>
                    </ul>
                    </div>
                </div>
                <ul id="slide_buttons" style="display:none; height:0px;">
                    <?php if(is_array($programe_images) && count($programe_images) > 0):$count=0;foreach($programe_images as $programe_image):?>
                        <li class="slide_<?php echo $count?>"></li>
                    <?php $count++; endforeach;endif;?>

                </ul>
                <div class="slide-nav">
                        <ul>
                            <li id="pause_button" class="pause_button"><a href="#" class="banner-pause-btn">pause</a></li>
                            <li id="prev_button"><a href="#s">&nbsp;</a></li>
                            <li id="next_button"><a href="#s">&nbsp;</a></li>
                        </ul>
                </div>
                <ul id="programe-desc" class="slide-text">
                    <?php if(is_array($programe_images) && count($programe_images) > 0):$count=0;foreach($programe_images as $programe_image):?>
                        <li>
                            <h2 class="slide-headline"><?php //echo $programe_image['programe_name']?></h2>
                            <p class="slide-tagline"></p>
                        </li>
                    <?php $count++; endforeach;endif;?>
                </ul>

            </div>

            <div class="map">
                <a href="/page/map-link-<?php echo(empty($settings['HOMEPAGE_MAP_LINK']))? '0':$settings['HOMEPAGE_MAP_LINK'];?>"><img src="<?php echo base_url()?>assets/photos/265x310/<?php echo $mapImage?>" alt="Map" width="265" height="310" /></a>
            </div>

            <span class="clear"></span>
            <?php else:?>
            <div class="<?php echo($innerPage)? 'inner-banner':'inner-banner'?>" style="background-color:<?php echo ((!empty($pageColor))?$pageColor:'#B89865'); ?>">
                <?php if($innerPage): ?>
                <div class="banner-photo-left">
                    <img alt="" height="175px" width="100%" src="/assets/photos/640x175/<?php echo $bannerPathLeft;?>"/>
                </div>
                <div class="banner-photo-right">
                    <img height="175px" width="100%" src="/assets/photos/230x160/<?php if(isset($pagesPhotos)) echo $bannerPathRight;?>" alt="" />
                </div>
                <?php else:?>
                <div class="inner-banner-left">
                    <div class="berdcum">
                        <?php echo $breadCrumb;?>
                    </div>
                    <h1 class="page-header"><?php echo $pageTitle ?></h1>
                </div>
                <div class="inner-banner-right">
                    <img src="/assets/photos/259x99/<?php if(isset($pagesPhotos)) echo $pagesPhotos[0]->path;?>" alt="<?php echo $pageTitle?>"/>
                </div>
                <?php endif;?>
            </div>
            <?php endif;?>
            <div id="content">
                <?php echo $content_for_layout; ?>
            </div>
        </div>
        <div id="footer">
            <div class="footer-left"> Copyright &copy; <?php echo date("Y") ?> Friendship</div>
            <div class="footer-credit"></div>
            <div class="footer-right"> <a href="/sitemap/view">Sitemap</a> | <a href="/contact/feedback">Feedback</a> | <a href="/page/contact-us<?php //echo(empty($contactPageId))? '0':$contactPageId['id'] ?>"><?php echo $this->lang->line('MENU_CONTACT'); ?></a></div>
        </div>

    </div>
</body>
</html>


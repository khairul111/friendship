<div id="inner-sidebar">
    <div class="round-block-gray">
        <div class="round-gray-t">
            <div>
                <div></div>
            </div>
        </div>
        <div class="block-gray-conten">
            <h2 class="sideheader">Archives by Year</h2>
            <?php foreach ($distinctYear as $n): ?>
            <a href="/news/home/<?php echo $n->year; ?>"><p style="padding-left: 30px; font-size: 15px;">&raquo; <?php echo $n->year; ?></p></a>
            <?php endforeach; ?>
        </div>
        <div class="round-gray-b">
            <div>
                <div></div>
            </div>
        </div>
    </div>
</div>

<?php if(isset($flag)):?>
<div id='sub-header' style="padding-bottom: 20px;">
    <h2><?php echo $flag; ?>
</div>
<?php endif; ?>

<?php foreach ($news as $n): ?>
    <div class="news">
        <img alt="News" src="http://www.friendship-bd.org/assets/photos/148x106/<?php echo is_null($n->path) ? 'no_image.gif' : $n->path; ?>" align="left" style="margin-right:10px;" />
		
<!--		
<h3><?php echo stripallslashes($news[0]->title); ?></h3>
<span class="small">Published Date: <?php echo date("F j, Y", strtotime($news[0]->create_date)); ?></span>
<p><?php echo stripallslashes($news[0]->details); ?></p> -->
<!--<a href="<?php echo base_url()?>news/view/<?php echo $newsInfo['slug'].'-'.$newsInfo['id']; ?>"> -->
		
       <h3><a href="/news/view/<?php echo $n['slug'].'-'.$n['id']; ?>"><?php echo limitCharacter(stripallslashes($n->title), 75); ?></a></h3>
        <label class="small">Published Date: <?php echo date("F j, Y", strtotime($n->create_date)); ?></label><br />
        <p><?php echo limitCharacter(stripallslashes($n->details), 230); ?><span class="readmore"><a href="/news/view/<?php echo $n->slug . '-' . $n->id; ?>"> read more</a></span></p>
    </div>
<?php endforeach; ?>
 -->
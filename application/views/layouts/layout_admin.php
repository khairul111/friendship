<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title><?php echo $settings["SITE_NAME"]; ?></title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/960.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/reset.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/text.css'); ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/smoothness/ui.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/blue.css'); ?>" />

    <script type="text/javascript" src="<?php echo site_url('assets/js/jquery-1.3.2.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo site_url('assets/js/blend/jquery.blend.js');?>"></script>
    <script type="text/javascript" src="<?php echo site_url('assets/js/ui.core.js');?>"></script>
    <script type="text/javascript" src="<?php echo site_url('assets/js/ui.sortable.js');?>"></script>
    <script type="text/javascript" src="<?php echo site_url('assets/js/ui.dialog.js');?>"></script>
    <script type="text/javascript" src="<?php echo site_url('assets/js/effects.js');?>"></script>
    <script type="text/javascript" src="<?php echo site_url('assets/js/common.js');?>"></script>

    <!--[if IE]>
    <script language="javascript" type="text/javascript" src="js/flot/excanvas.pack.js"></script>
    <![endif]-->

    <!--[if IE 6]>
    <link rel="stylesheet" type="text/css" href="css/iefix.css" />
    <script src="js/pngfix.js"></script>
    <script>DD_belatedPNG.fix('#menu ul li a span span');</script>
    <![endif]-->
    
</head>

<body <?php echo (isset($html_body_id)) ? "id=\"$html_body_id\"" : '';?>>

    <!-- WRAPPER START -->
    <div class="container_16" id="wrapper">

        <div class="grid_8" id="logo"><?php echo $settings["SITE_NAME"]; ?> Administration</div>

        <div class="grid_8">
            <div id="user_tools">
                <span>Welcome <a href="#"><?php if(isset($_SESSION['userName'])) echo $_SESSION['userName']?></a> | <a href="/admin/login/changepassword">Change Password</a> | <a href="/admin/login/logout">Logout</a></span>
            </div>
        </div>

        <div class="grid_16" id="header">

            <!-- MENU START -->
            <div id="menu">
                <ul class="group" id="menu_group_main">

                    <li class="item first" id="one"><a href="<?php echo base_url() ?>" target="_blank" class="main <?php echo (($html_body_id=='dashboard')?'current':'');?>"><span class="outer"><span class="inner dashboard">Main site</span></span></a></li>
                    <li class="item middle" id="two"><a href="/admin/page" class="main <?php echo (($html_body_id=='page')?'current':'');?>"><span class="outer"><span class="inner content">Pages</span></span></a></li>
                    <li class="item middle" id="three"><a href="/admin/media" class="main <?php echo (($html_body_id=='media')?'current':'');?>"><span class="outer"><span class="inner reports png">File Cabinet</span></span></a></li>
                    <li class="item middle" id="four"><a href="/admin/programe" class="main <?php echo (($html_body_id=='programe')?'current':'');?>"><span class="outer"><span class="inner event_manager">Programme</span></span></a></li>
                    <li class="item middle" id="five"><a href="/admin/album" class="main <?php echo (($html_body_id=='album')?'current':'');?>"><span class="outer"><span class="inner media_library">Photo Albums</span></span></a></li>
                    <li class="item middle" id="six"><a href="/admin/payment" class="main <?php echo (($html_body_id=='page')?'current':'');?>"><span class="outer"><span class="inner event_manager">Payments</span></span></a></li>
                    <li class="item middle" id="seven"><a href="/admin/news" class="main <?php echo (($html_body_id=='news')?'current':'');?>"><span class="outer"><span class="inner newsletter">News</span></span></a></li>
                    <li class="item middle" id="eight"><a href="/admin/setting" class="main <?php echo (($html_body_id=='page')?'current':'');?>"><span class="outer"><span class="inner settings">Settings</span></span></a></li>
                    <li class="item last"   id="nine"><a href="/admin/feedback" class="main <?php echo (($html_body_id=='page')?'current':'');?>"><span class="outer"><span class="inner content">Feedback</span></span></a></li>

                </ul>
            </div>
            <!-- MENU END -->
        </div>

        {FLASH_MSG_CONT}

        <!-- CONTENT START -->
        <div class="grid_16" id="content">
            
            <?php echo $content_for_layout; ?>
            
        </div>
        <!-- END CONTENT-->

        <div class="clear"> </div>

    </div>
    <!-- WRAPPER END -->

    <!-- FOOTER START -->
    <div class="container_16" id="footer">Powered by MIS deparment of Friendship </div>
    <div id="confirm-dialog" title="Confirmation"></div>
    <div id="dialog" title="Dialog"></div>
    <!-- FOOTER END -->
</body>
</html>

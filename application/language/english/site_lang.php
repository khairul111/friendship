<?php

$lang['COMMON_TEXT_LANGUGAGE'] 			= 'Language';
$lang['english'] 						= 'English';
$lang['bangla']							= 'বাংলা';

$lang['COMMON_TEXT_COPYRIGHT']			= 'Copyright &copy; Friendship. All Rights Reserved.';
$lang['COMMON_TEXT_NAME']				= 'Name';
$lang['COMMON_TEXT_EMAIL']				= 'Email';
$lang['COMMON_TEXT_PHONE']				= 'Phone';
$lang['COMMON_TEXT_ADDRESS']			= 'Address';
$lang['COMMON_TEXT_STATE']      		= 'State / Province';
$lang['COMMON_TEXT_CITY']       		= 'City';
$lang['COMMON_TEXT_ZIP']                = 'Zip / Postal Code';
$lang['COMMON_TEXT_COUNTRY']      		= 'Country';
$lang['COMMON_TEXT_COMMENTS']      		= 'Comments';
$lang['COMMON_TEXT_SUBJECT']			= 'Subject';
$lang['COMMON_TEXT_MESSAGE']			= 'Message';
$lang['COMMON_TEXT_CANCEL']             = 'Cancel';
$lang['COMMON_TEXT_SAVE']               = 'Save';
$lang['COMMON_TEXT_SEND']               = 'Send message';
$lang['COMMON_TEXT_ALBUM']              = 'Album';
$lang['COMMON_TEXT_PHOTO']              = 'Photo';
$lang['COMMON_TEXT_UPLOADED_ON']        = 'Uploaded on:';
$lang['COMMON_TEXT_AMOUNT']             = 'Amount';
$lang['COMMON_TEXT_PAYMENT_METHOD']     = 'Click on the following button to donate:';

$lang['MENU_HOME']                      = 'Home';
$lang['MENU_GALLERY']                   = 'Photo Gallery';
$lang['MENU_FILE_CABINET']              = 'File cabinet';
$lang['MENU_BLOG']                      = 'Blog';
$lang['MENU_CONTACT']                   = 'Contact Us';
$lang['MENU_DONATE']                    = 'Donate';
$lang['MENU_NEWS']                      = 'News';
$lang['MENU_NEWS_DETAILS']              = 'News details';
$lang['MENU_PRIVACY']                   = 'Privacy';

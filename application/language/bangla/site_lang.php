<?php

$lang['COMMON_TEXT_LANGUGAGE'] 			= 'Language';
$lang['english'] 						= 'English';
$lang['bangla']							= 'বাংলা';

$lang['COMMON_TEXT_COPYRIGHT']			= 'কপিরাইট &copy; ফ্রেন্ডশীপ, সর্বস্বত্ত্ব সংরক্ষিত';
$lang['COMMON_TEXT_NAME']				= 'নাম';
$lang['COMMON_TEXT_EMAIL']				= 'ইমেইল';
$lang['COMMON_TEXT_SUBJECT']			= 'বিষয়';
$lang['COMMON_TEXT_MESSAGE']			= 'বার্তা';
$lang['COMMON_TEXT_CANCEL']             = 'বাতিল';
$lang['COMMON_TEXT_SAVE']               = 'সংরক্ষন';
$lang['COMMON_TEXT_SEND']               = 'প্রেরণ করুন';
$lang['COMMON_TEXT_ALBUM']              = 'এলবাম';
$lang['COMMON_TEXT_PHOTO']              = 'ফটো';
$lang['COMMON_TEXT_UPLOADED_ON']        = 'আপলোডের তারিখ:';

$lang['MENU_HOME']                      = 'প্রথম পাতা';
$lang['MENU_GALLERY']                   = 'গ্যালারি';
$lang['MENU_FILE_CABINET']              = 'ফাইল সমূহ';
$lang['MENU_BLOG']                      = 'ব্লগ';
$lang['MENU_CONTACT']                   = 'যোগাযোগ';
$lang['MENU_DONATE']                    = 'অনুদান';
$lang['MENU_NEWS']                      = 'সংবাদ';
$lang['MENU_NEWS_DETAILS']              = 'বিস্তারিত সংবাদ';
$lang['MENU_PRIVACY']                   = 'গোপনীয়তা';


tinyMCEPopup.requireLangPack();

var PagelinkDialog = {
	init : function() {
		var f = document.forms[0];

		// Get the selected contents as text and place it in the input
		f.ontext.value = tinyMCEPopup.editor.selection.getContent({format : 'text'});
	},

	insert : function() {
		// Prepare the link
		var link = pathPrefix + "/" + document.forms[0].page_name.value;
		var text = document.forms[0].ontext.value;
		
		tinyMCEPopup.editor.execCommand('mceInsertContent', false, '<a href="'+ link +'">'+ text +'</a>' );
		tinyMCEPopup.close();
	}
};

tinyMCEPopup.onInit.add(PagelinkDialog.init, PagelinkDialog);

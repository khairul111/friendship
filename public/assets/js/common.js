$(document).ready(function()
{
       if(typeof $('#dialog').dialog == 'function' )
        {
             $('#dialog').dialog({
			 autoOpen: false,
			 resizable: false,
			 width: 350,
			 buttons: {
				"Ok": function() {
					$(this).dialog("close");
				}}
            });

            $('#confirm-dialog').dialog({
                autoOpen: false,
                resizable: false,
                width: 300,
                modal: true,
                buttons: {}
            });
        }

        if(typeof bindPageEvents  == 'function')
        {
            bindPageEvents();
        }

});


function showConfirmation(title, message)
{
    var title   = title   || 'Warning!';
    var message = message || 'Are you sure to do this?';
    var iconspan    = '<span style="margin: 0pt 7px 50px 0pt; float: left;" class="ui-icon ui-icon-alert"> </span>';

    $('#confirm-dialog').dialog('option', 'title', title);
    $('#confirm-dialog').html('<p>' + iconspan + message + '</p>').dialog('open');
}




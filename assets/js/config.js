$(document).ready(

    function(){
        $('ul#slide').innerfade({
            speed: 1000,
            timeout: 5000,
            type: 'sequence',
            containerheight: 	'300px',
            slide_timer_on: 	'yes',
            slide_ui_parent: 	'slide',
            slide_ui_text:		'programe-desc',
            pause_button_id: 	'pause_button',
            slide_nav_id:		'slide_buttons'
        });

        $.setOptionsButtonEvent();

        $("#pause_button").click(function() {
            $.pause();
        });

        $("#next_button").click(function() {
            $.next();
        });

        $("#prev_button").click(function() {
            $.prev();
        });

    });
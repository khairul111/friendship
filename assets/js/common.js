$(document).ready(function()
{
    var fontSize = parseInt($.cookie("font-size"));
   $('body').css('font-size', fontSize);
   $('.index-left p').css('font-size', fontSize);
   
   if(typeof $('#dialog').dialog == 'function' )
    {
         $('#dialog').dialog({
                     autoOpen: false,
                     resizable: false,
                     width: 350,
                     buttons: {
                            "Ok": function() {
                                    $(this).dialog("close");
                            }}
        });

        $('#confirm-dialog').dialog({
            autoOpen: false,
            resizable: false,
            width: 300,
            modal: true,
            buttons: {}
        });
    }

    if(typeof bindPageEvents  == 'function')
    {
        bindPageEvents();
    }

});

$('a.newsletter-signup').live('click', function(){
    firstname = $('#newsletter-firstname').val();
    lastname = $('#newsletter-lastname').val();
    email = $('#newsletter-email').val();
    $.post("/contact/newsletter", {"firstName":firstname, "lastName":lastname, "email":email}, function(data){ 
        alert(data.message);
        $('#newsletter-firstname').val('First Name');
        $('#newsletter-lastname').val('Last Name');
        $('#newsletter-email').val('Email');
    }, 'json');
    return false;
});

$('ul.textResize a').live('click',function(){
    var fontSize = 12;
    if($(this).attr('name') == 'fz_midium'){
        fontSize = 14;
    }else if($(this).attr('name') == 'fz_large'){
        fontSize = 15.5;
    }
    $.cookie("font-size", fontSize, { expires : 10 });
    $('body').css('font-size', fontSize);
    $('.index-left p').css('font-size', fontSize);
});

function showConfirmation(title, message)
{
    var title   = title   || 'Warning!';
    var message = message || 'Are you sure to do this?';
    var iconspan    = '<span style="margin: 0pt 7px 50px 0pt; float: left;" class="ui-icon ui-icon-alert"> </span>';

    $('#confirm-dialog').dialog('option', 'title', title);
    $('#confirm-dialog').html('<p>' + iconspan + message + '</p>').dialog('open');
}




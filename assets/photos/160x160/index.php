<?php

ini_set("memory_limit","12M");

//include_once("$_sc[_ssi_dir]/main.php");

//if (!isset($fileName)) $fileName = '';
//echo $fileName;
//if (isset($in)) $fileName = $in;

// include from ../../php/...
include_once(dirname(dirname(dirname(__FILE__))).'/php/ImageManipulate/class.imagemanipulatefactory.php');

// remove all non-alphanumeric chars at begin & end of string
$fileName = preg_replace('/^\W+|\W+$/', '', $_GET['in']);

// compress internal whitespace and replace with _
//$fileName = preg_replace('/\s+/', '_', $fileName);

// remove all non-alphanumeric chars except '_', '-', ' ' and '.'
$fileName = preg_replace('/[^\w_\-\. ]/', '', $fileName);

// Read the original from parent directory
$srcFile = dirname(dirname(__FILE__)) . '/' . $fileName; //die($srcFile);

// Store the thumb in this directory
$destFile = dirname(__FILE__) . '/'. $fileName; 

// Dimensions of thumbnail
$width  = $_GET['width'];
$height = $_GET['height'];

//$objResize = new ImageManipulateFactory();

// Instantiate the correct object depending on type of image i.e jpg or png
$objResize = ImageManipulateFactory::getInstanceOf($srcFile);//die($srcFile);

// Call the method to resize the image
//$objResize->ResizeImage($width, $height);

// Call the method to resize, fitting the smaller side
$objResize->ResizeImageToOverflow($width, $height);

// Crop from overlapping parts. If horizontal overlap, crop the center.
// If vertical overlap, crop near the top
$objResize->FancyCrop($width, $height);

// Display to screen
$objResize->Display(80); // pass the quality 

// Save to file
// pass the filename and quality
$objResize->Save($destFile, 80);
//$objResize->Save($destFile);

unset($objResize);
?>
